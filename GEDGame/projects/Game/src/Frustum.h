#pragma once

#include <DXUT.h>
#include <d3dx11effect.h>
#include <DirectXMath.h>
//#include <d3d11.h>

using namespace DirectX;

class Frustum{
public:
	Frustum();
	~Frustum();

	void ConstructFrustum(float screenDepth, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix);

	bool CheckPoint(float x, float y, float z);
	//not implemented
	bool CheckCube(float xCenter, float yCenter, float zCenter, float radius);
	bool CheckSphere(float xCenter, float yCenter, float zCenter, float radius);
	//not implemented
	bool CheckRectangle(float xCenter, float yCenter, float zCenter, float xSize, float ySize, float zSize);

private:
	XMFLOAT4 _planes[6];

};