#include "Game.h"


//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

    // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

    // Old Direct3D Documentation:
    // Start > All Programs > Microsoft DirectX SDK (June 2010) > Windows DirectX Graphics Documentation

    // DXUT Documentaion:
    // Start > All Programs > Microsoft DirectX SDK (June 2010) > DirectX Documentation for C++ : The DirectX Software Development Kit > Programming Guide > DXUT
	
    // New Direct3D Documentaion (just for reference, use old documentation to find explanations):
    // http://msdn.microsoft.com/en-us/library/windows/desktop/hh309466%28v=vs.85%29.aspx


    // Initialize COM library for windows imaging components
    /*HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    if (hr != S_OK)
    {
        MessageBox(0, L"Error calling CoInitializeEx", L"Error", MB_OK | MB_ICONERROR);
        exit(-1);
    }*/

    // Set DXUT callbacks
    DXUTSetCallbackMsgProc( MsgProc );
    DXUTSetCallbackKeyboard( OnKeyboard );
    DXUTSetCallbackFrameMove( OnFrameMove );
    DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

    DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
    DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
    DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
    DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
    DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );
    DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
    //DXUTSetIsInGammaCorrectMode(false);

    InitApp();
    DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
    DXUTSetCursorSettings( true, true );
    DXUTCreateWindow( L"Mega ultra awesome game" ); // You may change the title
	
    DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, _frameWidth, _frameHeight );

    DXUTMainLoop(); // Enter into the DXUT render loop
	DXUTShutdown();
	DeinitApp();
	delete _cfgParser;
    return DXUTGetExitCode();
}

//--------------------------------------------------------------------------------------
// Initialize the app 
//--------------------------------------------------------------------------------------
void InitApp()
{
    HRESULT hr;
    WCHAR path[MAX_PATH];

    // Parse the config file

	//find file
    V(DXUTFindDXSDKMediaFileCch(path, MAX_PATH, L"game.cfg"));
	char pathA[MAX_PATH];
	size_t size;
	wcstombs_s(&size, pathA, path, MAX_PATH);

	//load file (game.cfg)
	std::string pathToFile(pathA);
	_cfgParser=new ConfigParser();
	_cfgParser->load(pathToFile);
	_shadowMapRes = _cfgParser->getShadowMapRes();
	_grassResW = _cfgParser->getTerrainWidth();
	_grassResD = _cfgParser->getTerrainDepth();
	_frameWidth = _cfgParser->getResX();
	_frameHeight = _cfgParser->getResY();
	if (_cfgParser->getenablePostProcessing() == 1)
		_enablePP = true;

	

	std::map<std::wstring, ConfigParser::MeshData> configParserMapMeshes = _cfgParser->getMeshMap();
	for (std::map<std::wstring, ConfigParser::MeshData>::iterator it = configParserMapMeshes.begin(); it != configParserMapMeshes.end(); ++it)
	{
		ConfigParser::MeshData &meshData = it->second;

		if (it->first.compare(L"grass_medium") == 0) {
			Mesh* mesh = new Mesh(meshData.low, meshData.diffuse, meshData.specualr, meshData.glow, (_grassResD / 8) * (_grassResW / 8));
			meshes.emplace(it->first, mesh);
		}
		else if (it->first.compare(L"redwood_01") == 0) {
			Mesh* mesh = new Mesh(meshData.low, meshData.diffuse, meshData.specualr, meshData.glow, (_grassResD / 8) * (_grassResW / 8));
			meshes.emplace(it->first, mesh);
		}
		else {
			Mesh* mesh = new Mesh(meshData.low, meshData.diffuse, meshData.specualr, meshData.glow, 20);
			meshes.emplace(it->first, mesh);
		}
	}

	for (int x = 0; x < 4; ++x) {
		for (int y = 0; y < 4; ++y) {
			_treeTemplate(x, y) = ((double)rand() / (RAND_MAX)) + 1;
		}
	}
	_sprite = new SpriteRenderer(_cfgParser->getSpriteFiles());

    // Intialize the user interface

    g_settingsDlg.Init( &g_dialogResourceManager );
    g_hud.Init( &g_dialogResourceManager );
    g_sampleUI.Init( &g_dialogResourceManager );

    g_hud.SetCallback( OnGUIEvent );
    int iY = 30;
    int iYo = 26;
    g_hud.AddButton( IDC_TOGGLEFULLSCREEN, L"Toggle full screen", 0, iY, 170, 22 );
    g_hud.AddButton( IDC_TOGGLEREF, L"Toggle REF (F3)", 0, iY += iYo, 170, 22, VK_F3 );
    g_hud.AddButton( IDC_CHANGEDEVICE, L"Change device (F2)", 0, iY += iYo, 170, 22, VK_F2 );

	g_hud.AddButton (IDC_RELOAD_SHADERS, L"Reload shaders (F5)", 0, iY += 24, 170, 22, VK_F5);
    
    g_sampleUI.SetCallback( OnGUIEvent ); iY = 10;
    iY += 24;
    g_sampleUI.AddCheckBox( IDC_TOGGLESPIN, L"Toggle Spinning", 0, iY += 24, 125, 22, g_terrainSpinning ); 

	g_sampleUI.AddCheckBox(IDC_TOGGLELIGHTSPIN, L"Toggle light Spinning", 0, iY += 24, 125, 22, g_lightSpinning);

	//initialize player
	_playerMovement = XMVectorSet(0, 0, 0, 0);

	ShowCursor(false);
}

//--------------------------------------------------------------------------------------
// Render the help and statistics text. This function uses the ID3DXFont interface for 
// efficient text rendering.
//--------------------------------------------------------------------------------------
void RenderText()
{
    g_txtHelper->Begin();
    g_txtHelper->SetInsertionPos( 5, 5 );
    g_txtHelper->SetForegroundColor(XMVectorSet(1.0f, 1.0f, 0.0f, 1.0f));
    g_txtHelper->DrawTextLine( DXUTGetFrameStats(true)); //DXUTIsVsyncEnabled() ) );
	g_txtHelper->DrawTextLine(DXUTGetDeviceStats());
	g_txtHelper->SetForegroundColor(XMVectorSet(1.0f, 0.0f, 1.0f, 1.0f));
	g_txtHelper->DrawTextLine(L"fire gatling with \'R\'");
	g_txtHelper->DrawTextLine(L"fire plasma beam with \'F\'");
	g_txtHelper->DrawTextLine(L"fire guided missiles with \'T\'");
	wstringstream ws2;
	ws2 << "score: " << _score;
	g_txtHelper->DrawTextLine(ws2.str().c_str());
    g_txtHelper->End();
}

//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *, UINT, const CD3D11EnumDeviceInfo *,
        DXGI_FORMAT, bool, void* )
{
    return true;
}

//--------------------------------------------------------------------------------------
// Specify the initial device settings
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	UNREFERENCED_PARAMETER(pDeviceSettings);
	UNREFERENCED_PARAMETER(pUserContext);

    // For the first device created if its a REF device, optionally display a warning dialog box
    static bool s_bFirstTime = true;
    if( s_bFirstTime )
    {
        s_bFirstTime = false;
        if (pDeviceSettings->d3d11.DriverType == D3D_DRIVER_TYPE_REFERENCE)
        {
            DXUTDisplaySwitchingToREFWarning();
        }
    }
    //// Enable anti aliasing
    pDeviceSettings->d3d11.sd.SampleDesc.Count = 4;
    pDeviceSettings->d3d11.sd.SampleDesc.Quality = 1;

    return true;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependant on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice,
        const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	UNREFERENCED_PARAMETER(pBackBufferSurfaceDesc);
	UNREFERENCED_PARAMETER(pUserContext);

    HRESULT hr;

    ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext(); // http://msdn.microsoft.com/en-us/library/ff476891%28v=vs.85%29
    V_RETURN( g_dialogResourceManager.OnD3D11CreateDevice( pd3dDevice, pd3dImmediateContext ) );
    V_RETURN( g_settingsDlg.OnD3D11CreateDevice( pd3dDevice ) );
    g_txtHelper = new CDXUTTextHelper( pd3dDevice, pd3dImmediateContext, &g_dialogResourceManager, 15 );

    V_RETURN( ReloadShader(pd3dDevice) );

	
	// Load Meshes
	for (std::map<std::wstring, Mesh*>::iterator it = meshes.begin(); it != meshes.end(); ++it)
	{
		it->second->create(pd3dDevice);
	}


	//create input layout
	V_RETURN(Mesh::createInputLayout(pd3dDevice, g_gameEffect.meshPass1));
	V_RETURN(g_terrain.create(pd3dDevice, _cfgParser));

	//create render to texture
	_RenderTexture = new RenderToTexture;
	V_RETURN(_RenderTexture->create(pd3dDevice, _frameWidth*1.5, _frameHeight*1.5));
	//create render to texture shadow map
	_RenderShadowMap = new RenderToTexture;
	V_RETURN(_RenderShadowMap->create(pd3dDevice, _shadowMapRes, _shadowMapRes));
	//create render to texture sky map
	_renderSkyMap = new RenderToTexture;
	V_RETURN(_renderSkyMap->create(pd3dDevice, _frameWidth*1.5, _frameHeight*1.5));

	//create postProcessing
	_postProcessing = new PostProcessing;
	V_RETURN(_postProcessing->create(pd3dDevice, _frameWidth*1.5, _frameHeight*1.5));//create postProcessing
	_postProcessing2 = new PostProcessing;
	V_RETURN(_postProcessing2->create(pd3dDevice, _frameWidth*1.5, _frameHeight*1.5));
	_postProcessingDown = new PostProcessing;
	V_RETURN(_postProcessingDown->create(pd3dDevice, _frameWidth / 2, _frameHeight / 2));
	_postProcessingDown2 = new PostProcessing;
	V_RETURN(_postProcessingDown2->create(pd3dDevice, _frameWidth / 2, _frameHeight / 2));
	_postProcessingSun = new PostProcessing;
	V_RETURN(_postProcessingSun->create(pd3dDevice, _frameHeight / 2, _frameHeight / 2));
	_postProcessingFinal = new PostProcessing;
	V_RETURN(_postProcessingFinal->create(pd3dDevice, _frameWidth, _frameHeight));

	V_RETURN(_sprite->create(pd3dDevice));
	//create water
	_water = new Water;
	V_RETURN(_water->create(pd3dDevice,nullptr));


	//load Meshinfos
	std::vector<ConfigParser::MeshInstanceProperties> groundInstances = _cfgParser->getGroundInstanceObjects();
	for (std::vector<ConfigParser::MeshInstanceProperties>::iterator it = groundInstances.begin(); it != groundInstances.end(); ++it) {
		if (it->name.compare(L"grass_medium") == 0){
			for (int x = -_grassResW / 16; x < _grassResW / 16; ++x){
				for (int y = -_grassResD / 16; y < _grassResD / 16; ++y){
					ConfigParser::ObjectData groundData;
					groundData.name = it->name;

					groundData.rotation.x = 0;
					float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
					groundData.rotation.y = 360 * r;
					groundData.rotation.z = 0;
					groundData.translation.x = x * 8;
					groundData.translation.y = g_terrain.getHeight((float)(x * 8) / (_grassResW / 2) / 2 + 0.5, (float)(y * 8) / (_grassResD / 2) / 2 + 0.5)*_cfgParser->getTerrainHeight();
					//groundData.translation.y += 20;
					groundData.translation.z = y * 8;
					groundData.scale = 0.3;
					_cfgParser->applyTransformation(&groundData);
					if (groundData.translation.y < 110)
						_groundInstanceObjects.push_back(groundData);
				}
			}
		}
		else if (it->name.compare(L"redwood_01") == 0){
			for (int x = 0; x < 4; ++x) {
				for (int y = 0; y < 4; ++y) {
					if (_treeTemplate(x, y) > 1.5) {
						for (int xi = 0; xi < 4; ++xi) {
							for (int yi = 0; yi < 4; ++yi) {
								if (_treeTemplate(xi, yi) > 1.5) {
									int a = (_grassResW / 4 * x) + (_grassResW / 32 * xi) -_grassResW / 2;
									int b = (_grassResW / 4 * y) + (_grassResW / 32 * yi) -_grassResW / 2;
									ConfigParser::ObjectData groundData;
									groundData.name = it->name;

									groundData.rotation.x = 0;
									float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
									groundData.rotation.y = 360 * r;
									groundData.rotation.z = 0;
									groundData.translation.x = a;
									groundData.translation.y = g_terrain.getHeight((float)(a) / (_grassResW / 2) / 2 + 0.5, (float)(b) / (_grassResD / 2) / 2 + 0.5)*_cfgParser->getTerrainHeight();
									groundData.translation.z = b;
									groundData.scale = 2;
									_cfgParser->applyTransformation(&groundData);
									if (groundData.translation.y < 110)
										_groundInstanceObjects.push_back(groundData);
								}
							}
						}
					}
				}
			}
		}
	}


	//add ememy instances

	// Initialize the camera
	float offset = 30.0f;
	vEye = XMVectorSet(0.0f, g_terrain.getMiddle() * (_cfgParser->getTerrainHeight()) + offset , 0.0f, 0.0f);   // Camera eye is here
	vAt = XMVectorSet((_cfgParser->getTerrainDepth()), (_cfgParser->getTerrainHeight()), (_cfgParser->getTerrainWidth()), 1.0f);  //look at the edge of the terrain  // ... facing at this position
	g_camera.SetViewParams(vEye, vAt); // http://msdn.microsoft.com/en-us/library/windows/desktop/bb206342%28v=vs.85%29.aspx
	g_camera.SetScalers(g_cameraRotateScaler, g_cameraMoveScaler);
    return S_OK;
}


//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	UNREFERENCED_PARAMETER(pUserContext);

    g_dialogResourceManager.OnD3D11DestroyDevice();
    g_settingsDlg.OnD3D11DestroyDevice();
    DXUTGetGlobalResourceCache().OnDestroyDevice();
    //SAFE_RELEASE( g_terrainVertexLayout );
    
	//destroy input layout
	Mesh::destroyInputLayout();

	// Destroy the terrain
	g_terrain.destroy();

	// Destroy the meshes
	for (std::map<std::wstring, Mesh*>::iterator it = meshes.begin(); it != meshes.end(); ++it)
	{
		it->second->destroy();
	}

	_RenderTexture->destroy();
	_RenderShadowMap->destroy();
	_renderSkyMap->destroy();
	_postProcessing->destroy();
	_postProcessing2->destroy();
	_postProcessingDown->destroy();
	_postProcessingDown2->destroy();
	_postProcessingSun->destroy();
	_postProcessingFinal->destroy();
	_water->destroy();
	_sprite->destroy();

    SAFE_DELETE( g_txtHelper );
    ReleaseShader();
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
        const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	UNREFERENCED_PARAMETER(pSwapChain);
	UNREFERENCED_PARAMETER(pUserContext);

    HRESULT hr;
    
    // Intialize the user interface

    V_RETURN( g_dialogResourceManager.OnD3D11ResizedSwapChain( pd3dDevice, pBackBufferSurfaceDesc ) );
    V_RETURN( g_settingsDlg.OnD3D11ResizedSwapChain( pd3dDevice, pBackBufferSurfaceDesc ) );

    g_hud.SetLocation( pBackBufferSurfaceDesc->Width - 170, 0 );
    g_hud.SetSize( 170, 170 );
    g_sampleUI.SetLocation( pBackBufferSurfaceDesc->Width - 170, pBackBufferSurfaceDesc->Height - 300 );
    g_sampleUI.SetSize( 170, 300 );

    // Initialize the camera

    g_cameraParams.aspect = pBackBufferSurfaceDesc->Width / ( FLOAT )pBackBufferSurfaceDesc->Height;
    g_cameraParams.fovy = 0.785398f;
    g_cameraParams.nearPlane = 1.f;
    g_cameraParams.farPlane = 5000.f;

    g_camera.SetProjParams(g_cameraParams.fovy, g_cameraParams.aspect, g_cameraParams.nearPlane, g_cameraParams.farPlane);
	g_camera.SetEnablePositionMovement(false);
	g_camera.SetRotateButtons(false, false, false,true);
	g_camera.SetScalers( g_cameraRotateScaler, g_cameraMoveScaler );
	g_camera.SetDrag( true );



    return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{
	UNREFERENCED_PARAMETER(pUserContext);
    g_dialogResourceManager.OnD3D11ReleasingSwapChain();
}

//--------------------------------------------------------------------------------------
// Loads the effect from file
// and retrieves all dependent variables
//--------------------------------------------------------------------------------------
HRESULT ReloadShader(ID3D11Device* pd3dDevice)
{
    assert(pd3dDevice != NULL);

    HRESULT hr;

    ReleaseShader();
	V_RETURN(g_gameEffect.create(pd3dDevice));
	V_RETURN(_depthShader.create(pd3dDevice));
	V_RETURN(_waterShader.create(pd3dDevice));
	V_RETURN(_spriteShader.create(pd3dDevice));
	if (_postProcessing)
		_postProcessing->ReloadShader(pd3dDevice);
	if (_postProcessing2)
		_postProcessing2->ReloadShader(pd3dDevice);
	if (_postProcessingDown)
		_postProcessingDown->ReloadShader(pd3dDevice);
	if (_postProcessingDown2)
		_postProcessingDown2->ReloadShader(pd3dDevice);
	if (_postProcessingSun)
		_postProcessingSun->ReloadShader(pd3dDevice);
	if (_postProcessingFinal)
		_postProcessingFinal->ReloadShader(pd3dDevice);
    return S_OK;
}

//--------------------------------------------------------------------------------------
// Release resources created in ReloadShader
//--------------------------------------------------------------------------------------
void ReleaseShader()
{
	g_gameEffect.destroy();
	_depthShader.destroy();
	_pp.destroy();
	_waterShader.destroy();
	_spriteShader.destroy();
}

//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing,
                          void* pUserContext )
{
	UNREFERENCED_PARAMETER(pUserContext);

    // Pass messages to dialog resource manager calls so GUI state is updated correctly
    *pbNoFurtherProcessing = g_dialogResourceManager.MsgProc( hWnd, uMsg, wParam, lParam );
    if( *pbNoFurtherProcessing )
        return 0;

    // Pass messages to settings dialog if its active
    if( g_settingsDlg.IsActive() )
    {
        g_settingsDlg.MsgProc( hWnd, uMsg, wParam, lParam );
        return 0;
    }

    // Give the dialogs a chance to handle the message first
    *pbNoFurtherProcessing = g_hud.MsgProc( hWnd, uMsg, wParam, lParam );
    if( *pbNoFurtherProcessing )
        return 0;
    *pbNoFurtherProcessing = g_sampleUI.MsgProc( hWnd, uMsg, wParam, lParam );
    if( *pbNoFurtherProcessing )
        return 0;
        
    // Use the mouse weel to control the movement speed
    if(uMsg == WM_MOUSEWHEEL) {
        int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
        g_cameraMoveScaler *= (1 + zDelta / 500.0f);
        if (g_cameraMoveScaler < 0.1f)
          g_cameraMoveScaler = 0.1f;
        g_camera.SetScalers(g_cameraRotateScaler, g_cameraMoveScaler);
    }

    // Pass all remaining windows messages to camera so it can respond to user input
    g_camera.HandleMessages( hWnd, uMsg, wParam, lParam );

    return 0;
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
	UNREFERENCED_PARAMETER(nChar);
	UNREFERENCED_PARAMETER(bKeyDown);
	UNREFERENCED_PARAMETER(bAltDown);
	UNREFERENCED_PARAMETER(pUserContext);
	if (nChar == 'C' && bKeyDown) {
		if (!camCanMove){
			g_camera.SetEnablePositionMovement(true);
			camCanMove = true;
		}
		else {
			g_camera.SetEnablePositionMovement(false);
			g_camera.SetViewParams(vEye, vAt); //reset position and orientation of Camera
			camCanMove = false;
		}
	}
	if (nChar == 'R') {
		if (bKeyDown)
			leftPressed = true;
		else leftPressed = false;
		//cout << "left pressed\n";
	}
	if (nChar == 'F'){
		if (bKeyDown)
			rightPressed = true; 
		else rightPressed = false;
	}
	if (nChar == 'T'){
		if (bKeyDown)
			_missilePressed = true;
		else _missilePressed = false;
	}
	if (nChar == ' '){
		if (bKeyDown) _playerThrust = true;
		else _playerThrust = false;
	}
}

//--------------------------------------------------------------------------------------
// Handles the GUI events
//--------------------------------------------------------------------------------------
void CALLBACK OnGUIEvent( UINT nEvent, int nControlID, CDXUTControl* pControl, void* pUserContext )
{
	UNREFERENCED_PARAMETER(nEvent);
	UNREFERENCED_PARAMETER(pControl);
	UNREFERENCED_PARAMETER(pUserContext);

    switch( nControlID )
    {
        case IDC_TOGGLEFULLSCREEN:
            DXUTToggleFullScreen(); break;
        case IDC_TOGGLEREF:
            DXUTToggleREF(); break;
        case IDC_CHANGEDEVICE:
            g_settingsDlg.SetActive( !g_settingsDlg.IsActive() ); break;
        case IDC_TOGGLESPIN:
            g_terrainSpinning = g_sampleUI.GetCheckBox( IDC_TOGGLESPIN )->GetChecked();
            break;
		case IDC_TOGGLELIGHTSPIN:
			g_lightSpinning = g_sampleUI.GetCheckBox(IDC_TOGGLELIGHTSPIN)->GetChecked();
			break;
		case IDC_RELOAD_SHADERS:
			ReloadShader(DXUTGetD3D11Device ());
			break;
    }
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene.  This is called regardless of which D3D API is used
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double fTime, float fElapsedTime, void* pUserContext )
{
	//enemy spawn
	if (timeforSpawnLeft - fElapsedTime <= 0){
		//(max - min) * ((((float)rand()) / (float)RAND_MAX)) + min;

		//get Infos
		std::map<std::wstring, ConfigParser::EnemyData> enemyData = _cfgParser->getEnemieTypes();
		ConfigParser::SpawnData spawndata = _cfgParser->getSpawnData();
		std::vector<std::wstring> EnemyNames = _cfgParser->getEnemyNames();

		//Calculate spawn points
		timeforSpawnLeft = (spawndata.minTime - spawndata.maxTime) * (((float)rand()) / (float)RAND_MAX) + spawndata.minTime;
		int enemyType = (EnemyNames.size()) * (((float)rand()) / (float)RAND_MAX);
		int x = 0, z = 0;
		float y;
		while (sqrt(x*x + z*z) <= spawndata.minRange){
			x = ((_cfgParser->getTerrainDepth())* ((float)rand() / (float)RAND_MAX)) - _cfgParser->getTerrainDepth() /2;
			z = ((_cfgParser->getTerrainWidth())* ((float)rand() / (float)RAND_MAX)) - _cfgParser->getTerrainWidth() /2 ;
		}
		y = ((spawndata.maxHeigh - spawndata.minHeigh) * (((float)rand()) / (float)RAND_MAX) + spawndata.minHeigh);
		XMFLOAT3 position = XMFLOAT3(x,y, z);
		while (sqrt(x*x + z*z) >= spawndata.minApproach){
			x = 2 * spawndata.minApproach * ((float)rand() / (float)RAND_MAX) - spawndata.minApproach;
			z = 2 * spawndata.minApproach * ((float)rand() / (float)RAND_MAX) - spawndata.minApproach;
		}
		y = ((spawndata.maxHeigh - spawndata.minHeigh) * (((float)rand()) / (float)RAND_MAX) + spawndata.minHeigh);
		XMFLOAT3 position2 = XMFLOAT3(x,y ,z);


		//Set Values
		Missile::EnemieInstance temp;
		temp.typeName = EnemyNames[enemyType];
		temp.pos = position;
		temp.dir = XMFLOAT3((-position.x + position2.x), (-position.y + position2.y), (-position.z + position2.z));
		float divider = sqrt(temp.dir.x*temp.dir.x + temp.dir.y*temp.dir.y + temp.dir.z*temp.dir.z);
		temp.dir.x /= (divider / enemyData[temp.typeName].speed);
		temp.dir.y /= (divider / enemyData[temp.typeName].speed);
		temp.dir.z /= (divider / enemyData[temp.typeName].speed);
		temp.health = enemyData[temp.typeName].health;
		temp.isDead = false;
		_enemieInstances.emplace_back(temp);
	}
	else {
		timeforSpawnLeft -= fElapsedTime;
	}
	//update enemy position and delete enemys out of bounds
	for (auto i = _enemieInstances.begin(); i != _enemieInstances.end();){
		if (i->isDead) i->dir.y -= 10 * fElapsedTime;//apply gravety to dead enemies
		i->pos.x += i->dir.x * fElapsedTime;
		i->pos.y += i->dir.y * fElapsedTime;
		i->pos.z += i->dir.z * fElapsedTime;
		if (i->pos.x > _cfgParser->getTerrainWidth() / 2 || i->pos.z > _cfgParser->getTerrainDepth() / 2 ||
			i->pos.x < -_cfgParser->getTerrainWidth() / 2 || i->pos.z < -_cfgParser->getTerrainDepth() / 2 ||
			i->pos.y<0){
			//std::cout << "Deleted" << i->pos.x << " | " << i->pos.z << "\n";
			i=_enemieInstances.erase(i);
		}
		else  ++i;
	}

	//update projectiles
	ConfigParser::WeaponData leftData = _cfgParser->getLeftWeapon();
	//ConfigParser::WeaponData rightData = _cfgParser->getRightWeapon();
	for (auto it = leftProjectiles.begin(); it != leftProjectiles.end();){
		//delete projectiles out of bounds
		if (it->sprite.position.x < -(_cfgParser->getTerrainWidth() / 2) || (_cfgParser->getTerrainWidth() / 2) < it->sprite.position.x ||
			it->sprite.position.y < 0 || 600 < it->sprite.position.y ||
			it->sprite.position.z < -(_cfgParser->getTerrainWidth() / 2) || (_cfgParser->getTerrainWidth() / 2) < it->sprite.position.z
			){
			leftProjectiles.erase(it);
			//cout << "left deleted\n";
		}
		else{
			it->dir.y -= leftData.g*fElapsedTime*0.003;
			XMVECTOR pos = XMLoadFloat4(&it->sprite.position) + XMLoadFloat4(&it->dir)*leftData.speed*fElapsedTime;
			XMStoreFloat4(&it->sprite.position, pos);
			++it;
			//cout << "left moved\n";
		}
	}


	//spawn projectiles
	if (leftPressed){
		//cout << "left try, time: " << fTime << "last: "<<leftLastFired<<"\n";
		if ((fTime - leftLastFired) > leftData.cd){
			leftLastFired = fTime;
			//transform spawn pos to weapon
			XMVECTOR spawnPosShift = XMVectorSet(leftData.spawnPos.x, leftData.spawnPos.y, leftData.spawnPos.z,1);
			spawnPosShift = XMVector4Transform(spawnPosShift, g_camera.GetWorldMatrix());
			XMFLOAT4 pos;
			XMStoreFloat4(&pos, spawnPosShift);
			SpriteRenderer::SpriteVertex v;
			v.position.x = pos.x;
			v.position.y = pos.y;
			v.position.z = pos.z;
			v.position.w = 1.0f;
			v.radius = leftData.r;
			v.textureIndex = leftData.spriteIndex;
			v.alpha = 1;
			ProjectileData p;
			p.sprite = v;
			XMStoreFloat4(&p.dir, calcSpread(XMVector3Normalize(g_camera.GetWorldAhead()), 1.2f));
			leftProjectiles.emplace_back(p);
			//cout << "left spawned\n";
		}
	}

	//collision gatling and missile
	std::map<std::wstring, ConfigParser::EnemyData> enemyTypes = _cfgParser->getEnemieTypes();
	ConfigParser::WeaponData weapon = _cfgParser->getLeftWeapon();
	for (auto it = _enemieInstances.begin(); it != _enemieInstances.end();){
		for (auto itp = leftProjectiles.begin(); itp != leftProjectiles.end();){
			float dx = it->pos.x - itp->sprite.position.x;
			float dy = it->pos.y - itp->sprite.position.y;
			float dz = it->pos.z - itp->sprite.position.z;
			float dist = sqrt(dx*dx+dy*dy+dz*dz);
			if (dist<(enemyTypes[it->typeName].size+itp->sprite.radius)){//collision
				it->health -= weapon.dmg;
				leftProjectiles.erase(itp);
				//spawn FLAK
				Particle particle(2, 22, 0.6, 10, 1,XMVectorSet(itp->sprite.position.x, itp->sprite.position.y, itp->sprite.position.z, 0),fTime);
				_particles.emplace_back(particle);
			}
			else ++itp;
		}
		for (auto itm = _missiles.begin(); itm != _missiles.end();){
			float dx = it->pos.x - itm->getPos().x;
			float dy = it->pos.y - itm->getPos().y;
			float dz = it->pos.z - itm->getPos().z;
			float dist = sqrt(dx*dx + dy*dy + dz*dz);
			if (dist<(enemyTypes[it->typeName].size)){//collision
				it->health -= 160;
				_missiles.erase(itm);
				//spawn FLAK
				Particle particle(2, 22, 0.6, 24, 1, XMVectorSet(itm->getPos().x, itm->getPos().y, itm->getPos().z, 0), fTime);
				_particles.emplace_back(particle);
			}
			else ++itm;
		}
		if (it->health <= 0&&!it->isDead){
			//spawn explosion
			spawnExplosion(*it, fTime);
			it->isDead = true;
			++it;// it = _enemieInstances.erase(it);
		}
		else ++it;
	}
	//collision beam (line-sphere collision)
	if (rightPressed){
		weapon = _cfgParser->getRightWeapon();
		XMFLOAT4 beamStart;
		beamStart.x = weapon.spawnPos.x;
		beamStart.y = weapon.spawnPos.y;
		beamStart.z = weapon.spawnPos.z;
		beamStart.w = 1;
		XMVECTOR start = XMLoadFloat4(&beamStart), dir = g_camera.GetWorldAhead();
		start=XMVector4Transform(start,g_camera.GetWorldMatrix());
		for (auto it = _enemieInstances.begin(); it != _enemieInstances.end();){
			XMVECTOR center = XMVectorSet(it->pos.x, it->pos.y, it->pos.z, 0);
			float t =XMVectorGetByIndex( XMVector3Dot(center-start, dir),0);
			if (t < 0){//behind gun
				++it;
				continue;
			}
			XMVECTOR nearPoint=t*dir+start;
			float dist = XMVectorGetByIndex(XMVector3Length(center - nearPoint), 0); 
			if (dist < enemyTypes[it->typeName].size){
				it->health -= weapon.dmg*fElapsedTime;//beam DPS *fElapsedTime
				//cout << "beam hit, dmg: " << weapon.dmg*fElapsedTime <<"\n";
			}
			if (it->health <= 0 && !it->isDead){
				//spawn explosion
				spawnExplosion(*it, fTime);
				it->isDead = true;
				++it;// it = _enemieInstances.erase(it);
			}
			else ++it;
		}
	}
	//update particles
	vector<Particle> _spawnedParticles;
	for (auto it = _particles.begin(); it != _particles.end();){
		if (it->isNextSpawn(fTime)){
			Particle p = it->getNextSpawn(fTime);
			_spawnedParticles.emplace_back(p);
		}
		it->update(fTime);
		if (it->isDead()) _particles.erase(it);
		else ++it;
	}
	//update missiles
	for (auto it = _missiles.begin(); it != _missiles.end();){
		it->update(fTime, fElapsedTime);
		if (it->getIsDead()){
			_missiles.erase(it);
			//spawn FLAK
			Particle particle(2, 22, 0.6, 24, 1, XMVectorSet(it->getPos().x, it->getPos().y, it->getPos().z, 0), fTime);
			_particles.emplace_back(particle);
		}
		else{
			if (it->getParticle().isNextSpawn(fTime)){
				Particle p = it->getParticle().getNextSpawn(fTime);
				_spawnedParticles.emplace_back(p);
			}
			++it;
		};
	}
	for (auto it = _spawnedParticles.begin(); it != _spawnedParticles.end(); ++it){
		//cout << "emplacing spawned\n";
		_particles.emplace_back(*it);
	}

	//spawn missile
	Missile::EnemieInstance* enemy=nullptr;
	float lastT=10000;
	if (_missilePressed&&fTime - _missileLastFired>2){
		XMVECTOR start = g_camera.GetEyePt(), dir = g_camera.GetWorldAhead();
		for (auto it = _enemieInstances.begin(); it != _enemieInstances.end(); ++it){
			XMVECTOR center = XMVectorSet(it->pos.x, it->pos.y, it->pos.z, 0);
			float t = XMVectorGetByIndex(XMVector3Dot(center - start, dir), 0);
			if (t < 0){//behind gun
				continue;
			}
			XMVECTOR nearPoint = t*dir + start;
			float dist = XMVectorGetByIndex(XMVector3Length(center - nearPoint), 0);
			if (dist < 20){
				if (t < lastT){
					lastT = t;
					enemy = &(*it);
				}
			}
		}
		if (enemy){
			_missileLastFired = fTime;
			//cout << "target acquired " << "\n";
			Particle missileEffect(0, 1, 6, 3.5, 1, start, fTime);
			missileEffect.setSpawner(4, 20, 2, 1, 2.4, 0.6, 0.015);
			missileEffect.setSpawnEndFade(0.1, 1.8, 0.2);
			Missile missile(250.0f, 120.0f, XMVectorSet(30, 65, -60, 0), XMVectorSet(0, 1, 0, 0), enemy, missileEffect, 6, fTime);
			_missiles.emplace_back(missile);
		}
	}
	UNREFERENCED_PARAMETER(pUserContext);


    // Update the camera's position (player) based on user input 
	XMVECTOR playerDir = g_camera.GetWorldUp();
	float speed = 0;
	if (_playerThrust){
		speed = _playerSpeed*fElapsedTime;
	}
	_playerMovement = _playerMovement + playerDir*speed + XMVectorSet(0, -1, 0, 0)*_playerG*fElapsedTime;
	//keep player in bounds
	XMFLOAT3 pos;
	XMStoreFloat3(&pos, g_camera.GetEyePt()+_playerMovement);
	if (pos.y < 0){ 
		_playerMovement = XMVectorSet(0, 0, 0, 0);
		pos.y = 0;
		//g_camera.SetViewParams(XMLoadFloat3(&pos), g_camera.GetLookAtPt());
	}//else g_camera.SetViewParams(g_camera.GetEyePt() + _playerMovement, g_camera.GetLookAtPt() + _playerMovement);
	g_camera.FrameMove(fElapsedTime);
	//SetCursorPos(960, 600);
	
    // Initialize the terrain world matrix
    // http://msdn.microsoft.com/en-us/library/windows/desktop/bb206365%28v=vs.85%29.aspx
    
	// Start with identity matrix
	g_terrainWorld = XMMatrixIdentity();
	//apply terrain scaling
	g_terrainWorld *= XMMatrixScaling(_cfgParser->getTerrainWidth()/2, _cfgParser->getTerrainHeight(), _cfgParser->getTerrainDepth()/2);
    
    if( g_terrainSpinning ) 
    {
		// If spinng enabled, rotate the world matrix around the y-axis
        g_terrainWorld *= XMMatrixRotationY(30.0f * DEG2RAD((float)fTime)); // Rotate around world-space "up" axis
		
	}
	

	// Set the light vector
	g_lightDir = XMVectorSet(10, 1, 0, 0); // Direction to the directional light in world space
	if (g_lightSpinning) 
		g_lightDir = XMVector4Transform(g_lightDir, XMMatrixRotationZ(3.0f * DEG2RAD((float)fTime))); // Rotate around world-space "up" axis 
	g_lightDir = XMVector3Normalize(g_lightDir);
	
	//set minimap
	g_minimap = XMMatrixIdentity();
	g_minimap *= XMMatrixScaling(0.1, 0.1, 0.1);
	g_minimap *= XMMatrixTranslationFromVector(g_camera.GetEyePt());
	XMVECTOR minimapPos = XMVectorSet(0.5, -0.4, 1.2,0);
	minimapPos = XMVector4Transform(minimapPos, g_camera.GetWorldMatrix());
	g_minimap *= XMMatrixTranslationFromVector(minimapPos);

	//enemys on minimap
	_minimapEnemy.clear();
	SpriteRenderer::SpriteVertex sv;
	sv.radius = 0.01;
	sv.textureIndex = 0;
	sv.alpha = 1;
	for (auto it = _enemieInstances.begin(); it != _enemieInstances.end(); ++it){
		XMVECTOR pos = XMVectorSet(it->pos.x/400, it->pos.y/200, it->pos.z/400, 1);
		pos *= 0.1;
		pos += minimapPos+g_camera.GetEyePt();
		sv.position.x = XMVectorGetByIndex(pos, 0);
		sv.position.y = XMVectorGetByIndex(pos, 1);
		sv.position.z = XMVectorGetByIndex(pos, 2);
		sv.position.w = 1;
		_minimapEnemy.emplace_back(sv);
	}
}


//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
        float fElapsedTime, void* pUserContext )
{
	
	UNREFERENCED_PARAMETER(pd3dDevice);
	UNREFERENCED_PARAMETER(fTime);
	UNREFERENCED_PARAMETER(pUserContext);

    HRESULT hr;

    // If the settings dialog is being shown, then render it instead of rendering the app's scene
    if( g_settingsDlg.IsActive() )
    {
        g_settingsDlg.OnRender( fElapsedTime );
        return;
    }     

    ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	//float clearColor[4] = {0.5f, 0.5f, 0.5f, 1.0f};
	pd3dImmediateContext->ClearRenderTargetView(pRTV, _skyBlue);
	// Clear the depth stencil
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0, 0);
        
	if (g_gameEffect.effect == NULL || _depthShader.effect == NULL) {
        g_txtHelper->Begin();
        g_txtHelper->SetInsertionPos( 5, 5 );
        g_txtHelper->SetForegroundColor( XMVectorSet( 1.0f, 1.0f, 0.0f, 1.0f ) );
        g_txtHelper->DrawTextLine( L"SHADER ERROR" );
        g_txtHelper->End();
        return;
	}

	//render shadow map
	RenderDShadowMap(pd3dDevice, pd3dImmediateContext, fTime, fElapsedTime, pUserContext);
	//setBackBufferRenderTarget(pd3dImmediateContext);
	V(g_gameEffect.shadowMapEV->SetResource(_RenderShadowMap->GetShaderResourceView()));
    
    // Update variables that change once per frame
    XMMATRIX const view = g_camera.GetViewMatrix(); // http://msdn.microsoft.com/en-us/library/windows/desktop/bb206342%28v=vs.85%29.aspx
    XMMATRIX const proj = g_camera.GetProjMatrix(); // http://msdn.microsoft.com/en-us/library/windows/desktop/bb147302%28v=vs.85%29.aspx
    XMMATRIX worldViewProj = g_terrainWorld * view * proj;
	//XMMATRIX cockpitViewProj = g_cockpit * view * proj;
	XMMATRIX minimapViewProj = g_minimap * view * proj;
	//init frustrum culling
	XMFLOAT4X4 fView, fProj;
	XMStoreFloat4x4(&fView, view);
	XMStoreFloat4x4(&fProj, proj);
	_frustum.ConstructFrustum(5000.f, fView, fProj);

	V(g_gameEffect.viewEV->SetMatrix((float*)&view));
	V(g_gameEffect.projectionEV->SetMatrix((float*)&proj));
	V(g_gameEffect.lightDirEV->SetFloatVector( ( float* )&g_lightDir ));
	V(g_gameEffect.time->SetFloat((float)fTime));//(float)fTime
	V(g_gameEffect.shaderID->SetInt(_cfgParser->getExperimentalShaderID()));
	V(g_gameEffect.cameraPosWorldEV->SetFloatVector((float*)&g_camera.GetEyePt()));
	//terrain
	V(g_gameEffect.worldEV->SetMatrix((float*)&g_terrainWorld));
	V(g_gameEffect.worldViewProjectionEV->SetMatrix((float*)&worldViewProj));
	V(g_gameEffect.worldNormalsMatrix->SetMatrix((float*)&XMMatrixInverse(nullptr, XMMatrixTranspose(g_terrainWorld))));

	//minimap
	V(g_gameEffect.minimapWorldEV->SetMatrix((float*)&g_minimap));
	V(g_gameEffect.minimapViewProjectionEV->SetMatrix((float*)&minimapViewProj));
	V(g_gameEffect.minimapNormalsEV->SetMatrix((float*)&XMMatrixInverse(nullptr, XMMatrixTranspose(g_minimap))));
	//fog
	V(g_gameEffect.fogBegin->SetFloat(_cfgParser->getFogData().begin));
	V(g_gameEffect.fogBeginStrength->SetFloat(_cfgParser->getFogData().beginStrength));
	V(g_gameEffect.fogBeginColor->SetFloatVector((float*)&XMVectorSet(_cfgParser->getFogData().beginColor.x, _cfgParser->getFogData().beginColor.y, _cfgParser->getFogData().beginColor.z, 1)));
	V(g_gameEffect.fogEnd->SetFloat(_cfgParser->getFogData().end));
	V(g_gameEffect.fogEndStrength->SetFloat(_cfgParser->getFogData().endStrength));
	V(g_gameEffect.fogEndColor->SetFloatVector((float*)&XMVectorSet(_cfgParser->getFogData().endColor.x, _cfgParser->getFogData().endColor.y, _cfgParser->getFogData().endColor.z, 1)));

	//grass
	V(g_gameEffect.fObjectHeight->SetFloat(2));
	//water
	V(_waterShader.worldViewProjectionEV->SetMatrix((float*)&worldViewProj));
	V(_waterShader.waterHeight->SetFloat(0.3));
	V(_waterShader.waterTransparency->SetFloat(0.8));
	V(_waterShader.time->SetFloat((float)fTime));
	V(_waterShader.tessAmount->SetInt(64));
	V(_waterShader.waterColor->SetFloatVector((float*)&XMVectorSet(0, 0, 1, 1)));
	//render sky map
	if (_enablePP){
		XMMATRIX vp = view * proj;
		RenderSkyMap(pd3dDevice, pd3dImmediateContext, &vp);
		_RenderTexture->SetRenderTarget(pd3dImmediateContext);
		_RenderTexture->ClearRenderTarget(pd3dImmediateContext, _skyBlue[0], _skyBlue[1], _skyBlue[2]);
	}
	else
		setBackBufferRenderTarget(pd3dImmediateContext);

    // Set input layout
    pd3dImmediateContext->IASetInputLayout( nullptr );//g_terrainVertexLayout
	
	//render terrain
	g_terrain.render(pd3dImmediateContext, g_gameEffect.pass0);
	
	//render cockpit objects
	std::vector<ConfigParser::ObjectData> cockpitObjects = _cfgParser->getCockpitObjects();
	for (std::vector<ConfigParser::ObjectData>::iterator it = cockpitObjects.begin(); it != cockpitObjects.end(); ++it)
	{
		
		//set mesh transformation matrix
		XMMATRIX meshMatrix = XMLoadFloat4x4(&it->tMatrix);
		meshMatrix *= g_camera.GetWorldMatrix();
		
		meshes[it->name]->addInstance(&meshMatrix, &(view * proj));
	}

	//add ground objects
	std::vector<ConfigParser::ObjectData> groundObjects = _cfgParser->getGroundObjects();
	for (auto it = groundObjects.begin(); it != groundObjects.end(); ++it){
		if (_frustum.CheckSphere(it->translation.x, it->translation.y+15, it->translation.z,15))
			meshes[it->name]->addInstance(&XMLoadFloat4x4(&it->tMatrix), &(view * proj));
	}

	//add instanced grass
	for (auto it = _groundInstanceObjects.begin(); it != _groundInstanceObjects.end(); ++it){
		if (_frustum.CheckSphere(it->translation.x, it->translation.y + 2.5, it->translation.z, 2.5)) {
			meshes[it->name]->addInstance(&XMLoadFloat4x4(&it->tMatrix), &(view * proj));
		}
			
	}
	//add all enemies
	std::map<std::wstring, ConfigParser::EnemyData> enemies=_cfgParser->getEnemieTypes();
	for (auto it = _enemieInstances.begin(); it != _enemieInstances.end(); it++){
		ConfigParser::ObjectData enemyObject = enemies[it->typeName].data;
		//if (!_frustum.CheckPoint(enemyObject.translation.x, enemyObject.translation.y, enemyObject.translation.z)) continue;
		//set mesh transformation matrix
		XMMATRIX meshMatrix = XMLoadFloat4x4(&enemyObject.tMatrix);
		//add enemy instance transformation
		float angle = XMVectorGetByIndex(XMVector3AngleBetweenVectors(XMVectorSet(1, 0, 0, 0), XMLoadFloat3(&it->dir)),0);
		XMVECTOR normal= XMVector3Cross(XMVectorSet(1, 0, 0, 0), XMLoadFloat3(&it->dir));
		meshMatrix *= XMMatrixRotationAxis(normal, angle);
		meshMatrix *= XMMatrixTranslation(it->pos.x, it->pos.y, it->pos.z);
		//set enemy mesh instance
		meshes[enemyObject.name]->addInstance(&meshMatrix, &(view * proj));
	}

	//render all instances => this resets the instanceCount to 0!
	for (auto it = meshes.begin(); it != meshes.end(); ++it){
		if (it->first.compare(L"grass_medium")  == 0)
			it->second->render(pd3dImmediateContext, g_gameEffect.grassPass1, g_gameEffect.diffuseEV, g_gameEffect.specularEV, g_gameEffect.glowEV);
		else
			it->second->render(pd3dImmediateContext, g_gameEffect.meshPass1, g_gameEffect.diffuseEV, g_gameEffect.specularEV, g_gameEffect.glowEV);
	}

	//render minimap
	if (_cfgParser->getenableMinimap() == 1)
		g_terrain.render(pd3dImmediateContext, g_gameEffect.minimapPass);

	//render sprites
	//collect all sprites
	//add projectiles
	std::vector<SpriteRenderer::SpriteVertex> sprites; 
	for (auto it = leftProjectiles.begin(); it != leftProjectiles.end(); ++it){
		//if (_frustum.CheckSphere(it->sprite.position.x, it->sprite.position.y, it->sprite.position.z, it->sprite.radius))
		sprites.emplace_back(it->sprite);
	}
	//add particles
	for (auto it = _particles.begin(); it != _particles.end(); ++it){
		SpriteRenderer::SpriteVertex sv = it->getParticle();
		if (_frustum.CheckSphere(sv.position.x, sv.position.y, sv.position.z, sv.radius))
		sprites.emplace_back(it->getParticle());
	}
	//add minimap enemys
	for (auto it = _minimapEnemy.begin(); it != _minimapEnemy.end(); ++it){
		sprites.emplace_back(*it);
	}
	//add missiles
	for (auto it = _missiles.begin(); it != _missiles.end(); ++it){
		sprites.emplace_back(it->getParticle().getParticle());
	}

	//add demo sprites
	SpriteRenderer::SpriteVertex v;
	/*v.position.x = 100;
	v.position.y = 120+sin(fTime)*10;
	v.position.z = 100;
	v.position.w = 1.0f;
	v.radius = 2;
	v.textureIndex = 0;
	//sprites.emplace_back(v);
	v.textureIndex = 1;
	v.radius = 1.5;
	v.position.z = 110;
	v.position.y = 115 + sin(fTime/1.8) * 5;
	sprites.emplace_back(v);*/

	//bind sprite resources
	V(_spriteShader.viewEV->SetMatrix((float*)&view));
	V(_spriteShader.projectionEV->SetMatrix((float*)&proj));
	V(_spriteShader.lightDirEV->SetFloatVector((float*)&g_lightDir));
	V(_spriteShader.time->SetFloat((float)fTime));//(float)fTime
	V(_spriteShader.up->SetFloatVector((float*)&g_camera.GetWorldUp()));
	V(_spriteShader.right->SetFloatVector((float*)&g_camera.GetWorldRight()));
	//sort sprites
	sort(sprites.begin(), sprites.end(), spriteCompare);
	//render sprites
	_sprite->renderSprites(pd3dImmediateContext, sprites,_spriteShader.pass0);
	//beam particle
	sprites.clear();
	if (rightPressed){
		//transform spawn pos to weapon
		XMVECTOR spawnPosShift = XMVectorSet(_cfgParser->getRightWeapon().spawnPos.x, _cfgParser->getRightWeapon().spawnPos.y, _cfgParser->getRightWeapon().spawnPos.z, 1);
		spawnPosShift = XMVector4Transform(spawnPosShift, g_camera.GetWorldMatrix());
		XMFLOAT4 pos;
		XMStoreFloat4(&pos, spawnPosShift);
		SpriteRenderer::SpriteVertex beam;
		beam.position.x = pos.x;
		beam.position.y = pos.y;
		beam.position.z = pos.z;
		beam.position.w = 1;
		beam.radius = _cfgParser->getRightWeapon().r;
		beam.textureIndex = _cfgParser->getRightWeapon().spriteIndex;
		sprites.push_back(beam);
		V(_spriteShader.dir->SetFloatVector((float*)&g_camera.GetWorldAhead()));
		V(_spriteShader.length->SetFloat((float)_cfgParser->getRightWeapon().speed));
		_sprite->renderSprites(pd3dImmediateContext, sprites, _spriteShader.pass1beam);
		sprites.clear();
	}
	//add demo sphere sprites
	/*v.position.x = 100;
	v.position.y = 120;
	v.position.z = 100;
	v.position.w = 1.0f;
	v.radius = 3;
	v.textureIndex = 0;
	sprites.emplace_back(v); 
	_sprite->renderSprites(pd3dImmediateContext, sprites, _spriteShader.pass2sphere);
	sprites.clear();*/
	//render water, does not work with render to texture
	//_water->render(pd3dImmediateContext, _waterShader.pass0);
	//post processing
	XMVECTOR sunPos = XMVector4Transform(g_lightDir, XMMatrixIdentity()*view*proj);
	if (XMVectorGetByIndex(sunPos, 2) > 0){//looking to sun
		_postProcessing->setPoint(XMVectorGetByIndex(sunPos, 0) / 2 + 0.5, -XMVectorGetByIndex(sunPos, 1) / 2 + 0.5);
		_postProcessingDown->setPoint(XMVectorGetByIndex(sunPos, 0) / 2 + 0.5, -XMVectorGetByIndex(sunPos, 1) / 2 + 0.5);
		_postProcessingDown2->setPoint(XMVectorGetByIndex(sunPos, 0) / 2 + 0.5, -XMVectorGetByIndex(sunPos, 1) / 2 + 0.5);
		_postProcessingSun->setPoint(XMVectorGetByIndex(sunPos, 0) / 2 + 0.5, -XMVectorGetByIndex(sunPos, 1) / 2 + 0.5);
	}
	else
		_postProcessingDown->setPoint(0.5,0.5);
	if (_enablePP)
		PostProcess(pd3dDevice, pd3dImmediateContext, fTime, fElapsedTime, pUserContext);
	
    DXUT_BeginPerfEvent( DXUT_PERFEVENTCOLOR, L"HUD / Stats" );
    V(g_hud.OnRender( fElapsedTime ));
    V(g_sampleUI.OnRender( fElapsedTime ));
    RenderText();
    DXUT_EndPerfEvent();
	
    static DWORD dwTimefirst = GetTickCount();
    if ( GetTickCount() - dwTimefirst > 5000 )
    {    
        OutputDebugString( DXUTGetFrameStats( DXUTIsVsyncEnabled() ) );
        OutputDebugString( L"\n" );
        dwTimefirst = GetTickCount();
    }
}
void RenderDShadowMap(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime, float fElapsedTime, void* pUserContext){
	HRESULT hr;
	_RenderShadowMap->SetRenderTarget(pd3dImmediateContext);
	_RenderShadowMap->ClearRenderTarget(pd3dImmediateContext);
	//test directional light view
	XMMATRIX lwvp = _RenderShadowMap->getLightWorldViewProj(&g_terrainWorld, &g_lightDir);
	XMMATRIX lvp = _RenderShadowMap->getLightViewProj(&g_lightDir);

	//render terrain
	V(_depthShader.lightDirEV->SetFloatVector((float*)&g_lightDir));
	V(_depthShader.worldEV->SetMatrix((float*)&g_terrainWorld));
	V(_depthShader.worldViewProjectionEV->SetMatrix((float*)&(lwvp)));
	V(g_gameEffect.lightWorldViewProjectionEV->SetMatrix((float*)&(lwvp)));
	V(g_gameEffect.lightMeshViewProjectionEV->SetMatrix((float*)&lvp));


	std::vector<ConfigParser::ObjectData> groundObjects = _cfgParser->getGroundObjects();
	for (auto it = groundObjects.begin(); it != groundObjects.end(); ++it){
		XMMATRIX meshMatrix = XMLoadFloat4x4(&it->tMatrix);
		meshes[it->name]->addInstance(&meshMatrix, &(lvp));
	}
	std::vector<ConfigParser::ObjectData> cockpitObjects = _cfgParser->getCockpitObjects();
	for (std::vector<ConfigParser::ObjectData>::iterator it = cockpitObjects.begin(); it != cockpitObjects.end(); ++it)
	{
		//set mesh transformation matrix
		XMMATRIX meshMatrix = XMLoadFloat4x4(&it->tMatrix);
		meshMatrix *= g_camera.GetWorldMatrix();
		meshes[it->name]->addInstance(&meshMatrix, &(lvp));
	}
	//add instanced trees
	for (auto it = _groundInstanceObjects.begin(); it != _groundInstanceObjects.end(); ++it){
		if (it->name.compare(L"grass_medium")!=0)
			meshes[it->name]->addInstance(&XMLoadFloat4x4(&it->tMatrix), &(lvp));
	}
	//render all instances => this resets the instanceCount to 0!
	for (auto it = meshes.begin(); it != meshes.end(); ++it){
		it->second->renderDepth(pd3dImmediateContext, _depthShader.pMesh);
	}
	g_terrain.render(pd3dImmediateContext, _depthShader.pTerrain);
}
void RenderSkyMap(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, XMMATRIX* viewProj){
	HRESULT hr;
	_renderSkyMap->SetRenderTarget(pd3dImmediateContext);
	_renderSkyMap->ClearRenderTarget(pd3dImmediateContext);
	V(_depthShader.worldViewProjectionEV->SetMatrix((float*)&(g_terrainWorld*(*viewProj))));
	//render cockpit objects
	std::vector<ConfigParser::ObjectData> cockpitObjects = _cfgParser->getCockpitObjects();
	for (std::vector<ConfigParser::ObjectData>::iterator it = cockpitObjects.begin(); it != cockpitObjects.end(); ++it)
	{

		//set mesh transformation matrix
		XMMATRIX meshMatrix = XMLoadFloat4x4(&it->tMatrix);
		meshMatrix *= g_camera.GetWorldMatrix();

		meshes[it->name]->addInstance(&meshMatrix, (viewProj));
	}

	//add ground objects
	std::vector<ConfigParser::ObjectData> groundObjects = _cfgParser->getGroundObjects();
	for (auto it = groundObjects.begin(); it != groundObjects.end(); ++it){
		if (_frustum.CheckSphere(it->translation.x, it->translation.y + 15, it->translation.z, 15))
			meshes[it->name]->addInstance(&XMLoadFloat4x4(&it->tMatrix), viewProj);
	}

	//add instanced grass
	for (auto it = _groundInstanceObjects.begin(); it != _groundInstanceObjects.end(); ++it){
		if (_frustum.CheckSphere(it->translation.x, it->translation.y + 2.5, it->translation.z, 2.5))
			if (it->name.compare(L"grass_medium") != 0)
				meshes[it->name]->addInstance(&XMLoadFloat4x4(&it->tMatrix), viewProj);
	}
	//add all enemies
	std::map<std::wstring, ConfigParser::EnemyData> enemies = _cfgParser->getEnemieTypes();
	for (auto it = _enemieInstances.begin(); it != _enemieInstances.end(); it++){
		ConfigParser::ObjectData enemyObject = enemies[it->typeName].data;
		//if (!_frustum.CheckPoint(enemyObject.translation.x, enemyObject.translation.y, enemyObject.translation.z)) continue;
		//set mesh transformation matrix
		XMMATRIX meshMatrix = XMLoadFloat4x4(&enemyObject.tMatrix);
		//add enemy instance transformation
		float angle = XMVectorGetByIndex(XMVector3AngleBetweenVectors(XMVectorSet(1, 0, 0, 0), XMLoadFloat3(&it->dir)), 0);
		XMVECTOR normal = XMVector3Cross(XMVectorSet(1, 0, 0, 0), XMLoadFloat3(&it->dir));
		meshMatrix *= XMMatrixRotationAxis(normal, angle);
		meshMatrix *= XMMatrixTranslation(it->pos.x, it->pos.y, it->pos.z);
		//set enemy mesh instance
		meshes[enemyObject.name]->addInstance(&meshMatrix, viewProj);
	}

	//render terrain
	g_terrain.render(pd3dImmediateContext, _depthShader.pSkyTerrain);
	//render all instances => this resets the instanceCount to 0!
	for (auto it = meshes.begin(); it != meshes.end(); ++it){
		//if (it->first.compare(L"grass_medium") == 0)
			it->second->render(pd3dImmediateContext, _depthShader.pSkyMesh, g_gameEffect.diffuseEV, g_gameEffect.specularEV, g_gameEffect.glowEV);
	}
}
void PostProcess(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime, float fElapsedTime, void* pUserContext){
	//blur sky map
	_postProcessingDown->setRenderToTexture(pd3dImmediateContext);
	_postProcessingDown->setSRV(_renderSkyMap->GetShaderResourceView());
	_postProcessingDown->setTechnique(PostProcessing::POSTPROCESSING_TECHNIQUE_BLUR_HORIZONTAL);
	_postProcessingDown->render(pd3dImmediateContext);
	_postProcessingDown2->setRenderToTexture(pd3dImmediateContext);
	_postProcessingDown2->setSRV(_postProcessingDown->getResultSRV());
	_postProcessingDown2->setTechnique(PostProcessing::POSTPROCESSING_TECHNIQUE_BLUR_VERTICAL);
	_postProcessingDown2->setGamma(1.7);
	_postProcessingDown2->render(pd3dImmediateContext);
	//create sun
	_postProcessingSun->setRenderToTexture(pd3dImmediateContext);
	_postProcessingSun->setSRV(_postProcessingDown2->getResultSRV());
	_postProcessingSun->setTechnique(PostProcessing::POSTPROCESSING_TECHNIQUE_CROP);
	_postProcessingSun->setDimension(0.15, 0.15*g_cameraParams.aspect);
	_postProcessingSun->render(pd3dImmediateContext);
	//blur sun centric
	_postProcessingDown->setRenderToTexture(pd3dImmediateContext);
	_postProcessingDown->setSRV(_postProcessingSun->getResultSRV());
	_postProcessingDown->setTechnique(PostProcessing::POSTPROCESSING_TECHNIQUE_BLUR_CENTRIC);
	_postProcessingDown->setPoint(0.5, 0.5);
	_postProcessingDown->setGamma(1.5);
	_postProcessingDown->render(pd3dImmediateContext);
	//render lens flares
	_postProcessing->setRenderToTexture(pd3dImmediateContext);
	_postProcessing->setSRV(_RenderTexture->GetShaderResourceView());
	_postProcessing->setSkySRV(_postProcessingDown->getResultSRV());
	_postProcessing->setTechnique(PostProcessing::POSTPROCESSING_TECHNIQUE_LENSFLARE);
	_postProcessing->render(pd3dImmediateContext);
	//final render to back buffer
	setBackBufferRenderTarget(pd3dImmediateContext);
	_postProcessingFinal->setSRV(_postProcessing->getResultSRV());
	_postProcessingFinal->setSkySRV(_renderSkyMap->GetShaderResourceView());
	_postProcessingFinal->setTechnique(PostProcessing::POSTPROCESSING_TECHNIQUE_IDENTITY);
	_postProcessingFinal->render(pd3dImmediateContext);
}
void setBackBufferRenderTarget(ID3D11DeviceContext* pd3dImmediateContext){
	DXUTSetupD3D11Views(pd3dImmediateContext);
}
//weapon functions


XMVECTOR calcSpread(XMVECTOR dir, float maxAngle){
	//random angle
	float angle = (rand() % (int)(maxAngle * 100)) / 100.0f;
	//cout << "rand angle: " << angle << endl;
	//random axis in x-y plane [-5,5]
	float x, y;
	x = (rand() % 1000) / 100 - 5;
	y = (rand() % 1000) / 100 - 5;
	XMVECTOR axis= XMVectorSet(x,y,0,0);
	//transform rotation axis
	axis=XMVector4Transform(axis,g_camera.GetWorldMatrix());
	//transform direction
	dir = XMVector4Transform(dir, XMMatrixRotationAxis(axis, DEG2RAD( angle)));
	return dir;
}
//random 3D (x,y,z,0) direction normalized
XMVECTOR randomDir(){
	float x, y, z;
	x = (rand() / (float)RAND_MAX) * 2 - 1;
	y = (rand() / (float)RAND_MAX) * 2 - 1;
	z = (rand() / (float)RAND_MAX) * 2 - 1;
	XMVECTOR v = XMVectorSet(x, y, z, 0);
	return XMVector3Normalize(v);
}
XMVECTOR randomDir2D(){
	float x, y;
	x = (rand() / (float)RAND_MAX) * 2 - 1;
	y = (rand() / (float)RAND_MAX) * 2 - 1;
	XMVECTOR v = XMVectorSet(x, y, 0, 0);
	return XMVector2Normalize(v);
}
//random float [0,1]
float randomFloat(float min, float max){
	return (rand() / (float)RAND_MAX)*(max - min) + min;
}
void spawnExplosion(Missile::EnemieInstance enemy, float fTime){
	++_score;
	XMVECTOR pos = XMVectorSet(enemy.pos.x, enemy.pos.y, enemy.pos.z, 0);
	XMVECTOR dir = XMVectorSet(enemy.dir.x, enemy.dir.y, enemy.dir.z, 0);
	float sizeFactor = _cfgParser->getEnemieTypes()[enemy.typeName].size/10;
	//main explosion
	Particle p(2, 22, 1.3, 35 * sizeFactor, 1, pos, dir*0.8, fTime);
	_particles.emplace_back(p);
	//shockwave
	Particle p5(1, 1, 0.6, 0.1, 0.4, pos,dir, fTime);
	p5.setEndFade(0.01, 600, 0.1);
	_particles.emplace_back(p5);
	//smaler explosions
	for (int i = 0; i < 8; i++){
		Particle p1(2, 22, randomFloat(0.8, 1.3), randomFloat(10, 20)* sizeFactor, 1, pos + randomDir()*randomFloat(5, 20)* sizeFactor, dir*0.8, fTime);
		_particles.emplace_back(p1);
	}
	//falling particles with smoke
	for (int i = 0; i < 96; i++){
		float r = randomFloat(1, 2);//use a single random for over-all size (radius, lifetime, smoke) of the effect particle
		Particle p3(0, 1, r * 3, r*1.5, 1, pos, dir + randomDir()*randomFloat(20, 36), XMVectorSet(0, -10, 0, 0), fTime);
		p3.setSpawner(4, 20, r*0.3, 1, r*1.8, 0.6, 0.015);
		p3.setSpawnEndFade(0.1, r * 4, 0.2);
		p3.setEndFade(0.3, 0, 0.3);
		_particles.emplace_back(p3);
	}
	//enemy smoke trail
	for (int i = 0; i < 6; i++){
		float r = randomFloat(2, 3)* sizeFactor;
		Particle p2(5, 1, 10, r*0.6, 1, pos + randomDir()*randomFloat(0, 6)* sizeFactor, dir, XMVectorSet(0, -10, 0, 0), fTime);
		p2.setSpawner(6, 18, r*0.5, 1, r*0.6, 0.8, 0.015);
		//p2.setSpawnStartFade(0.8, r*0.6, 0.8);
		p2.setSpawnEndFade(0.2, r * 3,0.4);
		_particles.emplace_back(p2);
	}
	//falling particles without smoke
	for (int i = 0; i < 128 * sizeFactor; i++){
		float r = randomFloat(1, 2);//use a single random for over-all size of the effect particle
		Particle p4(0, 1, r * 2.5, r*1.2, 1,pos, dir*randomFloat(0.5,1) + randomDir()*randomFloat(60, 95), XMVectorSet(0, -10, 0, 0), fTime);
		//particle.setStartFade(0.5, 20, 0);
		p4.setEndFade(0.75, 0.1, 1);
		_particles.emplace_back(p4);
	}
}
bool spriteCompare(const SpriteRenderer::SpriteVertex i, const SpriteRenderer::SpriteVertex j){
	float di = XMVectorGetByIndex(XMVector3Dot(XMVectorSet(i.position.x, i.position.y, i.position.z, 0), g_camera.GetWorldAhead()), 0);
	float dj = XMVectorGetByIndex(XMVector3Dot(XMVectorSet(j.position.x, j.position.y, j.position.z, 0), g_camera.GetWorldAhead()), 0);
	return di > dj;
}
void DeinitApp(){
	for (std::map<std::wstring, Mesh*>::iterator it = meshes.begin(); it != meshes.end(); ++it)
	{
		SAFE_DELETE(it->second);
	}
	meshes.clear();
	SAFE_DELETE(_RenderTexture); 
	SAFE_DELETE(_RenderShadowMap);
	SAFE_DELETE(_renderSkyMap);
	SAFE_DELETE(_postProcessing);
	SAFE_DELETE(_postProcessing2);
	SAFE_DELETE(_postProcessingDown);
	SAFE_DELETE(_postProcessingDown2);
	SAFE_DELETE(_postProcessingSun);
	SAFE_DELETE(_postProcessingFinal);
	SAFE_DELETE(_water);
	SAFE_DELETE(_sprite);
	ShowCursor(true);
}

