#pragma once

#include <d3dx11effect.h>
#include <DDSTextureLoader.h>
#include <math.h>
#include "Particle.h"

class Missile{
public:
	struct EnemieInstance{ std::wstring typeName; XMFLOAT3 pos; XMFLOAT3 dir; float health; bool isDead; };
	Missile();
	Missile(float speed, float turnRate, XMVECTOR pos, XMVECTOR dir, EnemieInstance* target, Particle effect, float lifeTime, float time);
	~Missile();

	void setTarget(EnemieInstance* target);
	void update(float time, float elapsedTime);
	Particle getParticle(){ return _effect; };
	bool getIsDead(){ return _isDead; };
	XMFLOAT3 getPos(){ return _pos; };

private:
#define DEG2RAD( a ) ( (a) * XM_PI / 180.f )
	XMVECTOR getAimPoint();
	float _speed,_turnRate, _startTime, _lifeTime;
	XMFLOAT3 _pos, _dir;
	EnemieInstance* _target;
	Particle _effect;
	bool _isDead=false;
};