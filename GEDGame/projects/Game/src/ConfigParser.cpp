#include "ConfigParser.h"


ConfigParser::ConfigParser()
{
}


ConfigParser::~ConfigParser()
{
}

void ConfigParser::load(std::string file){
	//open file input stream
	std::wifstream in(file,std::wifstream::in);
	
	if (in.is_open()){
		std::wstring key;
		//parse file
		while (in >> key){
			if (key.compare(L"spinning") == 0) in >> spinning;
			else if (key.compare(L"spinSpeed") == 0) in >> spinSpeed;
			else if (key.compare(L"backgroundColor") == 0) in >> backgroundColor.r >> backgroundColor.g >> backgroundColor.b;
			else if (key.compare(L"TerrainPath") == 0) in >> _terrainPath.height >> _terrainPath.color>>_terrainPath.normal;
			else if (key.compare(L"TerrainDimension") == 0) in >> terranWidth >> terranDepth >> terranHeight;
			else if (key.compare(L"ExperimentalShaderID") == 0) in >> experimentalShaderID;
			else if (key.compare(L"EnableMinimap") == 0) in >> enableMinimap;
			else if (key.compare(L"EnablePostProcessing") == 0) in >> enablePostProcessing;
			else if (key.compare(L"ShadowMapResolution") == 0) in >> shadowMapRes;
			else if (key.compare(L"Fog") == 0) in >> fogData.begin >> fogData.beginColor.x >> fogData.beginColor.y >> fogData.beginColor.z >> fogData.beginStrength 
				>> fogData.end >> fogData.endColor.x >> fogData.endColor.y >> fogData.endColor.z >> fogData.endStrength;
			else if (key.compare(L"Resolution") == 0) in >> _resX >> _resY;
			else if (key.compare(L"Mesh") == 0) {
				std::wstring name;
				in >> name;
				MeshData meshData;
				in >> meshData.low >> meshData.diffuse >> meshData.specualr >> meshData.glow;
				//rework Meshdata
				if (meshData.diffuse.compare(L"-") != 0)
					meshData.diffuse = L"resources/" + meshData.diffuse;
				if (meshData.low.compare(L"-") != 0)
					meshData.low = L"resources/" + meshData.low;
				if (meshData.glow.compare(L"-") != 0)
					meshData.glow = L"resources/" + meshData.glow;
				if (meshData.specualr.compare(L"-") != 0)
					meshData.specualr = L"resources/" + meshData.specualr;
				_Meshes.emplace(name, meshData);
				//in >> _cockpit.name >> _cockpit.low >> _cockpit.diffuse >> _cockpit.specualr >> _cockpit.glow;
			}
			else if (key.compare(L"CockpitObject") == 0) {
				ObjectData objectData;
				in >> objectData.name;
				in >> objectData.rotation.x >> objectData.rotation.y >> objectData.rotation.z;
				in >> objectData.translation.x >> objectData.translation.y >> objectData.translation.z;
				in >> objectData.scale;
				applyTransformation(&objectData);
				_cockpitObjects.push_back(objectData);
			}
			else if (key.compare(L"GroundObject") == 0) {
				ObjectData objectData;
				in >> objectData.name;
				in >> objectData.rotation.x >> objectData.rotation.y >> objectData.rotation.z;
				in >> objectData.translation.x >> objectData.translation.y >> objectData.translation.z;
				in >> objectData.scale;
				applyTransformation(&objectData);
				/*in >> objectData.name;
				in >> objectData.rotation.x >> objectData.rotation.y >> objectData.rotation.z;
				in >> objectData.translation.x >> objectData.translation.y >> objectData.translation.z;
				in >> objectData.scale;*/
				_groundObjects.push_back(objectData);
			}
			else if (key.compare(L"GroundInstanceObject") == 0) {
				MeshInstanceProperties objectData;
				in >> objectData.name;
				in >> objectData.count;
				_groundInstanceObjects.push_back(objectData);
			}
			else if (key.compare(L"EnemyType") == 0) {
				EnemyData enemyData;
				std::wstring name;
				in >> name >> enemyData.health >> enemyData.size >> enemyData.speed;
				in >> enemyData.data.name;
				enemyNames.push_back(name);
				in >> enemyData.data.rotation.x >> enemyData.data.rotation.y >> enemyData.data.rotation.z;
				in >> enemyData.data.translation.x >> enemyData.data.translation.y >> enemyData.data.translation.z;
				in >> enemyData.data.scale;
				applyTransformation(&enemyData.data);
				_enemies.emplace(name, enemyData);
			}
			else if (key.compare(L"Spawn") == 0) {
				in >> spawnData.minTime >> spawnData.maxTime >> spawnData.minHeigh >> spawnData.maxHeigh >> spawnData.minApproach >> spawnData.minRange;
			}
			else if (key.compare(L"Sprite") == 0){
				std::wstring filename;
				in >> filename;
				filename = L"resources/" + filename;
				spriteFiles.push_back(filename);
			}
			else if (key.compare(L"LeftWeapon") == 0){
				in >> leftWeapon.spawnPos.x >> leftWeapon.spawnPos.y >> leftWeapon.spawnPos.z >>
					leftWeapon.speed >> leftWeapon.g >> leftWeapon.cd >> leftWeapon.dmg >>
					leftWeapon.spriteIndex >> leftWeapon.r;
			}
			else if (key.compare(L"RightWeapon") == 0){
				in >> rightWeapon.spawnPos.x >> rightWeapon.spawnPos.y >> rightWeapon.spawnPos.z >>
					rightWeapon.speed >> rightWeapon.g >> rightWeapon.cd >> rightWeapon.dmg >>
					rightWeapon.spriteIndex >> rightWeapon.r;
			}
		}
	}
	//close file input stream
	in.close();
	
}

void ConfigParser::applyTransformation(ConfigParser::ObjectData* d){
	XMMATRIX matrix = XMMatrixIdentity();
	matrix *= XMMatrixScaling(d->scale, d->scale, d->scale);
	matrix *= XMMatrixRotationX(XMConvertToRadians(d->rotation.x));
	matrix *= XMMatrixRotationY(XMConvertToRadians(d->rotation.y));
	matrix *= XMMatrixRotationZ(XMConvertToRadians(d->rotation.z));
	matrix *= XMMatrixTranslation(d->translation.x, d->translation.y, d->translation.z);
	XMFLOAT4X4 sMatrix;
	XMStoreFloat4x4(&sMatrix, matrix);
	d->tMatrix = sMatrix;
}

//getter
float ConfigParser::getSpinning(){ return spinning; }
float ConfigParser::getSpinSpeed(){ return spinSpeed; }
int ConfigParser::getResX(){ return _resX; }
int ConfigParser::getResY(){ return _resY; }
ConfigParser::Color ConfigParser::getBackgroundColor(){ return backgroundColor; }
ConfigParser::terrainPath ConfigParser::getTerrainPath(){ return _terrainPath; }
ConfigParser::FogData ConfigParser::getFogData(){ return fogData; }
float ConfigParser::getTerrainWidth(){ return terranWidth; }
float ConfigParser::getTerrainDepth(){ return terranDepth; }
float ConfigParser::getTerrainHeight(){ return terranHeight; }
int ConfigParser::getenableMinimap(){ return enableMinimap; }
int ConfigParser::getenablePostProcessing(){ return enablePostProcessing; }
int ConfigParser::getExperimentalShaderID(){ return experimentalShaderID; }
//ConfigParser::MeshData ConfigParser::getcockpitMeshData(){ return _cockpit; }
std::map<std::wstring, ConfigParser::MeshData> ConfigParser::getMeshMap() { return _Meshes; }
std::vector<ConfigParser::ObjectData> ConfigParser::getCockpitObjects(){ return _cockpitObjects; }
std::vector<ConfigParser::ObjectData> ConfigParser::getGroundObjects(){ return _groundObjects; }
std::vector<ConfigParser::MeshInstanceProperties> ConfigParser::getGroundInstanceObjects(){ return _groundInstanceObjects; }
std::map<std::wstring, ConfigParser::EnemyData> ConfigParser::getEnemieTypes() { return _enemies; }
int ConfigParser::getShadowMapRes(){ return shadowMapRes; }