#pragma once
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <DirectXMath.h>
/* 
	parse programm parameters from game.cfg
	see franzer\GEDGame\projects\Game\cfg_layout_guide.txt for more information
*/
using namespace DirectX;
class ConfigParser
{
public:

	struct Color { float r; float g; float b; };
	struct Vector3{ float x, y, z; };
	struct terrainPath { std::wstring height; std::wstring color; std::wstring normal; };
	struct MeshData { std::wstring low; std::wstring diffuse; std::wstring specualr; std::wstring glow; };
	struct MeshInstanceProperties{
		std::wstring name;//mesh name
		int count;
	};

	//struct for object transformations and mesh name
	//struct ObjectData{ std::wstring name;  Vector3 rotation; Vector3 translation; float scale; };
	struct ObjectData{ std::wstring name;  Vector3 rotation; Vector3 translation; float scale; XMFLOAT4X4 tMatrix; };
	//struct TransData{Vector3 rotation; Vector3 translation; float scale; };
	struct EnemyData{ int health; float size; float speed; ObjectData data; };
	struct FogData{ float begin; Vector3 beginColor; float beginStrength; float end; Vector3 endColor; float endStrength; };
	struct SpawnData{ float minTime; float maxTime; float minHeigh; float maxHeigh; float minApproach; float minRange; };
	struct WeaponData{ Vector3 spawnPos; float speed; float g; float cd; float dmg; unsigned int spriteIndex; float r; };
	ConfigParser();
	~ConfigParser();
	void load(std::string file);

	//getter
	float getSpinning();
	float getSpinSpeed();
	Color getBackgroundColor();
	terrainPath getTerrainPath();
	FogData getFogData();
	SpawnData getSpawnData(){ return spawnData; };
	//MeshData getcockpitMeshData();
	std::map<std::wstring, MeshData> getMeshMap();
	std::vector<ObjectData> getCockpitObjects();
	std::vector<ObjectData> getGroundObjects();
	std::vector<MeshInstanceProperties> getGroundInstanceObjects();
	std::map<std::wstring, EnemyData> getEnemieTypes();
	std::vector<std::wstring> getEnemyNames(){ return enemyNames; };
	std::vector<std::wstring> getSpriteFiles(){ return spriteFiles; };
	float getTerrainWidth();
	float getTerrainDepth();
	float getTerrainHeight();
	int getenableMinimap();
	int getenablePostProcessing();
	int getExperimentalShaderID();
	int getShadowMapRes();
	void applyTransformation(ObjectData* d);
	int getResX();
	int getResY();
	WeaponData getLeftWeapon(){ return leftWeapon; };
	WeaponData getRightWeapon(){ return rightWeapon; };

private:


	float spinning;
	float spinSpeed;
	int _resX, _resY;
	Color backgroundColor;
	terrainPath _terrainPath;
	FogData fogData;
	SpawnData spawnData;
	//MeshData _cockpit;
	std::map<std::wstring, MeshData> _Meshes;
	std::vector<ObjectData> _cockpitObjects;
	std::vector<ObjectData> _groundObjects;
	std::vector<MeshInstanceProperties> _groundInstanceObjects;
	std::map<std::wstring, EnemyData> _enemies;
	std::vector<std::wstring> enemyNames;
	std::vector<std::wstring> spriteFiles;
	WeaponData leftWeapon;
	WeaponData rightWeapon;
	float terranWidth;
	float terranDepth;
	float terranHeight;
	int enableMinimap;
	int enablePostProcessing;
	int experimentalShaderID;
	int shadowMapRes;
};

