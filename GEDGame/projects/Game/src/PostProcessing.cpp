#include "PostProcessing.h"

PostProcessing::PostProcessing() : 
_inputSRV(nullptr),
_skySRV(nullptr), 
_additionalSRVs(nullptr),
_renderTexture(nullptr),
_vertexBuffer(nullptr),
_inputLayout(nullptr)
{}
PostProcessing::~PostProcessing(){}

HRESULT PostProcessing::create(ID3D11Device* device, int textureWidth, int textureHeight){
	HRESULT hr;
	_width = textureWidth; _height = textureHeight;
	//create render to texture
	_renderTexture = new RenderToTexture;
	V(_renderTexture->create(device, textureWidth, textureHeight));

	//create plane vertex buffer
	float x = (float)_width / 2, y = (float)_height / 2;
	// This buffer contains positions and texture coordinates
	float triangle[] = {
		// Vertex 0
		-x, y,0.0f, 1.0f, // Position
		0.0f, 0.0f,       // Texcoords
		// Vertex 1
		x, -y, 0.0f, 1.0f, // Position
		1.0f, 1.0f,       // Texcoords
		// Vertex 2
		-x, -y, 0.0f, 1.0f, // Position
		0.0f, 1.0f,       // Texcoords
		// Vertex 3
		-x, y,0.0f, 1.0f, // Position
		0.0f, 0.0f,       // Texcoords
		// Vertex 4
		x, y, 0.0f, 1.0f, // Position
		1.0f, 0.0f,       // Texcoords
		// Vertex 5
		x, -y, 0.0f, 1.0f, // Position
		1.0f, 1.0f,       // Texcoords
	};
	D3D11_SUBRESOURCE_DATA id;
	id.pSysMem = &triangle[0];
	id.SysMemPitch = 6 * sizeof(float); // Stride
	id.SysMemSlicePitch = 0;

	D3D11_BUFFER_DESC bd;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.ByteWidth = sizeof(triangle); //The size in bytes of the triangle array
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;

	V(device->CreateBuffer(&bd, &id, &_vertexBuffer));
	V(ReloadShader(device));
	V(createInputLayout(device));
	return S_OK;
}
HRESULT PostProcessing::render(ID3D11DeviceContext* deviceContext){
	HRESULT hr;
	// Bind the terrain vertex buffer to the input assembler stage 
	ID3D11Buffer* vbs[] = { _vertexBuffer,};
	unsigned int strides[] = { 6*sizeof(float),}, offsets[] = { 0,};
	deviceContext->IASetVertexBuffers(0, 1, vbs, strides, offsets);
	// Tell the input assembler stage which primitive topology to use
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	deviceContext->IASetInputLayout(_inputLayout);
	//vp matrix
	XMMATRIX const view = DirectX::XMMatrixLookAtLH(XMVectorSet(0, 0, 0, 0), XMVectorSet(0, 0, 1, 1), XMVectorSet(0, 1, 0, 0));
	XMMATRIX const proj = DirectX::XMMatrixOrthographicLH(_width, _height, -1.f, 1.f);
	XMMATRIX wvp = DirectX::XMMatrixIdentity()*view*proj;
	V(_pp.worldViewProjectionEV->SetMatrix((float*)&wvp));
	V(_pp._width->SetInt(_width));
	V(_pp._height->SetInt(_height));
	//set resources
	V(_pp.inputEV->SetResource(_inputSRV));
	V(_pp.skyEV->SetResource(_skySRV));
	V(_pp.pointX->SetFloat(_pointX));
	V(_pp.pointY->SetFloat(_pointY));
	V(_pp.gamma->SetFloat(_gamma));
	//set Pass
	ID3DX11EffectPass* pass;
	switch (_technique){
	case POSTPROCESSING_TECHNIQUE_IDENTITY:
		pass = _pp.passIdentity;
		break;
	case POSTPROCESSING_TECHNIQUE_GREYSCALE :
		pass = _pp.passGreyscale;
		break;
	case POSTPROCESSING_TECHNIQUE_LENSFLARE:
		pass = _pp.passLensFlare;
		break;
	case POSTPROCESSING_TECHNIQUE_BLUR_HORIZONTAL:
		//V(_pp._width->SetInt(_width/4));
		pass = _pp.passBlurH;
		break;
	case POSTPROCESSING_TECHNIQUE_BLUR_VERTICAL:
		//V(_pp._height->SetInt(_height/4));
		pass = _pp.passBlurV;
		break;
	case POSTPROCESSING_TECHNIQUE_BLUR_CENTRIC:
		pass = _pp.passBlurC;
		break;
	case POSTPROCESSING_TECHNIQUE_CROP:
		V(_pp._width->SetFloat(_dX));
		V(_pp._height->SetFloat(_dY));
		pass = _pp.passCrop;
		break;
	default:
		pass = _pp.passIdentity;
		break;
	}
	V(pass->Apply(0, deviceContext));

	deviceContext->Draw(6,0);//render

	return S_OK;
}
HRESULT PostProcessing::createInputLayout(ID3D11Device* device){
	HRESULT hr;
	//create input layout
	ID3DX11EffectPass* pass = _pp.passIdentity;
	// Define the input layout, including world, worldViewProjection and normal matrix
	const D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);
	// Create the input layout
	D3DX11_PASS_DESC pd;
	V_RETURN(pass->GetDesc(&pd));
	V_RETURN(device->CreateInputLayout(layout, numElements, pd.pIAInputSignature, pd.IAInputSignatureSize, &_inputLayout));
	return S_OK;
}
HRESULT PostProcessing::ReloadShader(ID3D11Device* pd3dDevice){
	assert(pd3dDevice != NULL);
	HRESULT hr;
	_pp.destroy();
	V_RETURN(_pp.create(pd3dDevice));
	return S_OK;
}
void PostProcessing::destroy(){
	//SAFE_RELEASE(_inputSRV);
	SAFE_RELEASE(_vertexBuffer);
	SAFE_RELEASE(_additionalSRVs);
	SAFE_RELEASE(_inputLayout);
	_renderTexture->destroy();
	SAFE_DELETE(_renderTexture);
	_pp.destroy();
}

//setter
void PostProcessing::setTechnique(unsigned int technique){ _technique = technique; }
void PostProcessing::setPoint(float x, float y){ _pointX = x; _pointY = y; }
void PostProcessing::setDimension(float x, float y){ _dX = x; _dY = y; }
void PostProcessing::setGamma(float g){ _gamma = g; }
void PostProcessing::setSRV(ID3D11ShaderResourceView* inputSRV){ _inputSRV = inputSRV; }
void PostProcessing::setSkySRV(ID3D11ShaderResourceView* skySRV){ _skySRV = skySRV; }
void PostProcessing::setRenderToTexture(ID3D11DeviceContext* deviceContext){
	//set render target
	_renderTexture->SetRenderTarget(deviceContext);
	_renderTexture->ClearRenderTarget(deviceContext);
}

//getter
ID3D11ShaderResourceView* PostProcessing::getResultSRV(){ return _renderTexture->GetShaderResourceView(); }