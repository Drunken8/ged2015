#pragma once

#include <string>
#include <vector>

#include <DXUT.h>
#include <DXUTcamera.h>

#include <d3dx11effect.h>
#include <DDSTextureLoader.h>

#include "SpriteEffect.h"


using namespace DirectX;

class SpriteRenderer
{
public:
	struct SpriteVertex
	{
		XMFLOAT4 position;     // world-space position (sprite center)
		float radius;                   // world-space radius (= half side length of the sprite quad)
		int textureIndex;               // which texture to use (out of SpriteRenderer::m_spriteSRV)
		float alpha;
	};
	// Constructor: Create a SpriteRenderer with the given list of textures.
	// The textures are *not* be created immediately, but only when create is called!
	SpriteRenderer(const std::vector<std::wstring>& textureFilenames);
	// Destructor does nothing. Destroy and ReleaseShader must be called first!
	~SpriteRenderer();

	// Create all required D3D resources (textures, buffers, ...).
	HRESULT create(ID3D11Device* device);
	// Release D3D resources again.
	void destroy();

	// Render the given sprites. They must already be sorted into back-to-front order.
	void renderSprites(ID3D11DeviceContext* context, const std::vector<SpriteVertex>& sprites, ID3DX11EffectPass* pass);

private:
	std::vector<std::wstring> _textureFilenames;

	// Sprite textures and corresponding shader resource views.
	std::vector<ID3D11ShaderResourceView*> _spriteSRV;

	// Maximum number of allowed sprites, i.e. size of the vertex buffer.
	size_t _spriteCountMax;
	// Vertex buffer for sprite vertices, and corresponding input layout.
	ID3D11Buffer* _vertexBuffer;
	ID3D11InputLayout* _inputLayout;
};
