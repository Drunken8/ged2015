#pragma once
#include <stdio.h>
#include <tchar.h>

#include <windows.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdint>
#include <list>


#include "dxut.h"
#include "DXUTmisc.h"
#include "DXUTcamera.h"
#include "DXUTgui.h"
#include "DXUTsettingsDlg.h"
#include "SDKmisc.h"

#include "d3dx11effect.h"

#include "Terrain.h"
#include "GameEffect.h"
#include "Depth.h"

#include "debug.h"
#include "ConfigParser.h"
#include "Mesh.h"
#include "RenderToTexture.h"
#include "Frustum.h"
#include "PostProcessing.h"
#include "Water.h"
#include "SpriteRenderer.h"
#include "SpriteEffect.h"
#include "Particle.h"
#include "Missile.h"

using namespace std;
using namespace DirectX;

// Help macros
#define DEG2RAD( a ) ( (a) * XM_PI / 180.f )

//--------------------------------------------------------------------------------------
// Forward declarations 
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing,
	void* pUserContext);
void CALLBACK OnKeyboard(UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext);
void CALLBACK OnGUIEvent(UINT nEvent, int nControlID, CDXUTControl* pControl, void* pUserContext);
void CALLBACK OnFrameMove(double fTime, float fElapsedTime, void* pUserContext);
bool CALLBACK ModifyDeviceSettings(DXUTDeviceSettings* pDeviceSettings, void* pUserContext);

bool CALLBACK IsD3D11DeviceAcceptable(const CD3D11EnumAdapterInfo *, UINT, const CD3D11EnumDeviceInfo *,
	DXGI_FORMAT, bool, void*);
HRESULT CALLBACK OnD3D11CreateDevice(ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc,
	void* pUserContext);
HRESULT CALLBACK OnD3D11ResizedSwapChain(ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
	const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext);
void CALLBACK OnD3D11ReleasingSwapChain(void* pUserContext);
void CALLBACK OnD3D11DestroyDevice(void* pUserContext);
void CALLBACK OnD3D11FrameRender(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
	float fElapsedTime, void* pUserContext);

//--------------------------------------------------------------------------------------
// UI control IDs
//--------------------------------------------------------------------------------------
#define IDC_TOGGLEFULLSCREEN    1
#define IDC_TOGGLEREF           2
#define IDC_CHANGEDEVICE        3
#define IDC_TOGGLESPIN          4
#define IDC_TOGGLELIGHTSPIN     5
#define IDC_RELOAD_SHADERS		101

	void RenderText();
	void RenderDShadowMap(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
		float fElapsedTime, void* pUserContext);
	void PostProcess(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
		float fElapsedTime, void* pUserContext);
	void RenderSkyMap(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,XMMATRIX* viewProj);

	void InitApp();
	void DeinitApp();
	void ReleaseShader();
	void setBackBufferRenderTarget(ID3D11DeviceContext* pd3dImmediateContext);
	HRESULT ReloadShader(ID3D11Device* pd3dDevice);
	//--------------------------------------------------------------------------------------
	// Global variables
	//--------------------------------------------------------------------------------------
	//struct EnemieInstance{ std::wstring typeName; XMFLOAT3 pos; XMFLOAT3 dir; float health; bool isDead; };
	std::map<std::wstring, Mesh*> meshes;
	std::vector<ConfigParser::ObjectData> _groundInstanceObjects;
	std::list<Missile::EnemieInstance> _enemieInstances;



	// Camera
	struct CAMERAPARAMS {
		float   fovy;
		float   aspect;
		float   nearPlane;
		float   farPlane;
	}                                       g_cameraParams;
	float                                   g_cameraMoveScaler = 1000.f;
	float                                   g_cameraRotateScaler = 0.01f;
	float									_frameWidth = 1280;
	float									_frameHeight = 720;
	int										_shadowMapRes=1024;
	int										_grassResW = 800;
	int										_grassResD = 800;
	CFirstPersonCamera                      g_camera;               // A first person camera
	bool									camCanMove = false;
	XMVECTOR								vEye;
	XMVECTOR								vAt;

	// User Interface
	CDXUTDialogResourceManager              g_dialogResourceManager; // manager for shared resources of dialogs
	CD3DSettingsDlg                         g_settingsDlg;          // Device settings dialog
	CDXUTTextHelper*                        g_txtHelper = NULL;
	CDXUTDialog                             g_hud;                  // dialog for standard controls
	CDXUTDialog                             g_sampleUI;             // dialog for sample specific controls

	//ID3D11InputLayout*                      g_terrainVertexLayout; // Describes the structure of the vertex buffer to the input assembler stage


	bool                                    g_terrainSpinning = false;
	XMMATRIX                                g_terrainWorld; // object- to world-space transformation
	//XMMATRIX                                g_cockpit; // cockpit transformation
	XMMATRIX								g_minimap; //minimap transformation

	bool                                    g_lightSpinning = false;
	bool _enablePP=false;

	// Scene information
	XMVECTOR                                g_lightDir;
	Terrain									g_terrain;
	ConfigParser*							_cfgParser;

	Frustum									_frustum;
	GameEffect								g_gameEffect; // CPU part of Shader
	DepthShader _depthShader;
	PPEffect _pp;
	WaterEffect _waterShader;
	SpriteEffect _spriteShader;
	RenderToTexture* _RenderTexture;
	RenderToTexture* _RenderShadowMap;
	RenderToTexture* _renderSkyMap;
	PostProcessing* _postProcessing;
	PostProcessing* _postProcessing2;
	PostProcessing* _postProcessingDown;
	PostProcessing* _postProcessingDown2;
	PostProcessing* _postProcessingSun;
	PostProcessing* _postProcessingFinal;
	Water* _water;
	SpriteRenderer* _sprite;

	//Player stats
	bool _playerThrust=false;
	XMVECTOR _playerMovement;
	float _playerSpeed=1.2;
	float _playerG = 0.5;

	int _score=0;
	//weapon stats
	struct ProjectileData{ XMFLOAT4 dir; SpriteRenderer::SpriteVertex sprite; };
	bool leftPressed = false, rightPressed = false, _missilePressed=false;
	float leftLastFired = 0, rightLastFired = 0, _missileLastFired=0;
	vector<ProjectileData> leftProjectiles, rightProjectiles;
	vector<Missile> _missiles;
	//weapon functions
	XMVECTOR calcSpread(XMVECTOR dir, float maxAngle);
	bool spriteCompare(const SpriteRenderer::SpriteVertex i, const SpriteRenderer::SpriteVertex j);
	//particles
	vector<Particle> _particles;
	//explosoion
	void spawnExplosion(Missile::EnemieInstance enemy, float fTime);

	//random
	//ransom float [0,1]
	float randomFloat(float min, float max);
	//random 3D (x,y,z,0) direction normalized
	XMVECTOR randomDir();
	//random 2D (x,y,0,0) direction normalized
	XMVECTOR randomDir2D();

	vector<SpriteRenderer::SpriteVertex> _minimapEnemy;
	XMFLOAT4X4 _treeTemplate;

	float timeforSpawnLeft = -1;

	float _skyBlue[] = { 0.52f, 0.81f, 0.92f, 1.0f };
	

	

	
	
