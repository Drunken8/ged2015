#include "RenderToTexture.h"

RenderToTexture::RenderToTexture() 
	:_renderTargetTexture(nullptr),
	_renderTargetView(nullptr),
	_renderTargetSRV(nullptr),
	_depthStencilView(nullptr){
}
RenderToTexture::~RenderToTexture(){

}

HRESULT RenderToTexture::create(ID3D11Device* device, int textureWidth, int textureHeight){
	_width = textureWidth;
	_height = textureHeight;
	HRESULT hr;

	D3D11_TEXTURE2D_DESC textureDesc;
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;

	// Initialize the render target texture description.
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	// Setup the render target texture description.
	textureDesc.Width = _width;
	textureDesc.Height = _height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	// Create the render target texture.
	V(device->CreateTexture2D(&textureDesc, NULL, &_renderTargetTexture));

	// Setup the description of the render target view.
	renderTargetViewDesc.Format = textureDesc.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;
	// Create the render target view.
	V(device->CreateRenderTargetView(_renderTargetTexture, &renderTargetViewDesc, &_renderTargetView));
	

	// Setup the description of the shader resource view.
	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	// Create the shader resource view.
	V(device->CreateShaderResourceView(_renderTargetTexture, &shaderResourceViewDesc, &_renderTargetSRV));

	//create depth stencil
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	// Initialize the description of the depth buffer.
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

	// Set up the description of the depth buffer.
	depthBufferDesc.Width = _width;
	depthBufferDesc.Height = _width;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	
	// Create the texture for the depth buffer using the filled out description.
	V(device->CreateTexture2D(&depthBufferDesc, NULL, &_depthStencilBuffer));
	/*
	// Initialize the description of the stencil state.
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	// Set up the description of the stencil state.
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;


	// Create the depth stencil state.
	V(device->CreateDepthStencilState(&depthStencilDesc, &_depthStencilState));
	*/




	// Initailze the depth stencil view.
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	// Set up the depth stencil view description.
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view.
	V(device->CreateDepthStencilView(_depthStencilBuffer, &depthStencilViewDesc, &_depthStencilView));
	return S_OK;
}
void RenderToTexture::destroy(){
	SAFE_RELEASE(_renderTargetTexture);
	SAFE_RELEASE(_renderTargetView);
	SAFE_RELEASE(_renderTargetSRV);
	SAFE_RELEASE(_depthStencilBuffer);
	SAFE_RELEASE(_depthStencilView);
	//SAFE_RELEASE(_depthStencilState);
}

void RenderToTexture::SetRenderTarget(ID3D11DeviceContext* deviceContext){
	//set Viewport
	D3D11_VIEWPORT viewport;
	viewport.Width = _width;
	viewport.Height = _height;
	viewport.MinDepth = 0;
	viewport.MaxDepth = 1;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	deviceContext->RSSetViewports(1,&viewport);
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	deviceContext->OMSetRenderTargets(1, &_renderTargetView,_depthStencilView);

}
void RenderToTexture::ClearRenderTarget(ID3D11DeviceContext* deviceContext){
	ClearRenderTarget(deviceContext, 1, 1, 1);
}
void RenderToTexture::ClearRenderTarget(ID3D11DeviceContext* deviceContext, float r, float g, float b){
	float color[4];

	// Setup the color (black) to clear the buffer to.
	color[0] = r;//r
	color[1] = g;//g
	color[2] = b;//b
	color[3] = 1;//a

	// Clear the back buffer.
	deviceContext->ClearRenderTargetView(_renderTargetView, color);

	// Clear the depth buffer.
	deviceContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

}

XMMATRIX RenderToTexture::getLightWorldViewProj(XMMATRIX* world, XMVECTOR* lightDir){
	XMMATRIX const view = DirectX::XMMatrixLookAtLH(XMVectorSet(0, 0, 0, 0), -*lightDir, XMVectorSet(0, 0, 1, 0));
	XMMATRIX const proj = DirectX::XMMatrixOrthographicLH(_width, _height, -500.0f, 500.0f);
	XMMATRIX sworld = XMMatrixIdentity()*XMMatrixScaling((_height / 1024), (_width / 1024), 1);
	return *world*view * proj*sworld;
}

XMMATRIX RenderToTexture::getLightViewProj(XMVECTOR* lightDir){
	XMMATRIX const view = DirectX::XMMatrixLookAtLH(XMVectorSet(0, 0, 0, 0), -*lightDir, XMVectorSet(0, 0, 1, 0));
	XMMATRIX const proj = DirectX::XMMatrixOrthographicLH(_width, _height, -500.0f, 500.0f);
	XMMATRIX sworld = XMMatrixIdentity()* XMMatrixScaling((_height / 1024), (_width / 1024), 1);
	return view * proj*sworld;
}

ID3D11ShaderResourceView* RenderToTexture::GetShaderResourceView(){
	return _renderTargetSRV;
}