#include "Particle.h"

Particle::Particle(){}
Particle::Particle(int texStartIndex, int texCount, float lifeTime, float size, float alpha, XMVECTOR pos, float time){
	_startTime = _lastTime = _lastSpawned = time;
	_lifeTime = lifeTime;
	_loopCount = 1;
	_texStartIndex = texStartIndex;
	_texCount = texCount;
	_size = size;
	XMStoreFloat4(&_particleSprite.position, pos);
	_particleSprite.radius = _size;
	_particleSprite.textureIndex = _texStartIndex;
	XMStoreFloat4(&_dir, XMVectorZero());
	XMStoreFloat4(&_acc, XMVectorZero());
	_particleSprite.alpha = _maxAlpha = alpha;
}
Particle::Particle(int texStartIndex, int texCount, float lifeTime, float size, float alpha, XMVECTOR pos, XMVECTOR dir, float time){
	_startTime = _lastTime = _lastSpawned = time;
	_lifeTime = lifeTime;
	_loopCount = 1;
	_texStartIndex = texStartIndex;
	_texCount = texCount;
	_size = size;
	XMStoreFloat4(&_particleSprite.position, pos);
	_particleSprite.radius = _size;
	_particleSprite.textureIndex = _texStartIndex;
	XMStoreFloat4(&_dir, dir);
	XMStoreFloat4(&_acc, XMVectorZero());
	_particleSprite.alpha = _maxAlpha = alpha;
}
Particle::Particle(int texStartIndex, int texCount, float lifeTime, float size, float alpha, XMVECTOR pos, XMVECTOR dir, XMVECTOR acc, float time){
	_startTime = _lastTime = _lastSpawned = time;
	_lifeTime = lifeTime;
	_loopCount = 1;
	_texStartIndex = texStartIndex;
	_texCount = texCount;
	_size = size;
	XMStoreFloat4(&_particleSprite.position, pos);
	_particleSprite.radius = _size;
	_particleSprite.textureIndex = _texStartIndex;
	XMStoreFloat4(&_dir, dir);
	XMStoreFloat4(&_acc, acc);
	_particleSprite.alpha = _maxAlpha = alpha;
}
Particle::Particle(int texStartIndex, int texCount, float singleLoopTime, float loopCount, float size, float alpha, XMVECTOR pos, XMVECTOR dir, XMVECTOR acc, float time){
	_startTime = _lastTime = _lastSpawned = time;
	_lifeTime = singleLoopTime;
	_loopCount = loopCount;
	_texStartIndex = texStartIndex;
	_texCount = texCount;
	_size = size;
	XMStoreFloat4(&_particleSprite.position, pos);
	_particleSprite.radius = _size;
	_particleSprite.textureIndex = _texStartIndex;
	XMStoreFloat4(&_dir, dir);
	XMStoreFloat4(&_acc, acc);
	_particleSprite.alpha = _maxAlpha = alpha;
}
Particle::~Particle(){
}


void Particle::setSpawner(int texStartIndex, int texCount, float lifeTime, float loopCount, float size, float alpha, float interval){
	_isSpawner = true;
	_spawnTexStartIndex = texStartIndex;
	_spawnTexCount = texCount;
	_spawnInterval = interval;
	_spawnLifeTime = lifeTime;
	_spawnLoopCount = loopCount;
	_spawnSize = size;
	_spawnAlpha = alpha;

}

void Particle::setSpawnStartFade(float fadeEnd, float startSize, float startAlpha){
	_spawnStartFade = true;
	_spawnStartFadeEnd = fadeEnd;
	_spawnStartSize = startSize;
	_spawnStartAlpha = startAlpha; 
}
void Particle::setSpawnEndFade(float fadeStart, float endSize, float endAlpha){
	_spawnEndFade = true;
	_spawnEndFadeStart = fadeStart;
	_spawnEndSize = endSize;
	_spawnEndAlpha = endAlpha;
}
void Particle::setStartFade(float fadeEnd, float startSize, float startAlpha){
	_startFade = true;
	_startFadeEnd = fadeEnd;
	_startSize = startSize;
	_startAlpha = startAlpha;
}

void Particle::setEndFade(float fadeStart, float endSize, float endAlpha){
	_endFade = true;
	_endFadeStart = fadeStart;
	_endSize = endSize;
	_endAlpha = endAlpha;
}
//update pos and texIndex
void Particle::update(float time){
	float dt = time - _startTime;//delta time start - now
	if (_lifeTime*_loopCount < dt){//destroy particel
		_isDead = true;
		return;
	}
	float et = time - _lastTime;//elapsed time last - now
	float lifeP = std::fmod( dt, _lifeTime) / _lifeTime; //
	

	//fade
	_particleSprite.alpha = _maxAlpha;
	_particleSprite.radius = _size;
	//start fade
	if (_startFade){
		if (lifeP <= _startFadeEnd){
			_particleSprite.alpha = (_maxAlpha - _startAlpha)*(lifeP / _startFadeEnd) + _startAlpha;
			_particleSprite.radius = (_size - _startSize)*(lifeP / _startFadeEnd) + _startSize;
		}
	}
	//end fade
	if (_endFade){
		if (lifeP >= _endFadeStart){
			_particleSprite.alpha = _maxAlpha - (_maxAlpha - _endAlpha)*((lifeP-_endFadeStart) / (1-_startFadeEnd));
			_particleSprite.radius = _size - (_size - _endSize)*((lifeP - _endFadeStart) / (1 - _startFadeEnd));
		}
	}

	//movement
	//apply acceleration
	XMStoreFloat4(&_dir, XMLoadFloat4(&_dir) + XMLoadFloat4(&_acc)*et);
	//apply movement
	XMStoreFloat4(&_particleSprite.position, XMLoadFloat4(&_particleSprite.position) + XMLoadFloat4(&_dir)*et);

	//texture
	//update texture
	int i=dt / _lifeTime*_texCount;
	i %= _texCount;//if (_texCount-1 < i)i = _texCount-1;//cap max texture index
	_particleSprite.textureIndex = _texStartIndex + i;
	_lastTime = time;
}
//update pos
void Particle::updatePos(XMVECTOR pos){
	XMStoreFloat4(&_particleSprite.position, pos);
}
//update moving direction
void Particle::updateDir(XMVECTOR dir){
	XMStoreFloat4(&_dir,dir);
}
//update moving direction
void Particle::updateAcc(XMVECTOR acc){
	XMStoreFloat4(&_acc, acc);
}

bool Particle::isNextSpawn(float time){
	if (_isSpawner){
		if ((time - _lastSpawned) > _spawnInterval){
			//_nextSpawn = true;
			return true;
		}
	}
	return false;
}
Particle Particle::getNextSpawn(float time){
	//_nextSpawn = false;
	_lastSpawned = time;
	Particle p = Particle(_spawnTexStartIndex, _spawnTexCount, _spawnLifeTime, _spawnSize, _spawnAlpha, XMLoadFloat4(&_particleSprite.position), time);
	if (_spawnStartFade) p.setStartFade(_spawnStartFadeEnd, _spawnStartSize, _spawnStartAlpha);
	if (_spawnEndFade) p.setEndFade(_spawnEndFadeStart, _spawnEndSize, _spawnEndAlpha);
	return p;
}