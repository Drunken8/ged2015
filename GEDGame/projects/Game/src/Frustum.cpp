#include "Frustum.h"


Frustum::Frustum(){

}
Frustum::~Frustum(){}

void Frustum::ConstructFrustum(float screenDepth, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix){
	float zMinimum, r;
	XMFLOAT4X4 matrix;


	// Calculate the minimum Z distance in the frustum.
	zMinimum = -projectionMatrix._43 / projectionMatrix._33;
	r = screenDepth / (screenDepth - zMinimum);
	projectionMatrix._33 = r;
	projectionMatrix._43 = -r * zMinimum;

	// Create the frustum matrix from the view matrix and updated projection matrix.
	XMStoreFloat4x4(&matrix, XMMatrixMultiply(XMLoadFloat4x4(&viewMatrix), XMLoadFloat4x4(&projectionMatrix)));

	// Calculate near plane of frustum.
	_planes[0].x = matrix._14 + matrix._13;
	_planes[0].y = matrix._24 + matrix._23;
	_planes[0].z = matrix._34 + matrix._33;
	_planes[0].w = matrix._44 + matrix._43;
	XMStoreFloat4(&_planes[0], XMPlaneNormalize(XMLoadFloat4( &_planes[0])));

	// Calculate far plane of frustum.
	_planes[1].x = matrix._14 - matrix._13;
	_planes[1].y = matrix._24 - matrix._23;
	_planes[1].z = matrix._34 - matrix._33;
	_planes[1].w = matrix._44 - matrix._43;
	XMStoreFloat4(&_planes[1], XMPlaneNormalize(XMLoadFloat4(&_planes[1])));

	// Calculate left plane of frustum.
	_planes[2].x = matrix._14 + matrix._11;
	_planes[2].y = matrix._24 + matrix._21;
	_planes[2].z = matrix._34 + matrix._31;
	_planes[2].w = matrix._44 + matrix._41;
	XMStoreFloat4(&_planes[2], XMPlaneNormalize(XMLoadFloat4(&_planes[2])));

	// Calculate right plane of frustum.
	_planes[3].x = matrix._14 - matrix._11;
	_planes[3].y = matrix._24 - matrix._21;
	_planes[3].z = matrix._34 - matrix._31;
	_planes[3].w = matrix._44 - matrix._41;
	XMStoreFloat4(&_planes[3], XMPlaneNormalize(XMLoadFloat4(&_planes[3])));

	// Calculate top plane of frustum.
	_planes[4].x = matrix._14 - matrix._12;
	_planes[4].y = matrix._24 - matrix._22;
	_planes[4].z = matrix._34 - matrix._32;
	_planes[4].w = matrix._44 - matrix._42;
	XMStoreFloat4(&_planes[4], XMPlaneNormalize(XMLoadFloat4(&_planes[4])));

	// Calculate bottom plane of frustum.
	_planes[5].x = matrix._14 + matrix._12;
	_planes[5].y = matrix._24 + matrix._22;
	_planes[5].z = matrix._34 + matrix._32;
	_planes[5].w = matrix._44 + matrix._42;
	XMStoreFloat4(&_planes[5], XMPlaneNormalize(XMLoadFloat4(&_planes[5])));
}

bool Frustum::CheckPoint(float x, float y, float z){

	// Check if the point is inside all six planes of the view frustum.
	for (int i = 0; i<6; i++)
	{
		XMVECTOR plane = XMLoadFloat4(&_planes[i]);
		if (XMVectorGetByIndex(XMPlaneDotCoord(plane, XMVectorSet(x,y,z,1)),0) < 0.0f)
		{
			return false;
		}
	}

	return true;
}
bool Frustum::CheckCube(float xCenter, float yCenter, float zCenter, float radius){
	return true;
}
bool Frustum::CheckSphere(float xCenter, float yCenter, float zCenter, float radius){

	// Check if the radius of the sphere is inside the view frustum.
	for (int i = 0; i<6; i++)
	{
		XMVECTOR plane = XMLoadFloat4(&_planes[i]);
		if (XMVectorGetByIndex(XMPlaneDotCoord(plane, XMVectorSet(xCenter, yCenter, zCenter,1)),0) < -radius)
		{
			return false;
		}
	}

	return true;
}
bool Frustum::CheckRectangle(float xCenter, float yCenter, float zCenter, float xSize, float ySize, float zSize){
	return true;
}