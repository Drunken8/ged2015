#include "Missile.h"

Missile::Missile(){};
Missile::~Missile(){};
Missile::Missile(float speed, float turnRate, XMVECTOR pos, XMVECTOR dir, EnemieInstance* target, Particle effect, float lifeTime, float time){
	_speed = speed;
	_target = target;
	_effect = effect;
	_turnRate = DEG2RAD( turnRate);
	_startTime = time;
	_lifeTime = lifeTime;
	XMStoreFloat3(&_pos, pos);
	XMStoreFloat3(&_dir, dir);
}
void Missile::setTarget(EnemieInstance* target){
}
void Missile::update(float time, float elapsedTime){
	if (time - _startTime > _lifeTime){
		_isDead = true;
		return;
	}
	XMVECTOR pos = XMLoadFloat3(&_pos);
	XMVECTOR dir = XMLoadFloat3(&_dir);
	if (_target){
		XMVECTOR toAimPoint = XMVector3Normalize(getAimPoint() - pos);
		float angle =XMVectorGetX(XMVector3AngleBetweenVectors(dir, toAimPoint));
		if (angle > _turnRate*elapsedTime || angle < DEG2RAD(360) - _turnRate*elapsedTime){
			//rotate dir towards aimpoint with max turnrate
			XMVECTOR axis = XMVector3Cross(dir, toAimPoint);
			dir = XMVectorSetW(dir, 0);
			dir = XMVector4Transform(dir, XMMatrixRotationAxis(axis, _turnRate*elapsedTime));
		}
		else dir = toAimPoint;
		XMStoreFloat3(&_dir, dir);
	}
	//update position
	pos += dir*_speed*elapsedTime;
	XMStoreFloat3(&_pos, pos);
	_effect.updatePos(pos);
	_effect.update(time);
}
XMVECTOR Missile::getAimPoint(){
	XMVECTOR pos = XMLoadFloat3(&_pos);
	XMVECTOR targetPos = XMLoadFloat3(&_target->pos);
	XMVECTOR targetDir = XMLoadFloat3(&_target->dir);
	//distance to target
	XMVECTOR dist = targetPos - pos;
	XMVECTOR delta = (XMVector3Length(dist) / _speed)*targetDir;
	XMVECTOR aimPoint;
	for (int i = 0; i < 15; ++i){
		aimPoint = dist + delta;
		delta = (XMVector3Length(aimPoint) / _speed)*targetDir;
	}
	return dist + delta + pos;
}