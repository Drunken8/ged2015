#pragma once
#include "DXUT.h"
#include "d3dx11effect.h"
#include <d3d11.h>
#include <iostream>
#include "RenderToTexture.h"
#include "PPTechniques.h"
using namespace DirectX;
/*
	post processing render techniques from texture to texture
*/

class PostProcessing{
public:
	PostProcessing();
	~PostProcessing();

	HRESULT create(ID3D11Device* device, int textureWidth, int textureHeight); 
	HRESULT render(ID3D11DeviceContext* deviceContext);
	HRESULT ReloadShader(ID3D11Device* pd3dDevice);
	void destroy();

	//setter
	void setTechnique(unsigned int technique);
	void setPoint(float x, float y);
	void setDimension(float x, float y);
	void setGamma(float g);
	void setSRV(ID3D11ShaderResourceView* inputSRV);
	void setSkySRV(ID3D11ShaderResourceView* skySRV);
	void setAdditionalSRVs(ID3D11ShaderResourceView* additionalSRVs);
	void setRenderToTexture(ID3D11DeviceContext* deviceContext);

	//getter
	ID3D11ShaderResourceView* getResultSRV();

	//techinque constants
	//Identity shader, need: inputSRV
	static const unsigned int POSTPROCESSING_TECHNIQUE_IDENTITY = 0;
	//Horizontal blur shader, need: inputSRV
	static const unsigned int POSTPROCESSING_TECHNIQUE_BLUR_HORIZONTAL = 1;
	//Vertical blur shader, need: inputSRV
	static const unsigned int POSTPROCESSING_TECHNIQUE_BLUR_VERTICAL = 2;
	//Centric blur shader, need: inputSRV, centerPoint
	static const unsigned int POSTPROCESSING_TECHNIQUE_BLUR_CENTRIC = 3;
	//Lensflare shader, need: inputSRV, skyMapSRV, sunPoint
	static const unsigned int POSTPROCESSING_TECHNIQUE_LENSFLARE = 4;
	//Bloom shader, need: inputSRV NOT IMPLEMENTED
	static const unsigned int POSTPROCESSING_TECHNIQUE_BLOOM = 5;
	//Greyscale shader, need: inputSRV
	static const unsigned int POSTPROCESSING_TECHNIQUE_GREYSCALE = 6;
	//Crop shader, need: inputSRV, centerPoint, crop size dimension [0,1]
	static const unsigned int POSTPROCESSING_TECHNIQUE_CROP = 7;

private:

	HRESULT createInputLayout(ID3D11Device* device);

	int _technique=0;
	float _pointX=0.5, _pointY=0.5;
	float _gamma = 1;
	float _width, _height;
	float _dX=1, _dY=1;
	ID3D11ShaderResourceView* _inputSRV;
	ID3D11ShaderResourceView* _skySRV;
	ID3D11ShaderResourceView* _additionalSRVs;
	RenderToTexture* _renderTexture;
	ID3D11Buffer* _vertexBuffer;
	ID3D11InputLayout* _inputLayout;
	//effects
};