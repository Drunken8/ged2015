#include "Mesh.h"

#include "T3d.h"
#include <DDSTextureLoader.h>

ID3D11InputLayout*	Mesh::inputLayout;


Mesh::Mesh(const std::string& filename_t3d,
	const std::string& filename_dds_diffuse,
	const std::string& filename_dds_specular,
	const std::string& filename_dds_glow, UINT methodeMaxInstanceCount)
	:
	//Default values for all other member variables
	vertexBuffer(NULL), indexBuffer(NULL),
	indexCount(0),
	diffuseTex(NULL), diffuseSRV(NULL),
	specularTex(NULL), specularSRV(NULL),
	glowTex(NULL), glowSRV(NULL)
{
	filenameT3d = std::wstring(filename_t3d.begin(), filename_t3d.end());
	filenameDDSDiffuse = std::wstring(filename_dds_diffuse.begin(), filename_dds_diffuse.end());
	filenameDDSSpecular = std::wstring(filename_dds_specular.begin(), filename_dds_specular.end());
	filenameDDSGlow = std::wstring(filename_dds_glow.begin(), filename_dds_glow.end());
	maxInstanceCount = methodeMaxInstanceCount;
	indexCount = curInstanceCount = 0;
	vertexBuffer = indexBuffer = instanceBuffer = NULL;
}


Mesh::Mesh(const std::wstring& filename_t3d,
	const std::wstring& filename_dds_diffuse,
	const std::wstring& filename_dds_specular,
	const std::wstring& filename_dds_glow, UINT methodeMaxInstanceCount)
	: filenameT3d(filename_t3d),
	filenameDDSDiffuse(filename_dds_diffuse),
	filenameDDSSpecular(filename_dds_specular),
	filenameDDSGlow(filename_dds_glow),
	//Default values for all other member variables
	vertexBuffer(NULL), indexBuffer(NULL),
	indexCount(0),
	diffuseTex(NULL), diffuseSRV(NULL),
	specularTex(NULL), specularSRV(NULL),
	glowTex(NULL), glowSRV(NULL)
{
	maxInstanceCount = methodeMaxInstanceCount;
	indexCount = curInstanceCount = 0;
	vertexBuffer = indexBuffer = instanceBuffer = NULL;
}

Mesh::~Mesh(void)
{
}

HRESULT Mesh::create(ID3D11Device* device)
{
	HRESULT hr;
	instances = new InstanceType[maxInstanceCount];
	/*instances[0].world = DirectX::XMMatrixIdentity();
	instances[0].worldViewProj = DirectX::XMMatrixIdentity();
	instances[0].normal = DirectX::XMMatrixIdentity();*/

	//Some variables that we will need
	D3D11_SUBRESOURCE_DATA id = { 0 };
	D3D11_BUFFER_DESC bd = { 0 }, instanceBufferDesc = { 0 };

	//Read mesh
	std::vector<T3dVertex> vertexBufferData;
	std::vector<uint32_t> indexBufferData;

	V(T3d::readFromFile(filenameT3d.c_str(), vertexBufferData, indexBufferData));

	id.pSysMem = &vertexBufferData[0];
	id.SysMemPitch = sizeof(T3dVertex); // Stride
	id.SysMemSlicePitch = 0;

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = vertexBufferData.size() * sizeof(T3dVertex);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	V(device->CreateBuffer(&bd, &id, &vertexBuffer));


	ZeroMemory(&id, sizeof(id));
	ZeroMemory(&bd, sizeof(bd));

	indexCount = indexBufferData.size();
	id.pSysMem = &indexBufferData[0];

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(unsigned int) * indexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	// Define initial data

	//ZeroMemory(&id, sizeof(id));

	// Create Buffer
	V(device->CreateBuffer(&bd, &id, &indexBuffer));

	// INSTANCE-BUFFER
	ZeroMemory(&id, sizeof(id));

	id.pSysMem = instances; // initial data
	id.SysMemPitch = 0;
	id.SysMemSlicePitch = 0;

	instanceBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	instanceBufferDesc.ByteWidth = sizeof(InstanceType) * maxInstanceCount;
	instanceBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	instanceBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; //needed for Map/Unmap
	instanceBufferDesc.MiscFlags = 0;
	instanceBufferDesc.StructureByteStride = 0;


	V(device->CreateBuffer(&instanceBufferDesc, &id, &instanceBuffer));


	if (filenameDDSDiffuse.compare(L"-") != 0)
		// Create textures
		V(DirectX::CreateDDSTextureFromFile(device, const_cast<wchar_t*>(filenameDDSDiffuse.c_str()), nullptr, &diffuseSRV));
		//V(createTexture(device, filenameDDSDiffuse, &diffuseTex, &diffuseSRV));

	if (filenameDDSSpecular.compare(L"-") != 0)
		V(createTexture(device, filenameDDSSpecular, &specularTex, &specularSRV));

	if (filenameDDSGlow.compare(L"-") != 0)
		V(createTexture(device, filenameDDSGlow, &glowTex, &glowSRV));


	return S_OK;
}

void Mesh::destroy()
{
	SAFE_RELEASE(vertexBuffer);
	SAFE_RELEASE(indexBuffer);
	SAFE_RELEASE(diffuseTex);
	SAFE_RELEASE(diffuseSRV);
	SAFE_RELEASE(specularTex);
	SAFE_RELEASE(specularSRV);
	SAFE_RELEASE(glowTex);
	SAFE_RELEASE(glowSRV);
	SAFE_RELEASE(instanceBuffer);
	SAFE_DELETE(instances);
}

HRESULT Mesh::createInputLayout(ID3D11Device* device, ID3DX11EffectPass* pass)
{
	HRESULT hr;

	// Define the input layout, including world, worldViewProjection and normal matrix
	const D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WORLD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WVP", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WVP", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WVP", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WVP", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WNORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WNORMAL", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WNORMAL", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WNORMAL", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);
	// Create the input layout
	D3DX11_PASS_DESC pd;
	V_RETURN(pass->GetDesc(&pd));
	V_RETURN(device->CreateInputLayout(layout, numElements, pd.pIAInputSignature, pd.IAInputSignatureSize, &inputLayout));
	//V(T3d::createT3dInputLayout(device, pass, &inputLayout));
	return S_OK;
}

//pass objectWorld and viewProjection matrix 
bool Mesh::addInstance(const  DirectX::XMMATRIX* objWorld, const DirectX::XMMATRIX* viewProj){
	if (curInstanceCount + 1 > maxInstanceCount) return false;
	XMStoreFloat4x4(&instances[curInstanceCount].world , (*objWorld));
	XMStoreFloat4x4(&instances[curInstanceCount].worldViewProj, (*objWorld) * (*viewProj));
	XMMATRIX normal = DirectX::XMMatrixInverse(NULL, (*objWorld));
	XMStoreFloat4x4(&instances[curInstanceCount].normal , DirectX::XMMatrixTranspose(normal));
	++curInstanceCount;
	return true;
}
//construct objectWorld matrix, pass viewProjection, animation and world matrix
bool Mesh::addInstance(const ConfigParser::ObjectData& prop, const DirectX::XMMATRIX* viewProj, const DirectX::XMMATRIX* anim, const DirectX::XMMATRIX* world){
	if (curInstanceCount + 1 > maxInstanceCount) return false;
	DirectX::XMMATRIX mTrans, mScale, mRotX, mRotY, mRotZ, mWorld, mNormal;
	mWorld = DirectX::XMMatrixIdentity();
	mRotX = DirectX::XMMatrixRotationX(DirectX::XMConvertToRadians(prop.rotation.x));
	mRotY = DirectX::XMMatrixRotationY(DirectX::XMConvertToRadians(prop.rotation.y));
	mRotZ = DirectX::XMMatrixRotationZ(DirectX::XMConvertToRadians(prop.rotation.z));
	mTrans = DirectX::XMMatrixTranslation(prop.translation.x, prop.translation.y, prop.translation.z);
	mScale = DirectX::XMMatrixScaling(prop.scale, prop.scale, prop.scale);
	mWorld *= mScale * mRotX * mRotY * mRotZ * mTrans;
	if (anim != NULL)mWorld = mWorld * (*anim);
	if (world != NULL)mWorld = mWorld * (*world);
	XMStoreFloat4x4(&instances[curInstanceCount].world, mWorld);
	XMStoreFloat4x4(&instances[curInstanceCount].worldViewProj , mWorld * (*viewProj));
	mNormal = DirectX::XMMatrixInverse(NULL, mWorld);
	mNormal = DirectX::XMMatrixTranspose(mNormal);
	XMStoreFloat4x4(&instances[curInstanceCount].normal , mNormal);
	++curInstanceCount;
	return true;
}


void Mesh::destroyInputLayout()
{
	SAFE_RELEASE(inputLayout);
}

HRESULT Mesh::render(ID3D11DeviceContext* context, ID3DX11EffectPass* pass,
	ID3DX11EffectShaderResourceVariable* diffuseEffectVariable,
	ID3DX11EffectShaderResourceVariable* specularEffectVariable,
	ID3DX11EffectShaderResourceVariable* glowEffectVariable)
{
	HRESULT hr;

	if (curInstanceCount == 0) return S_OK;//don't render anything

	D3D11_MAPPED_SUBRESOURCE mappedResource = D3D11_MAPPED_SUBRESOURCE();

	context->Map(instanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource); // lock the instance buffer        
	memcpy((InstanceType*)mappedResource.pData, instances, sizeof(InstanceType) * curInstanceCount); //overwrite instance buffer with new data
	context->Unmap(instanceBuffer, 0); // unlock the instanceBuffer



	if ((diffuseEffectVariable == nullptr) || !diffuseEffectVariable->IsValid())
	{
		throw std::exception("Diffuse EV is null or invalid");
	}
	if ((specularEffectVariable == nullptr) || !specularEffectVariable->IsValid())
	{
		throw std::exception("Diffuse EV is null or invalid");
	}
	if ((glowEffectVariable == nullptr) || !glowEffectVariable->IsValid())
	{
		throw std::exception("Diffuse EV is null or invalid");
	}

	V(diffuseEffectVariable->SetResource(diffuseSRV));
	V(specularEffectVariable->SetResource(specularSRV));
	V(glowEffectVariable->SetResource(glowSRV));

	// Bind the terrain vertex buffer to the input assembler stage 
	ID3D11Buffer* vbs[] = { vertexBuffer, instanceBuffer };
	unsigned int strides[] = { sizeof(T3dVertex), sizeof(InstanceType) }, offsets[] = { 0, 0 };
	context->IASetVertexBuffers(0, 2, vbs, strides, offsets);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Tell the input assembler stage which primitive topology to use
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetInputLayout(inputLayout);

	V(pass->Apply(0, context));

	context->DrawIndexedInstanced(indexCount, curInstanceCount, 0, 0, 0);//render indexed instances

	curInstanceCount = 0; // reset the instance buffer


	//context->DrawIndexed(indexCount, 0, 0);

	return S_OK;

}
HRESULT Mesh::renderDepth(ID3D11DeviceContext* context, ID3DX11EffectPass* pass){
	HRESULT hr;
	if (curInstanceCount == 0) return S_OK;//don't render anything

	D3D11_MAPPED_SUBRESOURCE mappedResource = D3D11_MAPPED_SUBRESOURCE();

	context->Map(instanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource); // lock the instance buffer        
	memcpy((InstanceType*)mappedResource.pData, instances, sizeof(InstanceType) * curInstanceCount); //overwrite instance buffer with new data
	context->Unmap(instanceBuffer, 0); // unlock the instanceBuffer

	// Bind the terrain vertex buffer to the input assembler stage 
	ID3D11Buffer* vbs[] = { vertexBuffer, instanceBuffer };
	unsigned int strides[] = { sizeof(T3dVertex), sizeof(InstanceType) }, offsets[] = { 0, 0 };
	context->IASetVertexBuffers(0, 2, vbs, strides, offsets);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Tell the input assembler stage which primitive topology to use
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetInputLayout(inputLayout);

	V(pass->Apply(0, context));

	context->DrawIndexedInstanced(indexCount, curInstanceCount, 0, 0, 0);//render indexed instances

	curInstanceCount = 0; // reset the instance buffer

	return S_OK;
}

HRESULT Mesh::loadFile(const char * filename, std::vector<uint8_t>& data)
{
	FILE * filePointer = NULL;
	errno_t error = fopen_s(&filePointer, filename, "rb");
	if (error) 	{ return E_INVALIDARG; }
	fseek(filePointer, 0, SEEK_END);
	long bytesize = ftell(filePointer);
	fseek(filePointer, 0, SEEK_SET);
	data.resize(bytesize);
	fread(&data[0], 1, bytesize, filePointer);
	fclose(filePointer);
	return S_OK;
}

HRESULT Mesh::createTexture(ID3D11Device* device, const std::wstring& filename, ID3D11Texture2D** tex,
	ID3D11ShaderResourceView** srv)
{
	HRESULT hr;
	V_RETURN(DirectX::CreateDDSTextureFromFile(device, filename.c_str(), (ID3D11Resource**)tex, srv));

	return S_OK;

}

