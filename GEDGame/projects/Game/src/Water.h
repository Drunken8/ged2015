#pragma once
#include "DXUT.h"
#include "d3dx11effect.h"
#include <d3d11.h>
#include <iostream>
#include "ConfigParser.h"
#include "WaterEffect.h"
using namespace DirectX;

class Water{
public:
	Water();
	~Water();

	HRESULT create(ID3D11Device* device, ConfigParser* configParser);
	void destroy();
	HRESULT render(ID3D11DeviceContext* context, ID3DX11EffectPass* pass);

private:
	ID3D11Buffer*                           _indexBuffer;
	ID3D11Buffer*                           _vertexBuffer;
	ID3D11InputLayout* _inputLayout;
};