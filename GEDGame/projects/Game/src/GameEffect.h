#pragma once

#include "DXUT.h"
#include "d3dx11effect.h"
#include "SDKmisc.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>

// Convenience macros for safe effect variable retrieval
#define SAFE_GET_PASS(Technique, name, var)   {assert(Technique!=NULL); var = Technique->GetPassByName( name );						assert(var->IsValid());}
#define SAFE_GET_TECHNIQUE(effect, name, var) {assert(effect!=NULL); var = effect->GetTechniqueByName( name );						assert(var->IsValid());}
#define SAFE_GET_SCALAR(effect, name, var)    {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsScalar();			assert(var->IsValid());}
#define SAFE_GET_VECTOR(effect, name, var)    {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsVector();			assert(var->IsValid());}
#define SAFE_GET_MATRIX(effect, name, var)    {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsMatrix();			assert(var->IsValid());}
#define SAFE_GET_SAMPLER(effect, name, var)   {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsSampler();			assert(var->IsValid());}
#define SAFE_GET_RESOURCE(effect, name, var)  {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsShaderResource();	assert(var->IsValid());}


struct GameEffect
{
	// A D3DX rendering effect
	ID3DX11Effect*                          effect; // The whole rendering effect
	ID3DX11EffectTechnique*                 technique; // One technique to render the effect
	ID3DX11EffectPass*                      pass0; // One rendering pass of the technique
	ID3DX11EffectMatrixVariable*            viewEV; // World matrix effect variable
	ID3DX11EffectMatrixVariable*            projectionEV; // World matrix effect variable
	ID3DX11EffectMatrixVariable*            worldEV; // World matrix effect variable
	ID3DX11EffectMatrixVariable*            worldViewProjectionEV; // WorldViewProjection matrix effect variable
	ID3DX11EffectShaderResourceVariable*    diffuseEV; // Effect variable for the diffuse color texture
	ID3DX11EffectVectorVariable*            lightDirEV; // Light direction in object space

	ID3DX11EffectShaderResourceVariable*	heightmap;
	ID3DX11EffectShaderResourceVariable*	normalmap;
	ID3DX11EffectScalarVariable*			resolution;
	ID3DX11EffectMatrixVariable*			worldNormalsMatrix;

	ID3DX11EffectShaderResourceVariable* specularEV;
	ID3DX11EffectShaderResourceVariable* glowEV;
	ID3DX11EffectVectorVariable* cameraPosWorldEV;
	ID3DX11EffectPass* meshPass1;
	ID3DX11EffectPass* grassPass1;

	ID3DX11EffectScalarVariable*			time;
	ID3DX11EffectMatrixVariable*            cockpitWorldEV;
	ID3DX11EffectMatrixVariable*			cockpitViewProjectionEV;
	ID3DX11EffectMatrixVariable*			cockpitNormalsEV;
	//experimental
	ID3DX11EffectShaderResourceVariable*	hexagontex;
	ID3DX11EffectScalarVariable*			shaderID;

	//Variables for the minimap
	ID3DX11EffectPass*						minimapPass;
	ID3DX11EffectMatrixVariable*            minimapWorldEV;
	ID3DX11EffectMatrixVariable*			minimapViewProjectionEV;
	ID3DX11EffectMatrixVariable*			minimapNormalsEV;

	//shadow
	ID3DX11EffectShaderResourceVariable*    shadowMapEV;
	ID3DX11EffectMatrixVariable*			lightWorldViewProjectionEV;
	ID3DX11EffectMatrixVariable*			lightMeshViewProjectionEV;

	//fog
	ID3DX11EffectScalarVariable*			fogBegin;
	ID3DX11EffectScalarVariable*			fogBeginStrength;
	ID3DX11EffectVectorVariable*            fogBeginColor;
	ID3DX11EffectScalarVariable*			fogEnd;
	ID3DX11EffectScalarVariable*			fogEndStrength;
	ID3DX11EffectVectorVariable*            fogEndColor;




	ID3DX11EffectScalarVariable*			fObjectHeight;

	GameEffect() { ZeroMemory(this, sizeof(*this)); }		// WARNING: This will set ALL members to 0!


	HRESULT create(ID3D11Device* device)
	{
		HRESULT hr;
		WCHAR path[MAX_PATH];

		// Find and load the rendering effect
		V_RETURN(DXUTFindDXSDKMediaFileCch(path, MAX_PATH, L"shader\\game.fxo"));
		std::ifstream is(path, std::ios_base::binary);
		is.seekg(0, std::ios_base::end);
		std::streampos pos = is.tellg();
		is.seekg(0, std::ios_base::beg);
		std::vector<char> effectBuffer((unsigned int)pos);
		is.read(&effectBuffer[0], pos);
		is.close();
		V_RETURN(D3DX11CreateEffectFromMemory((const void*)&effectBuffer[0], effectBuffer.size(), 0, device, &effect));    
		assert(effect->IsValid());

		// Obtain the effect technique
		SAFE_GET_TECHNIQUE(effect, "Render", technique);

		// Obtain the effect pass
		SAFE_GET_PASS(technique, "P0", pass0);
		SAFE_GET_PASS(technique, "P1_Mesh", meshPass1);
		SAFE_GET_PASS(technique, "P2_Minimap", minimapPass);
		SAFE_GET_PASS(technique, "P3_Grass", grassPass1);

		// Obtain the effect variables
		SAFE_GET_MATRIX(effect, "g_View", viewEV);
		SAFE_GET_MATRIX(effect, "g_Projection", projectionEV);
		SAFE_GET_RESOURCE(effect, "g_Diffuse", diffuseEV);
		SAFE_GET_MATRIX(effect, "g_World", worldEV);
		SAFE_GET_MATRIX(effect, "g_WorldViewProjection", worldViewProjectionEV);   
		SAFE_GET_VECTOR(effect, "g_LightDir", lightDirEV);
		SAFE_GET_RESOURCE(effect, "g_HeightMap", heightmap);
		SAFE_GET_RESOURCE(effect, "g_NormalMap", normalmap);
		SAFE_GET_SCALAR(effect, "g_TerrainRes", resolution);
		SAFE_GET_MATRIX(effect, "g_WorldNormals", worldNormalsMatrix);
		SAFE_GET_SCALAR(effect, "g_Time", time);
		SAFE_GET_RESOURCE(effect, "g_HexagonTex", hexagontex);
		SAFE_GET_SCALAR(effect, "g_ShaderID", shaderID);
		//obtain variables for the cockpit mesh
		SAFE_GET_RESOURCE(effect, "g_DiffuseSpecularEV", specularEV);
		SAFE_GET_RESOURCE(effect, "g_DiffuseGlowEV", glowEV);
		SAFE_GET_VECTOR(effect, "g_CameraPosWorld", cameraPosWorldEV);
		SAFE_GET_MATRIX(effect, "g_CockpitWorld", cockpitWorldEV);
		SAFE_GET_MATRIX(effect, "g_CockpitViewProjection", cockpitViewProjectionEV);
		SAFE_GET_MATRIX(effect, "g_CockpitNormals", cockpitNormalsEV);


		//obtain variables for the minimap
		SAFE_GET_MATRIX(effect, "g_MinimapWorld", minimapWorldEV);
		SAFE_GET_MATRIX(effect, "g_MinimapWorldViewProjection", minimapViewProjectionEV);
		SAFE_GET_MATRIX(effect, "g_MinimapNormals", minimapNormalsEV);

		//shadow
		SAFE_GET_RESOURCE(effect, "_DepthEV", shadowMapEV); 
		SAFE_GET_MATRIX(effect, "_lightWorldViewProjection", lightWorldViewProjectionEV);
		SAFE_GET_MATRIX(effect, "_lightMeshViewProjection", lightMeshViewProjectionEV);

		//fog
		SAFE_GET_SCALAR(effect, "_fogBegin", fogBegin);
		SAFE_GET_SCALAR(effect, "_fogBeginStrength", fogBeginStrength);
		SAFE_GET_VECTOR(effect, "_fogBeginColor", fogBeginColor);
		SAFE_GET_SCALAR(effect, "_fogEnd", fogEnd);
		SAFE_GET_SCALAR(effect, "_fogEndStrength", fogEndStrength);
		SAFE_GET_VECTOR(effect, "_fogEndColor", fogEndColor);

		SAFE_GET_SCALAR(effect, "_fObjectHeight", fObjectHeight);

		return S_OK; 
	}


	void destroy()
	{
		SAFE_RELEASE(effect);
	}
};

extern GameEffect g_gameEffect;