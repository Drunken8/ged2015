#include "Water.h"

Water::Water() :
_indexBuffer(nullptr)
{}

Water::~Water(){}

HRESULT Water::create(ID3D11Device* device, ConfigParser* configParser){
	HRESULT hr;

	//vertex buffer
	float triangle[] = {
		// Vertex 0
		1, 0, -1.0f, 1.0f, // Position
		0.0f, 0.0f,       // Texcoords
		// Vertex 1
		1, 0, 1.0f, 1.0f, // Position
		1.0f, 0.0f,       // Texcoords
		// Vertex 2
		-1, 0, -1.0f, 1.0f, // Position
		0.0f, 1.0f,
	};
	D3D11_SUBRESOURCE_DATA id;
	id.pSysMem = &triangle[0];
	id.SysMemPitch = 6 * sizeof(float); // Stride
	id.SysMemSlicePitch = 0;

	D3D11_BUFFER_DESC bd;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.ByteWidth = sizeof(triangle); //The size in bytes of the triangle array
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;

	V(device->CreateBuffer(&bd, &id, &_vertexBuffer));

	// Fill index buffer
	unsigned int* indexBuffer = new unsigned int[3];//2*(_res-1)*(_res-1) triangles with 3 vertices each
	for (unsigned int i = 0; i < 3; ++i) indexBuffer[i] = i;
	//create index buffer
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(unsigned int) * 3;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	ZeroMemory(&id, sizeof(id));
	id.pSysMem = indexBuffer;
	id.SysMemPitch = 0;
	id.SysMemSlicePitch = 0;

	V(device->CreateBuffer(&bd, &id, &_indexBuffer));
	delete[] indexBuffer;

	ID3DX11EffectPass* pass = _waterShader.pass0;
	// Define the input layout, including world, worldViewProjection and normal matrix
	const D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);
	// Create the input layout
	D3DX11_PASS_DESC pd;
	V_RETURN(pass->GetDesc(&pd));
	V_RETURN(device->CreateInputLayout(layout, numElements, pd.pIAInputSignature, pd.IAInputSignatureSize, &_inputLayout));
	return S_OK;
}
HRESULT Water::render(ID3D11DeviceContext* context, ID3DX11EffectPass* pass){
	HRESULT hr;

	// Bind no vertexbuffer
	ID3D11Buffer* vbs[] = { _vertexBuffer, };
	unsigned int strides[] = { 6 * sizeof(float), }, offsets[] = { 0, };//10 * sizeof(float)
	context->IASetVertexBuffers(0, 1, vbs, strides, offsets);
	//Bind the index buffer to the input assembler stage
	//context->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	// Tell the input assembler stage which primitive topology to use
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);//D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST
	// Apply the rendering pass in order to submit the necessary render state changes to the device
	context->IASetInputLayout(_inputLayout);
	V(pass->Apply(0, context));
	context->Draw(3, 0);
	return S_OK;
}
void Water::destroy(){
	SAFE_RELEASE(_indexBuffer);
	SAFE_RELEASE(_vertexBuffer);
	SAFE_RELEASE(_inputLayout);
}