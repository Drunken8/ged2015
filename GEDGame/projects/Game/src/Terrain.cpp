#include "Terrain.h"

#include "GameEffect.h"
//#include "SimpleImage/SimpleImage.h"
#include <DDSTextureLoader.h>
#include "DirectXTex.h"

// You can use this macro to access your height field
#define IDX(X,Y,WIDTH) ((X) + (Y) * (WIDTH))

Terrain::Terrain(void):
	indexBuffer(nullptr),
	vertexBuffer(nullptr),
	diffuseTexture(nullptr),
	diffuseTextureSRV(nullptr),
	_normalMapTexture(nullptr),
	_normalMapTextureSRV(nullptr),
	_heightBuffer(nullptr),
	_heightBufferSRV(nullptr),
	debugSRV(nullptr)
{
}


Terrain::~Terrain(void)
{
}

HRESULT Terrain::create(ID3D11Device* device, ConfigParser* configParser)
{
	HRESULT hr;
	if (configParser->getExperimentalShaderID() != 0||configParser->getenableMinimap()==1) _useExpShader = true;
	//load the height field image (from file)
	configParser->getTerrainPath().height;
	std::wstring heightFile= (L"resources\\" + configParser->getTerrainPath().height);
	GEDUtils::SimpleImage _heightImage(const_cast<wchar_t*> (heightFile.c_str()));
	_res = _heightImage.getWidth();

	//create array for image
	_heightField = new float[_res*_res];
	for (int y = 0; y < _res; ++y){
		for (int x = 0; x < _res; ++x){
			_heightField[IDX(x, y, _res)] = _heightImage.getPixel(x, y);
		}
	}

	_middle = _heightField[IDX(_res / 2, _res / 2, _res)];

	//regenerate normals TODO
	//fill vertex buffer
	/*
	Vertex* _vertexBuffer = new Vertex[_res*_res];
	for (int y = 0; y < _res; ++y){
		for (int x = 0; x < _res; ++x){
			//set vertex postition
			_vertexBuffer[IDX(x, y, _res)].pos.x = (static_cast<float> (x) / (_res-1))*configParser->getTerrainWidth() - configParser->getTerrainWidth() / 2;//-(width/2-(x/_res)*width)
			_vertexBuffer[IDX(x, y, _res)].pos.z = (static_cast<float> (y) / (_res-1))*configParser->getTerrainDepth() - configParser->getTerrainDepth() / 2;
			_vertexBuffer[IDX(x, y, _res)].pos.y = _heightField[IDX(x, y, _res)] * configParser->getTerrainHeight();
			_vertexBuffer[IDX(x, y, _res)].pos.t = 1.0f;


			//set vertex normal
			if (0<x && x < _res - 1 && 0<y && y < _res - 1){
				float u = (_heightField[IDX(x + 1, y, _res)] - _heightField[IDX(x-1, y, _res)]) / 2;
				float v = (_heightField[IDX(x, y + 1, _res)] - _heightField[IDX(x, y-1, _res)]) / 2;
				u = -u*configParser->getTerrainHeight();
				v = -v*configParser->getTerrainHeight();
				float f = 1.0f;
				float length = sqrt(u*u+v*v+f*f);
				u /= length;
				v /= length;
				f /= length;
				_vertexBuffer[IDX(x, y, _res)].normal.x = u;
				_vertexBuffer[IDX(x, y, _res)].normal.y = f;
				_vertexBuffer[IDX(x, y, _res)].normal.z = v;
			}
			else if (x==0||y==0){//border forward
				float u = (_heightField[IDX(x + 1, y, _res)] - _heightField[IDX(x, y, _res)]);
				float v = (_heightField[IDX(x, y + 1, _res)] - _heightField[IDX(x, y, _res)]);
				u = -u*configParser->getTerrainHeight();
				v = -v*configParser->getTerrainHeight();
				float f = 1.0f;
				float length = sqrt(u*u+v*v+1);
				u /= length;
				v /= length;
				f /= length;
				_vertexBuffer[IDX(x, y, _res)].normal.x = u;
				_vertexBuffer[IDX(x, y, _res)].normal.y = f;
				_vertexBuffer[IDX(x, y, _res)].normal.z = v;
			}
			else if (x == _res - 1 || y == _res - 1){//border backward
				float u = (_heightField[IDX(x, y, _res)] - _heightField[IDX(x - 1, y, _res)]) ;
				float v = (_heightField[IDX(x, y, _res)] - _heightField[IDX(x, y - 1, _res)]);
				u = -u*configParser->getTerrainHeight();
				v = -v*configParser->getTerrainHeight();
				float f = 1.0f;
				float length = sqrt(u*u + v*v + 1);
				u /= length;
				v /= length;
				f /= length;
				_vertexBuffer[IDX(x, y, _res)].normal.x = u;
				_vertexBuffer[IDX(x, y, _res)].normal.y = f;
				_vertexBuffer[IDX(x, y, _res)].normal.z = v;
			}
			else{
				_vertexBuffer[IDX(x, y, _res)].normal.x = 0;
				_vertexBuffer[IDX(x, y, _res)].normal.y = 1;
				_vertexBuffer[IDX(x, y, _res)].normal.z = 0;
			}

			_vertexBuffer[IDX(x, y, _res)].normal.t = 0;


			//set uv coordinates
			_vertexBuffer[IDX(x, y, _res)].UVcoords.u = (static_cast<float> (x) / (_res - 1));
			_vertexBuffer[IDX(x, y, _res)].UVcoords.v = (static_cast<float> (y) / (_res - 1));
		}
	}*/
	/*
    D3D11_SUBRESOURCE_DATA id;
    id.pSysMem = _vertexBuffer;
    id.SysMemPitch = 10 * sizeof(float); // Stride
    id.SysMemSlicePitch = 0;

    D3D11_BUFFER_DESC bd;
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.ByteWidth = _res*_res*sizeof(float)*10; //The size in bytes of the triangle array
    bd.CPUAccessFlags = 0;
    bd.MiscFlags = 0;
    bd.Usage = D3D11_USAGE_DEFAULT;
	*/
    //V(device->CreateBuffer(&bd, &id, &vertexBuffer)); // http://msdn.microsoft.com/en-us/library/ff476899%28v=vs.85%29.aspx


	//set settings for height map buffer
	D3D11_SUBRESOURCE_DATA id;
	id.pSysMem = _heightField;
	id.SysMemPitch = sizeof(float); // Stride
	id.SysMemSlicePitch = 0;

	D3D11_BUFFER_DESC bd;
	bd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	bd.ByteWidth = _res*_res*sizeof(float); //The size in bytes of the height map array
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;

	//create height field buffer
	V(device->CreateBuffer(&bd, &id, &_heightBuffer)); // http://msdn.microsoft.com/en-us/library/ff476899%28v=vs.85%29.aspx

	D3D11_SHADER_RESOURCE_VIEW_DESC schaderRVdesc;
	schaderRVdesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	schaderRVdesc.Format = DXGI_FORMAT_R32_FLOAT;
	schaderRVdesc.Buffer.FirstElement = 0;
	schaderRVdesc.Buffer.NumElements = _res * _res;

	V(device->CreateShaderResourceView(_heightBuffer, &schaderRVdesc, &_heightBufferSRV));

	// Create index buffer
	//fill index buffer
	unsigned int* _indexBuffer = new unsigned int[6 * (_res - 1)*(_res - 1)];//2*(_res-1)*(_res-1) triangles with 3 vertices each
	int n = 0;
	for (int j = 0; j < _res - 1; ++j){
		for (int i = 0; i < _res - 1; ++i){
			_indexBuffer[n] = i + _res*j;
			_indexBuffer[n + 1] = i + _res*j + 1;
			_indexBuffer[n + 2] = i + _res*(j + 1);
			_indexBuffer[n + 3] = i + _res*(j + 1);
			_indexBuffer[n + 4] = i + _res*j + 1;
			_indexBuffer[n + 5] = i + _res*(j + 1) + 1;
			n += 6;
		}
	}
	//create index buffer
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(unsigned int) * 6 * (_res - 1)*(_res - 1);
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	ZeroMemory(&id, sizeof(id));
	id.pSysMem = _indexBuffer;

	V(device->CreateBuffer(&bd, &id, &indexBuffer));

	// Load color texture (color map)
	//configParser->getTerrainPath().height;
	std::wstring colorFile = L"resources\\" + configParser->getTerrainPath().color;
	V(DirectX::CreateDDSTextureFromFile(device, const_cast<wchar_t*>(colorFile.c_str()), nullptr, &diffuseTextureSRV));

	std::wstring normalTexFile = L"resources\\" + configParser->getTerrainPath().normal;
	V(DirectX::CreateDDSTextureFromFile(device, const_cast<wchar_t*>(normalTexFile.c_str()), nullptr, &_normalMapTextureSRV));
	//experimental
	if (_useExpShader){
		std::wstring hexagonFile = L"resources\\hexagon_01.dds";
		V(DirectX::CreateDDSTextureFromFile(device, const_cast<wchar_t*>(hexagonFile.c_str()), nullptr, &_hexagonTextureSRV));
	}

	//delete[] _vertexBuffer;
	delete[] _indexBuffer;
	return hr;
}


void Terrain::destroy()
{
	//Release index and vertex buffer
	SAFE_RELEASE(vertexBuffer);
	SAFE_RELEASE(indexBuffer);
	//SAFE_RELEASE(debugSRV);

    //Release the terrain's shader resource view and texture
	SAFE_RELEASE(diffuseTextureSRV);
	SAFE_RELEASE(diffuseTexture);
	SAFE_RELEASE(_normalMapTextureSRV);
	SAFE_RELEASE(_normalMapTexture);
	SAFE_RELEASE(_heightBufferSRV);
	SAFE_RELEASE(_heightBuffer);
	SAFE_RELEASE(_hexagonTexture);
	SAFE_RELEASE(_hexagonTextureSRV);
	delete[] _heightField;
}


void Terrain::render(ID3D11DeviceContext* context, ID3DX11EffectPass* pass)
{
	HRESULT hr;

	// Bind the terrain vertex buffer to the input assembler stage 
	ID3D11Buffer* vbs[] = { nullptr, };// { vertexBuffer, };
    unsigned int strides[] = { 0, }, offsets[] = { 0, };//10 * sizeof(float)
    context->IASetVertexBuffers(0, 1, vbs, strides, offsets);
	// TODO: Bind the terrain index buffer to the input assembler stage
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//context->IASetInputLayout(nullptr);
    // Tell the input assembler stage which primitive topology to use
    context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);    

    // Bind the SRV of the terrain diffuse texture to the effect variable
	V(g_gameEffect.diffuseEV->SetResource(diffuseTextureSRV));
	//bind height
	V(g_gameEffect.heightmap->SetResource(_heightBufferSRV));
	V(_depthShader.heightmap->SetResource(_heightBufferSRV));
	//bind normal
	V(g_gameEffect.normalmap->SetResource(_normalMapTextureSRV));
	//set resolution
	V(g_gameEffect.resolution->SetInt(_res));
	V(_depthShader.resolution->SetInt(_res));
	//experimental
	if (_useExpShader)
		V(g_gameEffect.hexagontex->SetResource(_hexagonTextureSRV));

    // Apply the rendering pass in order to submit the necessary render state changes to the device
    V(pass->Apply(0, context));

    // Draw
    // Use DrawIndexed to draw the terrain geometry using as shared vertex list
	//context->Draw(_res*_res, 0);
	context->DrawIndexed(6 * (_res - 1)*(_res - 1), 0, 0);
	
}

float Terrain::getHeight(float u,float v){
	if (u < 0 || 1 < u || v < 0 || 1 < v)return 0;
	int x = u*_res,y=v*_res;
	return _heightField[IDX(x, y, _res)];
}
