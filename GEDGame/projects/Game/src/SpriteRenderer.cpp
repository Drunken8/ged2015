#include "SpriteRenderer.h"


// Constructor: Create a SpriteRenderer with the given list of textures.
// The textures are *not* be created immediately, but only when create is called!
SpriteRenderer::SpriteRenderer(const std::vector<std::wstring>& textureFilenames) :
_vertexBuffer(nullptr),
_inputLayout(nullptr)
{
	_spriteCountMax = 16384;
	_textureFilenames = textureFilenames;
}
// Destructor does nothing. Destroy must be called first!
SpriteRenderer::~SpriteRenderer(){

}

// Create all required D3D resources (textures, buffers, ...).
HRESULT SpriteRenderer::create(ID3D11Device* device){
	HRESULT hr;
	//create empty vertex buffer
	D3D11_BUFFER_DESC bd;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.ByteWidth = _spriteCountMax* sizeof(SpriteVertex); //The size in bytes of the triangle array
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;

	V(device->CreateBuffer(&bd, nullptr, &_vertexBuffer));

	//create input layout
	ID3DX11EffectPass* pass = _spriteShader.pass0;
	// Define the input layout
	const D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "RADIUS", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "ALPHA", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);
	// Create the input layout
	D3DX11_PASS_DESC pd;
	V_RETURN(pass->GetDesc(&pd));
	V_RETURN(device->CreateInputLayout(layout, numElements, pd.pIAInputSignature, pd.IAInputSignatureSize, &_inputLayout));

	//create SRVs
	for (auto it = _textureFilenames.begin(); it != _textureFilenames.end(); ++it){
		ID3D11ShaderResourceView* SRV;
		V(DirectX::CreateDDSTextureFromFile(device, const_cast<wchar_t*>(it->c_str()), nullptr, &SRV));
		_spriteSRV.emplace_back(SRV);
	}
	return S_OK;
}
// Release D3D resources again.
void SpriteRenderer::destroy(){
	SAFE_RELEASE(_vertexBuffer);
	SAFE_RELEASE(_inputLayout);
	for (auto it = _spriteSRV.begin(); it != _spriteSRV.end(); ++it){
		SAFE_RELEASE(*it);
	}
}

// Render the given sprites. They must already be sorted into back-to-front order.
void SpriteRenderer::renderSprites(ID3D11DeviceContext* context, const std::vector<SpriteVertex>& sprites, ID3DX11EffectPass* pass){
	HRESULT hr;
	D3D11_BOX box;
	box.back = 1;
	box.bottom = 1;
	box.right = sprites.size()*sizeof(SpriteVertex);
	box.top = 0;
	box.left = 0;
	box.front = 0;
	//copy vertex data to tmp array to update buffer with
	SpriteVertex* spriteArray = new SpriteVertex[sprites.size()];
	int i = 0;
	for (auto it = sprites.begin(); it != sprites.end(); ++it){
		spriteArray[i] = *it;
		i++;
		if (i >= _spriteCountMax)break;
	}
	context->UpdateSubresource(_vertexBuffer, 0, &box, spriteArray, 0, 0);
	delete[] spriteArray;
	ID3D11Buffer* vbs[] = { _vertexBuffer, };
	unsigned int strides[] = { sizeof(SpriteVertex), }, offsets[] = { 0, };
	context->IASetVertexBuffers(0, 1, vbs, strides, offsets);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	context->IASetInputLayout(_inputLayout);
	//
	ID3D11ShaderResourceView** spriteSRVs = new ID3D11ShaderResourceView*[_spriteSRV.size()];
	i = 0;
	for (auto it = _spriteSRV.begin(); it != _spriteSRV.end(); ++it){
		spriteSRVs[i] = *it;
		i++;
		if (i >= 30)break;
	}
	V(_spriteShader.spriteEV->SetResourceArray(spriteSRVs,0,i));
	V(pass->Apply(0,context));
	context->Draw(sprites.size(),0);
}