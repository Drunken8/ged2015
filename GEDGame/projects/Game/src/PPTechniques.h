#pragma once

#include "DXUT.h"
#include "d3dx11effect.h"
#include "SDKmisc.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
// Convenience macros for safe effect variable retrieval
#define SAFE_GET_PASS(Technique, name, var)   {assert(Technique!=NULL); var = Technique->GetPassByName( name );						assert(var->IsValid());}
#define SAFE_GET_TECHNIQUE(effect, name, var) {assert(effect!=NULL); var = effect->GetTechniqueByName( name );						assert(var->IsValid());}
#define SAFE_GET_SCALAR(effect, name, var)    {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsScalar();			assert(var->IsValid());}
#define SAFE_GET_VECTOR(effect, name, var)    {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsVector();			assert(var->IsValid());}
#define SAFE_GET_MATRIX(effect, name, var)    {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsMatrix();			assert(var->IsValid());}
#define SAFE_GET_SAMPLER(effect, name, var)   {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsSampler();			assert(var->IsValid());}
#define SAFE_GET_RESOURCE(effect, name, var)  {assert(effect!=NULL); var = effect->GetVariableByName( name )->AsShaderResource();	assert(var->IsValid());}

struct PPEffect{
	ID3DX11Effect*                          effect; // The whole rendering effect
	ID3DX11EffectTechnique*                 technique; 
	ID3DX11EffectPass*                      passBlurH;
	ID3DX11EffectPass*                      passBlurV;
	ID3DX11EffectPass*                      passBlurC;
	ID3DX11EffectPass*                      passBoom;
	ID3DX11EffectPass*                      passLensFlare;
	ID3DX11EffectPass*                      passIdentity;
	ID3DX11EffectPass*                      passCrop;
	ID3DX11EffectPass*                      passGreyscale;

	//
	ID3DX11EffectMatrixVariable*            worldViewProjectionEV;
	ID3DX11EffectShaderResourceVariable*    inputEV;
	ID3DX11EffectShaderResourceVariable*    skyEV;
	ID3DX11EffectScalarVariable*			_width;
	ID3DX11EffectScalarVariable*			_height;
	ID3DX11EffectScalarVariable*			pointX;
	ID3DX11EffectScalarVariable*			pointY;
	ID3DX11EffectScalarVariable*			gamma;
	PPEffect() { ZeroMemory(this, sizeof(*this)); }		// WARNING: This will set ALL members to 0!


	HRESULT create(ID3D11Device* device)
	{
		HRESULT hr;
		WCHAR path[MAX_PATH];

		// Find and load the rendering effect
		V_RETURN(DXUTFindDXSDKMediaFileCch(path, MAX_PATH, L"shader\\PostProcessing.fxo"));
		std::ifstream is(path, std::ios_base::binary);
		is.seekg(0, std::ios_base::end);
		std::streampos pos = is.tellg();
		is.seekg(0, std::ios_base::beg);
		std::vector<char> effectBuffer((unsigned int)pos);
		is.read(&effectBuffer[0], pos);
		is.close();
		V_RETURN(D3DX11CreateEffectFromMemory((const void*)&effectBuffer[0], effectBuffer.size(), 0, device, &effect));
		assert(effect->IsValid());

		// Obtain the effect technique
		SAFE_GET_TECHNIQUE(effect, "PostProcess", technique);

		// Obtain the effect pass
		SAFE_GET_PASS(technique, "PIdentity", passIdentity);
		SAFE_GET_PASS(technique, "PCrop", passCrop);
		SAFE_GET_PASS(technique, "PGreyscale", passGreyscale);
		SAFE_GET_PASS(technique, "PLensflare", passLensFlare);
		SAFE_GET_PASS(technique, "PBlurH", passBlurH);
		SAFE_GET_PASS(technique, "PBlurV", passBlurV);
		SAFE_GET_PASS(technique, "PBlurC", passBlurC);

		// Obtain the effect variables
		SAFE_GET_MATRIX(effect, "_WorldViewProjection", worldViewProjectionEV);
		SAFE_GET_RESOURCE(effect, "_inputEV", inputEV);
		SAFE_GET_RESOURCE(effect, "_skyEV", skyEV);
		SAFE_GET_SCALAR(effect, "_width", _width);
		SAFE_GET_SCALAR(effect, "_height", _height);
		SAFE_GET_SCALAR(effect, "_pointX", pointX);
		SAFE_GET_SCALAR(effect, "_pointY", pointY);
		SAFE_GET_SCALAR(effect, "_gamma", gamma);

		return S_OK;
	}
	void destroy()
	{
		SAFE_RELEASE(effect);
	}

};
extern PPEffect _pp;

