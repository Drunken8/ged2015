#pragma once
#include "DXUT.h"
#include "d3dx11effect.h"
#include <SimpleImage.h>
#include "ConfigParser.h"
#include <math.h>
#include "Depth.h"

class Terrain
{
public:
	Terrain(void);
	~Terrain(void);

	HRESULT create(ID3D11Device* device, ConfigParser* configParser);
	void destroy();

	void render(ID3D11DeviceContext* context, ID3DX11EffectPass* pass);
	float getMiddle(){ return _middle; };
	//get height int [0,1] from terrain "uv" coordinates ([0,1])
	float getHeight(float u,float v);


private:
	/** Vector4 for positiona and normal **/
	struct Vector4{ float x, y, z, t; };
	/** Vector2 for uv coordinates (u = x, v = y) **/
	struct Vector2{ float u, v; };
	struct Vertex{ Vector4 pos, normal; Vector2 UVcoords; };

	Terrain(const Terrain&);
	Terrain(const Terrain&&);
	void operator=(const Terrain&);
	bool _useExpShader=false;
	// Terrain rendering resources

	
	ID3D11Buffer*                           vertexBuffer;	// The terrain's vertices
	ID3D11Buffer*                           indexBuffer;	// The terrain's triangulation
	ID3D11Texture2D*                        diffuseTexture; // The terrain's material color for diffuse lighting
	ID3D11ShaderResourceView*               diffuseTextureSRV; // Describes the structure of the diffuse texture to the shader stages

	ID3D11Texture2D*                        _normalMapTexture; // The terrain's normal map
	ID3D11ShaderResourceView*               _normalMapTextureSRV; // Describes the structure of the normal map (?) to the shader stages
	ID3D11Buffer*                           _heightBuffer;	// The height map...
	ID3D11ShaderResourceView*               _heightBufferSRV;

	ID3D11Texture2D*                        _hexagonTexture; // The terrain's normal map
	ID3D11ShaderResourceView*               _hexagonTextureSRV; // Describes the structure of the normal map (?) to the shader stages

	int _res;
	float _middle;
	float* _heightField;

	// General resources
	ID3D11ShaderResourceView*               debugSRV;
};

