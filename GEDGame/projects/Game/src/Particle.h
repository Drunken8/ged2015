#pragma once


#include <d3dx11effect.h>
#include <DDSTextureLoader.h>
#include <math.h>
#include "SpriteRenderer.h"


//represents a particle with changing position and texture

class Particle{
public:
	Particle();
	Particle(int texStartIndex, int texCount, float lifeTime, float size, float alpha, XMVECTOR pos, float time);
	Particle(int texStartIndex, int texCount, float lifeTime, float size, float alpha, XMVECTOR pos, XMVECTOR dir, float time);
	Particle(int texStartIndex, int texCount, float lifeTime, float size, float alpha, XMVECTOR pos, XMVECTOR dir, XMVECTOR acc, float time);
	//startIndex, texCount, loopTime, loopCount, size, alpha, pos, dir, acc, curTime
	Particle(int texStartIndex, int texCount, float singleLoopTime, float loopCount, float size, float alpha, XMVECTOR pos, XMVECTOR dir, XMVECTOR acc, float time);
	~Particle();

	void setSpawner(int texStartIndex, int texCount, float lifeTime, float loopCount, float size, float alpha, float interval);

	void setStartFade(float fadeEnd, float startSize, float startAlpha);

	void setEndFade(float fadeStart, float endSize, float endAlpha);
	void setSpawnStartFade(float fadeEnd, float startSize, float startAlpha);
	void setSpawnEndFade(float fadeStart, float endSize, float endAlpha);
	//update pos and texIndex
	void update(float time);
	void updatePos(XMVECTOR pos);
	//update moving direction
	void updateDir(XMVECTOR dir);
	//update acceleration
	void updateAcc(XMVECTOR acc);
	bool isNextSpawn(float time);
	Particle getNextSpawn(float time);
	//return SpriteVertex for rendering
	SpriteRenderer::SpriteVertex getParticle(){ return _particleSprite; };
	//returns if the lifetime is exeeded
	bool isDead(){ return _isDead; };
private:
	SpriteRenderer::SpriteVertex _particleSprite;
	XMFLOAT4 _dir,_acc;
	float _startTime, _lastTime, _lifeTime, _size, _loopCount, _maxAlpha;
	int _texStartIndex, _texCount;
	bool _isDead = false;
	//fade
	bool _startFade = false, _endFade = false;
	float _startFadeEnd, _endFadeStart, _startSize, _endSize, _startAlpha, _endAlpha;
	//spawner
	bool _isSpawner = false,_nextSpawn=false;
	float _spawnInterval, _lastSpawned, _spawnLifeTime, _spawnSize, _spawnLoopCount, _spawnAlpha;
	int _spawnTexStartIndex, _spawnTexCount;
	//spawnfade
	bool _spawnStartFade = false, _spawnEndFade = false;
	float _spawnStartFadeEnd, _spawnEndFadeStart, _spawnStartSize, _spawnEndSize, _spawnStartAlpha, _spawnEndAlpha;
};