//based on http://www.rastertek.com/dx11tut22.html
#pragma once
#include "DXUT.h"
#include "d3dx11effect.h"
#include <d3d11.h>
#include <iostream>

using namespace DirectX;
class RenderToTexture{
public:
	RenderToTexture();
	~RenderToTexture();

	HRESULT create(ID3D11Device* device, int width, int height);
	void destroy();

	void SetRenderTarget(ID3D11DeviceContext* deviceContext);
	void ClearRenderTarget(ID3D11DeviceContext* deviceContext, float r, float g, float b);
	void ClearRenderTarget(ID3D11DeviceContext* deviceContext);
	//return the SRV as result of the render to texture for further use
	ID3D11ShaderResourceView* GetShaderResourceView();
	XMMATRIX getLightWorldViewProj(XMMATRIX* world, XMVECTOR* lightDir);
	XMMATRIX getLightViewProj(XMVECTOR* lightDir);

	int getWidth(){ return _width; };
	int getHeight(){ return _height; };

private:
	int _width;
	int _height;
	ID3D11Texture2D* _renderTargetTexture;
	ID3D11RenderTargetView* _renderTargetView;
	ID3D11ShaderResourceView* _renderTargetSRV;
	ID3D11DepthStencilView* _depthStencilView;
	ID3D11DepthStencilState* _depthStencilState;
	ID3D11Texture2D* _depthStencilBuffer;
};