
Texture2D   _WaterEV;

//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{

	int _WaterRes;
	float _waterHeight;
	float4 _ColLightAmbient;
	float _fogBegin;
	float _fogBeginStrength;
	float4 _fogBeginColor;
	float _fogEnd;
	float _fogEndStrength;
	float4 _fogEndColor;
	float4 _waterColor;
	float _waterTransparency;
	float _tessellationAmount;
};

cbuffer cbChangesEveryFrame
{
	matrix _World;
	matrix _View;
	matrix _Projection;
	matrix _WorldViewProjection;
	float   _Time;
};

cbuffer cbUserChanges
{
};

//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault {
};

RasterizerState rsCullFront {
	CullMode = Front;
};

RasterizerState rsCullBack {
	CullMode = Back;
};

RasterizerState rsCullNone {
	CullMode = None;
};

RasterizerState rsLineAA {
	CullMode = None;
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

//
// AlphaBlendig
//
BlendState AlphaBlendingOn
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
	SrcBlend = SRC_ALPHA;
};

//functions
float4 applyFog(float colorFactor, float strength, float3 color){
	float3 fcolor = (1 - colorFactor)*_fogBeginColor.xyz + colorFactor*_fogEndColor.xyz;
		return float4((1 - strength)*color + strength*fcolor, 1);
}
//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------
struct VertexInputType
{
	float4 Pos : POSITION;
	float2 Tex : TEXCOORD;
};
struct HullInputType
{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD;
};
struct ConstantOutputType
{
	float edges[3] : SV_TessFactor;
	float inside : SV_InsideTessFactor;
};


struct HullOutputType
{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD;
};
struct PixelInputType
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
	//float fogColorFactor : FOG;
	//float fogStrength : FOG1;
};

//--------------------------------------------------------------------------------------
// Shaders
//--------------------------------------------------------------------------------------

HullInputType WaterVS(VertexInputType input){
	HullInputType output = (HullInputType)0;
	input.Pos.y = _waterHeight;
	output.Pos = input.Pos.xyz;
	output.Tex = input.Tex;
	return output;
}
PixelInputType Water2VS(VertexInputType input){
	PixelInputType output = (PixelInputType)0;
	input.Pos.y = _waterHeight;
	output.Pos = input.Pos;
	output.Tex = input.Tex;
	output.Pos=mul(output.Pos, _WorldViewProjection);
	return output;
}
////////////////////////////////////////////////////////////////////////////////
// Patch Constant Function
////////////////////////////////////////////////////////////////////////////////
ConstantOutputType WaterPatchConstantFunction(InputPatch<HullInputType, 3> inputPatch, uint patchId : SV_PrimitiveID)
{
	ConstantOutputType output;


	// Set the tessellation factors for the three edges of the triangle.
	output.edges[0] = _tessellationAmount;
	output.edges[1] = _tessellationAmount;
	output.edges[2] = _tessellationAmount;

	// Set the tessellation factor for tessallating inside the triangle.
	output.inside = _tessellationAmount;

	return output;
}
////////////////////////////////////////////////////////////////////////////////
// Hull Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("WaterPatchConstantFunction")]

HullOutputType WaterHS(InputPatch<HullInputType, 3> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
	HullOutputType output;

	// Set the position for this control point as the output position.
	output.Pos = patch[pointId].Pos;
	// Set the input color as the output color.
	output.Tex = patch[pointId].Tex;
	return output;
}
////////////////////////////////////////////////////////////////////////////////
// Domain Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]

PixelInputType WaterDS(ConstantOutputType input, float3 uvwCoord : SV_DomainLocation, const OutputPatch<HullOutputType, 3> patch)
{
	float3 vertexPosition;
	PixelInputType output;


	// Determine the position of the new vertex.
	vertexPosition = uvwCoord.x * patch[0].Pos + uvwCoord.y * patch[1].Pos + uvwCoord.z * patch[2].Pos;
	vertexPosition.y = 0.2+(sin(vertexPosition.x*50+_Time ) + 1)/40;
	output.Tex = patch[0].Tex;

	// Calculate the position of the new vertex against the world, view, and projection matrices.
	output.Pos = mul(float4(vertexPosition, 1.0f), _WorldViewProjection);

	return output;
}

float4 WaterPS(PixelInputType input) :SV_Target0{
	//return float4(_waterColor.xyz,_waterTransparency);

	return float4(0, 0, 1, 0.6);
}

technique11 Render
{
	pass P0
	{

		SetVertexShader(CompileShader(vs_5_0, WaterVS()));
		SetHullShader(CompileShader(hs_5_0, WaterHS()));
		SetDomainShader(CompileShader(ds_5_0, WaterDS()));
		//SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, WaterPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}