Texture2D sprites[30]; 

cbuffer cbConstant
{

};

cbuffer cbChangesEveryFrame
{
	float4  _LightDir;
	matrix  _World;
	matrix _View;
	matrix _Projection;
	matrix _WorldViewProjection;
	float  _Time;
	float4 _up;
	float4 _right;
	float4 _dir;
	float  _length;
};
//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};
//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------
RasterizerState rsCullNone {
	CullMode = None;
};

RasterizerState rsLineAA {
	CullMode = None;
	AntialiasedLineEnable = true;
};

//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

//
// AlphaBlendig
//
BlendState AlphaBlendingOn
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
	SrcBlend = SRC_ALPHA;
};

//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------



struct SpriteVertex
{
	float4 Pos : POSITION;
	float Radius : RADIUS;
	uint TexI : TEXCOORD;
	float Alpha : ALPHA;
};

struct SpritePSin
{
	float4 Pos : SV_POSITION;
	uint TexI : TEXCOORD;
	float2 Tex : TEXCOORD1;
	float Alpha : ALPHA;
};

struct SpherePSin
{
	float4 Pos : SV_POSITION;
	float4 vPos : POSITION;
	float4 c : POSITION1;
	float r : RADIUS;
	uint TexI : TEXCOORD;
	float2 Tex : TEXCOORD1;
	float Alpha : ALPHA;
};

SpriteVertex SpriteVS(SpriteVertex input){
	SpriteVertex output = (SpriteVertex)0;
	output.Pos = input.Pos;
	output.Pos.w = 1;
	output.Radius = input.Radius;
	output.TexI = input.TexI;
	output.Alpha = input.Alpha;
	return output;
}
[maxvertexcount(4)]
void SphereGS(point SpriteVertex vertex[1], inout TriangleStream<SpherePSin> stream){
	SpherePSin v = (SpherePSin)0;
	v.TexI = vertex[0].TexI;
	v.c = mul(vertex[0].Pos, _View);
	v.r = vertex[0].Radius*0.85;

	v.Pos = vertex[0].Pos + vertex[0].Radius*_up - vertex[0].Radius*_right; // set transformed position
	v.vPos = mul(v.Pos, _View);
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(0, 0);
	stream.Append(v);       // output first vertex
	v.Pos = vertex[0].Pos - vertex[0].Radius*_up - vertex[0].Radius*_right; // set transformed position
	v.vPos = mul(v.Pos, _View);
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(0, 1);
	stream.Append(v);       // output second vertex
	v.Pos = vertex[0].Pos + vertex[0].Radius*_up + vertex[0].Radius*_right; // set transformed position
	v.vPos = mul(v.Pos, _View);
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(1, 0);
	stream.Append(v);       // output third vertex
	v.Pos = vertex[0].Pos - vertex[0].Radius*_up + vertex[0].Radius*_right; // set transformed position
	v.vPos = mul(v.Pos, _View);
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(1, 1);
	stream.Append(v);       // output 4th vertex
}
float4 SpherePS(SpherePSin input) : SV_Target0{
	//line-sphere collision
	//start=(0,0,0) camera
	float3 dir = normalize(input.vPos.xyz);
	float3 center = input.c.xyz;
	float dist = dot(center,dir);
	float b = length((dist * dir)-center);
	if (b > input.r)return float4(1, 0, 1, 0.3);
	float c = sqrt(input.r*input.r - b*b);
	float3 pointOnSphere = dir*(dist - c);
	//color and light
	float3 matDiffuse = float3(0, 1, 0);
	float3 matSpecular = float3(0.8, 1, 0.8);
	float3 n=normalize(pointOnSphere-center);
	float4 l = mul(_LightDir, _View);
	float i = saturate(dot(float4(n, 0), l));
	float3 r = reflect(-l.xyz, n).xyz;
	float v = saturate(dot(r, normalize(-pointOnSphere)));
	float3 color = 0.58*matDiffuse*i +
		0.4*matSpecular*saturate(pow(v, 10)) + 0.02*matDiffuse;
	return float4(color, 1);
}


[maxvertexcount(4)]
void SpriteGS(point SpriteVertex vertex[1], inout TriangleStream<SpritePSin> stream){
	SpritePSin v = (SpritePSin)0;
	v.TexI = vertex[0].TexI;
	v.Pos = vertex[0].Pos + vertex[0].Radius*_up - vertex[0].Radius*_right; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(0, 0);
	v.Alpha = vertex[0].Alpha;
	stream.Append(v);       // output first vertex
	v.Pos = vertex[0].Pos - vertex[0].Radius*_up - vertex[0].Radius*_right; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(0, 1);
	v.Alpha = vertex[0].Alpha;
	stream.Append(v);       // output second vertex
	v.Pos = vertex[0].Pos + vertex[0].Radius*_up + vertex[0].Radius*_right; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(1, 0);
	v.Alpha = vertex[0].Alpha;
	stream.Append(v);       // output third vertex
	v.Pos = vertex[0].Pos - vertex[0].Radius*_up + vertex[0].Radius*_right; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(1, 1);
	v.Alpha = vertex[0].Alpha;
	stream.Append(v);       // output 4th vertex
}
float4 SpritePS(SpritePSin input) : SV_Target0{
	float4 color = float4(1, 0, 1, 1);
	if (input.TexI==0)
		color = sprites[0].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 1)
		color = sprites[1].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 2)
		color = sprites[2].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 3)
		color = sprites[3].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 4)
		color = sprites[4].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 5)
		color = sprites[5].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 6)
		color = sprites[6].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 7)
		color = sprites[7].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 8)
		color = sprites[8].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 9)
		color = sprites[9].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 10)
		color = sprites[10].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 11)
		color = sprites[11].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 12)
		color = sprites[12].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 13)
		color = sprites[13].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 14)
		color = sprites[14].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 15)
		color = sprites[15].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 16)
		color = sprites[16].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 17)
		color = sprites[17].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 18)
		color = sprites[18].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 19)
		color = sprites[19].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 20)
		color = sprites[20].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 21)
		color = sprites[21].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 22)
		color = sprites[22].Sample(samLinearClamp, input.Tex);
	else if (input.TexI == 23)
		color = sprites[23].Sample(samLinearClamp, input.Tex);
	//if (color.a < 0.5)color.a = 0.5;
	color.a *= input.Alpha;
	return color;
}
[maxvertexcount(4)]
void BeamGS(point SpriteVertex vertex[1], inout TriangleStream<SpritePSin> stream){
	SpritePSin v = (SpritePSin)0;
	v.TexI = vertex[0].TexI;
	v.Pos = vertex[0].Pos + vertex[0].Radius*_up; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(0, 0);
	stream.Append(v);       // output first vertex
	v.Pos = vertex[0].Pos - vertex[0].Radius*_up; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(0, 1);
	stream.Append(v);       // output second vertex
	v.Pos = vertex[0].Pos + vertex[0].Radius*_up + _length*_dir; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(1, 0);
	stream.Append(v);       // output third vertex
	v.Pos = vertex[0].Pos - vertex[0].Radius*_up + _length*_dir; // set transformed position
	v.Pos = mul(mul(v.Pos, _View), _Projection);
	v.Tex = float2(1, 1);
	stream.Append(v);       // output 4th vertex
}
float4 BeamPS(SpritePSin input) : SV_Target0{
	float3 color1 = float3(0.2, 0.6, 1);
	float3 color2 = float3(0.4, 0.8, 1);
	if (input.Tex.y > 1.0f / 3 && input.Tex.y <2.0f / 3){
		float minAlpha = 0.3;
		if (input.Tex.y > 1.0f / 3 && input.Tex.y <4.0f / 9){
			float a = (input.Tex.y - 1.0f / 3) / (1.f / 9);
			return float4((1 - a)*color1 + a*color2, 1);
		}
		if (input.Tex.y > 5.0f / 9 && input.Tex.y < 2.0f / 3){
			float a = (input.Tex.y - 5.0f / 9) / (1.f / 9);
			return float4(a*color1 + (1 - a)*color2, 1);
		}
		return float4(color2, 1);
	}
	if (input.Tex.y > (sin(-_Time*60 + input.Tex.x*_length) / 6 + 1.0f/6)) 
	if (input.Tex.y < (-sin(-_Time * 60 + input.Tex.x*_length) / 6 + 5.0f / 6)) return float4(color1, 1);
	return float4(color1, 0.25);
}
technique11 Render
{
	pass P0
	{

		SetVertexShader(CompileShader(vs_4_0, SpriteVS()));
		SetGeometryShader(CompileShader(gs_4_0, SpriteGS()));
		SetPixelShader(CompileShader(ps_4_0, SpritePS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass P1beam
	{

		SetVertexShader(CompileShader(vs_4_0, SpriteVS()));
		SetGeometryShader(CompileShader(gs_4_0, BeamGS()));
		SetPixelShader(CompileShader(ps_4_0, BeamPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass P2Sphere
	{

		SetVertexShader(CompileShader(vs_4_0, SpriteVS()));
		SetGeometryShader(CompileShader(gs_4_0, SphereGS()));
		SetPixelShader(CompileShader(ps_4_0, SpherePS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}