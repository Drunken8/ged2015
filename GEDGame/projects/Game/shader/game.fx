//--------------------------------------------------------------------------------------
// Shader resources
//--------------------------------------------------------------------------------------

Texture2D   g_Diffuse; // Material albedo for diffuse lighting

Buffer<float> g_HeightMap; //buffer for height map

Texture2D   g_NormalMap; //normal map...

Texture2D   g_DiffuseSpecularEV;

Texture2D   g_DiffuseGlowEV;

Texture2D   _DepthEV;


//resources for experimantals
Texture2D g_HexagonTex;


//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{ 
	
	int g_TerrainRes; // variable for terrrain resolution i suppose
	int g_ShaderID;
	float4 _ColLightAmbient;
	float _fogBegin;
	float _fogBeginStrength;
	float4 _fogBeginColor;
	float _fogEnd;
	float _fogEndStrength;
	float4 _fogEndColor;
	float _fObjectHeight;
};

cbuffer cbChangesEveryFrame
{
	float4  g_LightDir;

	matrix  g_World;
	matrix g_View;
	matrix g_Projection;
	matrix  g_CockpitWorld;
	matrix  g_WorldViewProjection;
	matrix  g_CockpitViewProjection;
    float   g_Time;
	matrix g_WorldNormals;
	matrix g_CockpitNormals;
	float4 g_CameraPosWorld;

	//variables for map
	matrix g_MinimapWorld;
	matrix g_MinimapWorldViewProjection;
	matrix g_MinimapNormals;

	//light variables for directional shadows
	matrix _lightWorldViewProjection;
	matrix _lightMeshViewProjection;
};

cbuffer cbUserChanges
{
};




//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
    Filter = ANISOTROPIC;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState samLinearClamp
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault {
};

RasterizerState rsCullFront {
    CullMode = Front;
};

RasterizerState rsCullBack {
    CullMode = Back;
};

RasterizerState rsCullNone {
	CullMode = None; 
};

RasterizerState rsLineAA {
	CullMode = None; 
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------
DepthStencilState EnableDepth
{
    DepthEnable = TRUE;
    DepthWriteMask = ALL;
    DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
    AlphaToCoverageEnable = FALSE;
    BlendEnable[0] = FALSE;
};

//
// AlphaBlendig
//
BlendState AlphaBlendingOn
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
	SrcBlend = SRC_ALPHA;
};

//functions
float4 applyFog(float colorFactor, float strength, float3 color){
	float3 fcolor = (1 - colorFactor)*_fogBeginColor.xyz + colorFactor*_fogEndColor.xyz;
		return float4((1 - strength)*color + strength*fcolor, 1);
}
//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------



struct PosTex
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
};

struct PosTexLi
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
	float4 LiPos : TEXCOORD1;
	float fogColorFactor : FOG;
	float fogStrength : FOG1;
};
struct PosTexWorld
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
	float4 World: POSITION;
};

struct T3dVertexVSIn {
	float3 Pos : POSITION; //Position in object space
	float2 Tex : TEXCOORD; //Texture coordinate
	float3 Nor : NORMAL;  //Normal in object space
	float3 Tan : TANGENT;  //Tangent in object space (not used in Ass. 5)

	row_major matrix instWorld : WORLD; //instance data:
	row_major matrix instWorldViewProj : WVP;
	row_major matrix instNormal : WNORMAL;
};

struct T3dVertexPSIn {
	float4 Pos : SV_POSITION; //Position in clip space
	float2 Tex : TEXCOORD;  //Texture coordinate
	float3 PosWorld : POSITION;  //Position in world space
	float3 NorWorld : NORMAL;  //Normal in world space
	float3 TanWorld : TANGENT;  //Tangent in world space (not used in Ass. 5)
	float4 LiPos : TEXCOORD1;
	float fogColorFactor : FOG;
	float fogStrength : FOG1;
	row_major matrix instWorld : WORLD; 
};

//--------------------------------------------------------------------------------------
// Shaders
//--------------------------------------------------------------------------------------




//terrain
PosTexLi TerrainVS(uint VertexID:SV_VertexID){
	PosTexLi output = (PosTexLi)0;
	int xID = VertexID%g_TerrainRes;
	int zID = VertexID / g_TerrainRes;
	//set vertex postition in [-1,1]
	float x = ((float(xID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float z = ((float(zID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float y = g_HeightMap[VertexID];
	float t = 1.0f;
	output.Pos = float4(x,y,z,t);
	output.LiPos = mul(output.Pos, _lightWorldViewProjection);
	// Transform position from object space to homogenious clip space
	output.Pos = mul(output.Pos, g_WorldViewProjection);
	float u = ((float)(xID) / (g_TerrainRes - 1));
	float v = ((float)(zID) / (g_TerrainRes - 1));
	output.Tex = float2(u, v);
	//fog
	float dist = mul(mul(float4(x, y, z, t), g_World),g_View).z;
	output.fogColorFactor = saturate((dist - _fogBegin) / (_fogEnd - _fogBegin));
	output.fogStrength = _fogBeginStrength + (_fogEndStrength - _fogBeginStrength)*output.fogColorFactor;
	return output;
}

float4 TerrainPS(PosTexLi input) :SV_Target0{
	float3 n;

	float bias = 0.002f;
	float2 projectTexCoord;
	float depthValue;
	float lightDepthValue;
	float3 color;
	//float3 _fogColor = float3(0.5, 0.5, 0.5);
	//get normal from normalmap
	n.xz = (g_NormalMap.Sample(samAnisotropic, input.Tex).rg * 2).rg - 1;
	n.y = sqrt(1 - n.x*n.x - n.z*n.z);
	//
	n = normalize(mul(float4(n, 0), g_WorldNormals).xyz);
	//get texture color
	float3 matDiffuse = g_Diffuse.Sample(samLinearClamp, input.Tex).xyz;
	float i = saturate(dot(float4(n, 0), g_LightDir));

	projectTexCoord.x = input.LiPos.x / input.LiPos.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.LiPos.y / input.LiPos.w / 2.0f + 0.5f;

	// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
	if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		// Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
		depthValue = _DepthEV.Sample(samLinearClamp, projectTexCoord).r;

		// Calculate the depth of the light.
		lightDepthValue = input.LiPos.z / input.LiPos.w;

		// Subtract the bias from the lightDepthValue.
		lightDepthValue = lightDepthValue - bias;

		// Compare the depth of the shadow map value and the depth of the light to determine whether to shadow or to light this pixel.
		if (lightDepthValue >= depthValue)//shadow
		{
			color = 0.1*matDiffuse;
		}
		else{
			if (i > 0.1)
				color = i*matDiffuse;
			else color = 0.1*matDiffuse;
		}
	}
	else{
		if (i > 0.1)
			color = i*matDiffuse;
		else color = 0.1*matDiffuse; 
	}

	return applyFog(input.fogColorFactor, input.fogStrength, color);
}

T3dVertexPSIn MeshVS(T3dVertexVSIn input){
	T3dVertexPSIn output = (T3dVertexPSIn)0;
	float4 inPos = float4(input.Pos, 1);
	float4 inNor = float4(input.Nor, 0);
	float4 inTan = float4(input.Tan, 0);
	output.Tex = input.Tex;

	output.LiPos = mul(inPos, mul(input.instWorld,_lightMeshViewProjection));
	output.Pos = mul(inPos, input.instWorldViewProj);
	output.PosWorld = mul(inPos, input.instWorld).xyz;
	output.NorWorld = normalize(mul(inNor, input.instNormal).xyz);
	output.TanWorld = normalize(mul(inTan, input.instNormal).xyz);
	output.instWorld = input.instWorld;
	//fog
	float dist = mul(mul(inPos, input.instWorld), g_View).z;
	output.fogColorFactor = saturate((dist - _fogBegin) / (_fogEnd - _fogBegin));
	output.fogStrength = _fogBeginStrength + (_fogEndStrength - _fogBeginStrength)*output.fogColorFactor;
	return output;
}

float4 MeshPS(T3dVertexPSIn input) :SV_Target0{
	float4 output = (float4)0; //output color
	float bias = 0.002f;
	float2 projectTexCoord;
	float depthValue;
	float lightDepthValue;
	float3 color;

	//get texture color
	float3 matDiffuse = g_Diffuse.Sample(samLinearClamp, input.Tex).xyz;
	//get specular color
	float3 matSpecular = g_DiffuseSpecularEV.Sample(samLinearClamp, input.Tex).xyz;
	//get glow color
	float3 matGlow = g_DiffuseGlowEV.Sample(samLinearClamp, input.Tex).xyz;

	float4 colLight = float4(1, 1, 1, 1);
	float4 colLightAmbient = float4(1, 1, 1, 1);

	float3 n = normalize(input.NorWorld);
	float4 i = mul(g_LightDir, g_World);//input.instWorld g_World
	float4 r = reflect(-i, float4(n, 0));
	float3 v = normalize(g_CameraPosWorld.xyz - input.PosWorld);

	float3 result = 0.5*matDiffuse*saturate(dot(n, i.xyz))*colLight.xyz +
	0.4*matSpecular*pow(saturate(dot(r.xyz, v)),64)*colLight.xyz +
	0.1*matDiffuse*colLightAmbient.xyz +
	0.5*matGlow;
	
	projectTexCoord.x = input.LiPos.x / input.LiPos.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.LiPos.y / input.LiPos.w / 2.0f + 0.5f;

	if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		depthValue = _DepthEV.Sample(samLinearClamp, projectTexCoord).r;
		lightDepthValue = input.LiPos.z / input.LiPos.w;
		lightDepthValue = lightDepthValue - bias;
		if (lightDepthValue >= depthValue)
		{
			result=0.1*result;
		}
	}

	return applyFog(input.fogColorFactor, input.fogStrength, result);
}

float4 MeshToonPS(T3dVertexPSIn input) :SV_Target0{
	return float4(1, 0, 1, 1);
}


T3dVertexPSIn GrassVS(T3dVertexVSIn input){
	T3dVertexPSIn output = (T3dVertexPSIn)0;
	float4 inPos = float4(input.Pos, 1);
		float4 inNor = float4(input.Nor, 0);
		float4 inTan = float4(input.Tan, 0);
		output.Tex = input.Tex;
	float low = 0.9;
	inPos = mul(inPos, input.instWorld);
	if (input.Tex.y <= low) {  // Or: if(v.TexCoords.y >= 0.9)
		float vx = 1.5*sin(g_Time*1.5 + inPos.x / 50);
		float3 vClusterTranslation = 0;
			inPos.x += vx*(1 - (input.Tex.y / low));
		//inNor = float4(normalize(input.Nor * _fObjectHeight + vClusterTranslation), 0);
	}

	output.LiPos = mul(inPos, _lightMeshViewProjection);
	output.Pos = mul(mul(inPos, g_View), g_Projection);
	output.PosWorld = mul(inPos, input.instWorld).xyz;
	output.NorWorld = normalize(mul(inNor, input.instNormal).xyz);
	output.TanWorld = normalize(mul(inTan, input.instNormal).xyz);
	output.instWorld = input.instWorld;
	//fog
	float dist = mul(mul(inPos, input.instWorld), g_View).z;
	output.fogColorFactor = saturate((dist - _fogBegin) / (_fogEnd - _fogBegin));
	output.fogStrength = _fogBeginStrength + (_fogEndStrength - _fogBeginStrength)*output.fogColorFactor;
	return output;
}
float4 GrassPS(T3dVertexPSIn input) :SV_Target0{
	float4 output = (float4)0; //output color
	float bias = 0.002f;
	float2 projectTexCoord;
	float depthValue;
	float lightDepthValue;
	float3 color;

	//get texture color
	float3 matDiffuse = g_Diffuse.Sample(samLinearClamp, input.Tex).xyz;
		//get specular color
		float3 matSpecular = g_DiffuseSpecularEV.Sample(samLinearClamp, input.Tex).xyz;

		float4 colLight = float4(1, 1, 1, 1);
		float4 colLightAmbient = float4(1, 1, 1, 1);

		float3 result = 0.5*matDiffuse*colLight.xyz +
		0.4*matSpecular*colLight.xyz +
		0.1*matDiffuse*colLightAmbient.xyz;

	projectTexCoord.x = input.LiPos.x / input.LiPos.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.LiPos.y / input.LiPos.w / 2.0f + 0.5f;

	if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		depthValue = _DepthEV.Sample(samLinearClamp, projectTexCoord).r;
		lightDepthValue = input.LiPos.z / input.LiPos.w;
		lightDepthValue = lightDepthValue - bias;
		if (lightDepthValue >= depthValue)
		{
			result = 0.1*result;
		}
	}

	return applyFog(input.fogColorFactor, input.fogStrength, result);
}
//minimap shader stuff
PosTexWorld MinimapVS(uint VertexID:SV_VertexID){
	PosTexWorld output = (PosTexWorld)0;
	int xID = VertexID%g_TerrainRes;
	int zID = VertexID / g_TerrainRes;
	//set vertex postition in [-1,1]
	float x = ((float(xID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float z = ((float(zID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float y = g_HeightMap[VertexID];
	float t = 1.0f;
	output.Pos = float4(x, y, z, t);
	//apply scaling
	//output.Pos = mul(output.Pos, g_TerrainScaling);
	output.World = mul(output.Pos, g_MinimapWorld);
	// Transform position from object space to homogenious clip space
	output.Pos = mul(output.Pos, g_MinimapWorldViewProjection);
	float u = ((float)(xID) / (g_TerrainRes - 1));
	float v = ((float)(zID) / (g_TerrainRes - 1));
	output.Tex = float2(u, v);
	return output;
}

float4 MinimapPS(PosTexWorld input) :SV_Target0{
	float3 n;
	float4 borderColor = float4(0, 0.8, 1, 1);
	//get normal from normalmap
	n.xz = (g_NormalMap.Sample(samAnisotropic, input.Tex).rg *2).rg - 1;
	n.y = sqrt(1 - n.x*n.x - n.z*n.z);
	//
	n = normalize(mul(float4(n, 0), g_MinimapNormals).xyz);
	//create a kind of fresnel effect
	float3 viewDir = normalize(input.World.xyz - g_CameraPosWorld.xyz);
	//angle between normal and view at vertex > 90 degree => look at bottom of minimap
	//=>invert viewDir to prevent a "negative" of the effect
	if (dot(n, viewDir)<0){
		viewDir.x = -viewDir.x;
		viewDir.y = -viewDir.y;
		viewDir.z = -viewDir.z;
	}
	float border = dot(n, viewDir);
	border = 1 - border;
	border = pow(border, 2);//<- change strength of effect: lower velue = stronger effect
	//get texture color
	float3 matHex = g_HexagonTex.Sample(samLinearClamp, input.Tex/2).xyz;
	float hexGrey=1-sqrt(matHex.x*matHex.x + matHex.y*matHex.y + matHex.z*matHex.z);
	//hexGrey = 1 - hexGrey;
	//calculate light
	if (hexGrey > 0.2) return float4(0.3, 1, 1, 0.8);
	//if (border <2.5&&border >0.5)return float4(0, 0, 1, 1);
	return (1 - border)*float4(0, 0, 0, 0) + border*borderColor;
}
//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------
technique11 Render
{
    pass P0
    {
		
		SetVertexShader(CompileShader(vs_4_0, TerrainVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, TerrainPS()));
		
        
        SetRasterizerState(rsCullNone);
        SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
    }

	pass P1_Mesh
	{

		SetVertexShader(CompileShader(vs_4_0, MeshVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, MeshPS()));


		SetRasterizerState(rsCullBack);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

	pass P2_Minimap
	{
		SetVertexShader(CompileShader(vs_4_0, MinimapVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, MinimapPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

	pass P3_Grass
	{
		SetVertexShader(CompileShader(vs_4_0, GrassVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, MeshPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}
