//--------------------------------------------------------------------------------------
// Shader resources
//--------------------------------------------------------------------------------------


Texture2D   _inputEV;
Texture2D   _skyEV;
Texture2D   _additionalEV[10];



//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{
	
};

cbuffer cbChangesEveryFrame
{
	float _gamma;
	float _width;
	float _height;
	matrix  _WorldViewProjection;
	float   _Time;
	float4 _CameraPosWorld;
	float _pointX;
	float _pointY;
};

cbuffer cbUserChanges
{
};




//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault {
};

RasterizerState rsCullFront {
	CullMode = Front;
};

RasterizerState rsCullBack {
	CullMode = Back;
};

RasterizerState rsCullNone {
	CullMode = None;
};

RasterizerState rsLineAA {
	CullMode = None;
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

//
// AlphaBlendig
//
BlendState AlphaBlendingOn
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
	SrcBlend = SRC_ALPHA;
};

//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------
struct VSInput {
	float4 Pos : POSITION; //Position in object space
	float2 Tex : TEXCOORD; //Texture coordinate
};
struct PSInput {
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0; //Texture coordinate
};
struct PSBlurInput {
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0; //Texture coordinate
	float2 t[13] : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Shaders
//--------------------------------------------------------------------------------------

PSInput PlaneVS(VSInput input){
	PSInput output = (PSInput)0;
	output.Pos = mul(input.Pos, _WorldViewProjection);
	output.Tex = input.Tex;
	return output;
}
PSBlurInput PlaneBlurHVS(VSInput input){
	PSBlurInput output = (PSBlurInput)0;
	output.Pos = mul(input.Pos, _WorldViewProjection);
	output.Tex = input.Tex;
	// Determine the floating point size of a texel for a screen with this specific width.
	float texelSize = 1.0f / _width;
	//check max blur strength
	// Create UV coordinates for the pixel and its horizontal neighbors on either side.
	output.t[0] = input.Tex;
	for (int i = 1; i < 6; i += 2){
		output.t[i] = input.Tex + float2(texelSize * i, 0.0f);
		output.t[i+6] = input.Tex + float2(-texelSize * i, 0.0f);
	}
	return output;
}
PSBlurInput PlaneBlurVVS(VSInput input){
	PSBlurInput output = (PSBlurInput)0;
	output.Pos = mul(input.Pos, _WorldViewProjection);
	output.Tex = input.Tex;
	// Determine the floating point size of a texel for a screen with this specific width.
	float texelSize = 1.0f / _height;
	//check max blur strength
	// Create UV coordinates for the pixel and its vertical neighbors on either side.
	output.t[0] = input.Tex;
	for (int i = 1; i < 7; i += 2){
		output.t[i] = input.Tex + float2(0.0f,texelSize * i);
		output.t[i + 6] = input.Tex + float2(0.0f,-texelSize * i);
	}
	return output;
}
PSBlurInput PlaneBlurCVS(VSInput input){
	PSBlurInput output = (PSBlurInput)0;
	output.Pos = mul(input.Pos, _WorldViewProjection);
	output.Tex = input.Tex;
	//find distance to next texel
	float2 center = float2(_pointX, _pointY);
	float2 dist = center - input.Tex;
	float d = length(dist);
	float texelSize = dist.x/ _width + dist.y/ _height;
	dist /= 16;
	//setTexels
	output.t[0] = input.Tex;
	for (int i = 1; i < 12; i++){
		output.t[i] = input.Tex + i*dist;
	}

	return output;
}
PSInput CropVS(VSInput input){
	PSInput output = (PSInput)0;
	output.Pos = mul(input.Pos, _WorldViewProjection);
	output.Tex = input.Tex;
	return output;
}
float4 IdentityPS(PSInput input) :SV_Target0{
	return _inputEV.Sample(samLinearClamp, input.Tex);
}
float4 CropPS(PSInput input) : SV_Target0{
	float xmin = _pointX - _width / 2;
	float ymin = _pointY - _height / 2;
	float2 tex = 0;
	tex.x = xmin + input.Tex.x*_width;
	tex.y = ymin + input.Tex.y*_height;
	float3 color = _inputEV.Sample(samLinearClamp, float2(_pointX, _pointY)).xyz;
	//sun border
	float sunBorder=0.3;
	if (color.x>0.4)sunBorder=color.x;
	sunBorder *= 0.15;
	float dist = distance(input.Tex, float2(0.5, 0.5));
	float sunFadeDist = 0.1*0.15;
	if (dist > sunBorder + sunFadeDist)return float4(0, 0, 0, 1);
	float sunFade = 0;
	if (dist > sunBorder){
		sunFade = (dist - sunBorder) / sunFadeDist;
	}
	return float4((1-sunFade)*_inputEV.Sample(samLinearClamp, tex).xyz,1);
}
float4 GreyscalePS(PSInput input) : SV_Target0{
	float3 color = _inputEV.Sample(samLinearClamp, input.Tex).xyz;
	float grey = sqrt(color.x*color.x + color.y*color.y + color.z*color.z);
	//float grey = (color.x + color.y + color.z) / 3;
	return float4(grey,grey,grey,1);
}
float4 LensflarePS(PSInput input) : SV_Target0{
	float aspect = _width / _height;
	float3 color = _inputEV.Sample(samLinearClamp, input.Tex).xyz;
	float3 sunColor = float3(1, 0.9, 0.9);
	//
	float2 flarePoint = float2(_pointX, _pointY);
	float sun = _skyEV.Sample(samLinearClamp, float2(0.5,0.5)).x;
	if (flarePoint.x<0 || 1<flarePoint.x || flarePoint.y<0 || 1<flarePoint.y){
		return float4(color, 1);
	}
	float2 t = float2(input.Tex.x * aspect, input.Tex.y);
	//input.Tex.x *= aspect;
	flarePoint.x *= aspect;
	float2 flareV = flarePoint - float2(0.5*aspect, 0.5);
	//float flareStrength = (1 - distance(float2(0.5, 0.5), flarePoint));
	//no flares when sun not visible
	float4 flareColor=0;
	if (sun > 0.4){
		float dist = 0;
		dist = distance(t, flarePoint + 0.15*flareV);
		if (dist < 0.006) flareColor= float4(0.95, 0.8, 0.8, 1);
		dist = distance(t, flarePoint - 0.8*flareV);
		if (dist < 0.014 && 0.011 < dist) flareColor = float4(0.8, 0.3, 0.1, 1);
		dist = distance(t, flarePoint - 1.6*flareV);
		if (dist < 0.02 && 0.017 < dist) flareColor = float4(0.8, 0.3, 0.1, 1);
		dist = distance(t, flarePoint - 2 * flareV);
		if (dist < 0.012 && 0.009 < dist) flareColor = float4(1, 0.7, 0.3, 1);
		if (distance(t, flarePoint - 2.4*flareV) < 0.005) flareColor = float4(1, 0.3, 0.4, 1);
	}
	//sun
	float sunR = 0.15;
	if (distance(t, flarePoint) < sunR){
		float2 fP = float2(flarePoint.x / aspect, flarePoint.y / aspect);
		float2 tP = float2(input.Tex.x, input.Tex.y / aspect);
		float2 d = tP - fP;
		d/=sunR;
		float sunl = _skyEV.Sample(samLinearClamp, float2(0.5,0.5)+d).x;
		color = (1 - sunl)*color + sunl*sunColor;
	}
	if (0 < flareColor.a){
		if (sun > 0.8)
			color = flareColor;
		else
			color = (1 - sun)*color + sun*flareColor;
	}

	return float4(color, 1);
}
float4 BlurLinearPS(PSBlurInput input) : SV_Target0{
	float weight[7];
	float normalization;
	for (int k = 0; k < 6 + 1; k++)
		weight[k] = ((float)6 - k) / (6+1);
	// Create a normalized value to average the weights out a bit.
	normalization = weight[0] + 2 * (weight[1] + weight[2] + weight[3] + weight[4] + weight[5] + weight[6]);

	// Initialize the color to black.
	float4 color = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Add the nine horizontal pixels to the color by the specific weight of each.
	color += _inputEV.Sample(samLinearClamp, input.t[0]) * weight[0];
	for (int i = 1; i < 7; i += 2){
		color += _inputEV.Sample(samLinearClamp, input.t[i]) * weight[i];
		color += _inputEV.Sample(samLinearClamp, input.t[i + 6]) * weight[i];
	}
	color /= normalization;
	color *= _gamma;
	color.a = 1.0f;
	return color;
}
float4 BlurCentricPS(PSBlurInput input) : SV_Target0{
	//sun
	//if (distance(input.Tex, float2(_pointX, _pointY)) < 0.01) return float4(1, 0.5, 0, 1);
	float weight[13];
	float normalization=0;
	for (int k = 0; k < 13; k++)
		normalization+=weight[k] = (12.0f - k) / (13);
	// Create a normalized value to average the weights out a bit.
	normalization;
	// Initialize the color to black.
	float4 color = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Add the nine horizontal pixels to the color by the specific weight of each.
	color += _inputEV.Sample(samLinearClamp, input.t[0]) * weight[0];
	for (int i = 1; i < 13; i++){
		color += _inputEV.Sample(samLinearClamp, input.t[i]) * weight[i];
	}
	color /= normalization;
	color *= _gamma;
	color.a = 1.0f;
	return color;
}



float4 PS(PSInput input) : SV_Target0{
	return 0;
}

//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------
technique11 PostProcess
{
	pass PIdentity
	{

		SetVertexShader(CompileShader(vs_4_0, PlaneVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, IdentityPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PCrop
	{

		SetVertexShader(CompileShader(vs_4_0, CropVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, CropPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PGreyscale
	{

		SetVertexShader(CompileShader(vs_4_0, PlaneVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, GreyscalePS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PLensflare
	{

		SetVertexShader(CompileShader(vs_4_0, PlaneVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, LensflarePS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PBlurH
	{

		SetVertexShader(CompileShader(vs_4_0, PlaneBlurHVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, BlurLinearPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PBlurV
	{

		SetVertexShader(CompileShader(vs_4_0, PlaneBlurVVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, BlurLinearPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PBlurC
	{

		SetVertexShader(CompileShader(vs_4_0, PlaneBlurCVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, BlurCentricPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}