

Buffer<float> g_HeightMap; //buffer for height map

//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{
	float4  g_LightDir; // Object space
	int g_TerrainRes; // variable for terrrain resolution i suppose
	int g_ShaderID;
};

cbuffer cbChangesEveryFrame
{
	matrix  g_World;
	matrix  g_CockpitWorld;
	matrix  g_WorldViewProjection;
	matrix  g_CockpitViewProjection;
	float   g_Time;
	float4 g_CameraPosWorld;
};

//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault {
};

RasterizerState rsCullFront {
	CullMode = Front;
};

RasterizerState rsCullBack {
	CullMode = Back;
};

RasterizerState rsCullNone {
	CullMode = None;
};

RasterizerState rsLineAA {
	CullMode = None;
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

//
// AlphaBlendig
//
BlendState AlphaBlendingOn
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
	SrcBlend = SRC_ALPHA;
};

struct PosDepth
{
	float4 Pos: SV_POSITION;
	float4 depthPosition : TEXTURE0;
};

struct T3dVertexVSIn {
	float3 Pos : POSITION; //Position in object space
	float2 Tex : TEXCOORD; //Texture coordinate
	float3 Nor : NORMAL;  //Normal in object space
	float3 Tan : TANGENT;  //Tangent in object space (not used in Ass. 5)

	row_major matrix instWorld : WORLD; //instance data:
	row_major matrix instWorldViewProj : WVP;
	row_major matrix instNormal : WNORMAL;
};

//
//Shaders
//

//terrain
PosDepth TerrainDepthVS(uint VertexID:SV_VertexID){
	PosDepth output = (PosDepth)0;
	int xID = VertexID%g_TerrainRes;
	int zID = VertexID / g_TerrainRes;
	//set vertex postition in [-1,1]
	float x = ((float(xID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float z = ((float(zID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float y = g_HeightMap[VertexID];
	float t = 1.0f;
	output.Pos = float4(x, y, z, t);
	// Transform position from object space to homogenious clip space
	output.Pos = mul(output.Pos, g_WorldViewProjection);
	output.depthPosition = output.Pos;
	return output;
}
PosDepth MeshDepthVS(T3dVertexVSIn input){
	PosDepth output = (PosDepth)0;

	//set position
	output.Pos = mul(float4(input.Pos, 1), g_CockpitViewProjection);
	output.depthPosition = output.Pos;

	output.Pos = mul(float4(input.Pos, 1), input.instWorldViewProj);
	output.depthPosition = output.Pos;
	return output;
}

float4 TerrainDepthPS(PosDepth input) :SV_Target0{
	// Get the depth value of the pixel by dividing the Z pixel depth by the homogeneous W coordinate.
	float depthValue = input.depthPosition.z / input.depthPosition.w;
	return float4(depthValue, depthValue, depthValue, 1);
}

float4 SkyMapPS(PosDepth input) :SV_Target0{
	return float4(0,0,0, 1);
}



//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------
technique11 RenderDepth
{
	pass PTerrain
	{
		SetVertexShader(CompileShader(vs_4_0, TerrainDepthVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, TerrainDepthPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PMesh
	{

		SetVertexShader(CompileShader(vs_4_0, MeshDepthVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, TerrainDepthPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PSkyTerrain
	{

		SetVertexShader(CompileShader(vs_4_0, TerrainDepthVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, SkyMapPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
	pass PSkyMesh
	{

		SetVertexShader(CompileShader(vs_4_0, MeshDepthVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, SkyMapPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

}