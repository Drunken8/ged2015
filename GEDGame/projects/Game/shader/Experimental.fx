//--------------------------------------------------------------------------------------
// Shader resources
//--------------------------------------------------------------------------------------

Texture2D   g_Diffuse; // Material albedo for diffuse lighting

Buffer<float> g_HeightMap; //buffer for height map

Texture2D   g_NormalMap; //normal map...

Texture2D   g_DiffuseSpecularEV;

Texture2D   g_DiffuseGlowEV;

Texture2D   _DepthEV;


//resources for experimantals
Texture2D g_HexagonTex;


//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{

	int g_TerrainRes; // variable for terrrain resolution i suppose
	int g_ShaderID;
	float4 _ColLightAmbient;
};

cbuffer cbChangesEveryFrame
{
	float4  g_LightDir;

	matrix  g_World;
	matrix g_View;
	matrix g_Projection;
	matrix  g_CockpitWorld;
	matrix  g_WorldViewProjection;
	matrix  g_CockpitViewProjection;
	float   g_Time;
	matrix g_WorldNormals;
	matrix g_CockpitNormals;
	float4 g_CameraPosWorld;

	//variables for map
	matrix g_MinimapWorld;
	matrix g_MinimapWorldViewProjection;
	matrix g_MinimapNormals;

	//light variables for directional shadows
	matrix _lightWorldViewProjection;
	matrix _lightMeshViewProjection;
};

cbuffer cbUserChanges
{
};




//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault {
};

RasterizerState rsCullFront {
	CullMode = Front;
};

RasterizerState rsCullBack {
	CullMode = Back;
};

RasterizerState rsCullNone {
	CullMode = None;
};

RasterizerState rsLineAA {
	CullMode = None;
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

//
// AlphaBlendig
//
BlendState AlphaBlendingOn
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
	SrcBlend = SRC_ALPHA;
};

//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------



struct PosTex
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
};

struct PosTexLi
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
	float4 LiPos : TEXCOORD1;
};
struct PosTexWorld
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
	float4 World: POSITION;
};

struct T3dVertexVSIn {
	float3 Pos : POSITION; //Position in object space
	float2 Tex : TEXCOORD; //Texture coordinate
	float3 Nor : NORMAL;  //Normal in object space
	float3 Tan : TANGENT;  //Tangent in object space (not used in Ass. 5)

	row_major matrix instWorld : WORLD; //instance data:
	row_major matrix instWorldViewProj : WVP;
	row_major matrix instNormal : WNORMAL;
};

struct T3dVertexPSIn {
	float4 Pos : SV_POSITION; //Position in clip space
	float2 Tex : TEXCOORD;  //Texture coordinate
	float3 PosWorld : POSITION;  //Position in world space
	float3 NorWorld : NORMAL;  //Normal in world space
	float3 TanWorld : TANGENT;  //Tangent in world space (not used in Ass. 5)
	float4 LiPos : TEXCOORD1;
	row_major matrix instWorld : WORLD;
};

//--------------------------------------------------------------------------------------
// Shaders
//--------------------------------------------------------------------------------------

//EXPERIMENTAL SHADER RELATED STUFF

//terrain
PosTex Experimental2VS(uint VertexID:SV_VertexID){
	PosTex output = (PosTex)0;
	int xID = VertexID%g_TerrainRes;
	int zID = VertexID / g_TerrainRes;
	//set vertex postition in [-1,1]
	float x = ((float(xID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float z = ((float(zID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float y = g_HeightMap[VertexID];
	//let the height grow
	if (g_Time <= 6.0)y = 0.0;
	else if (g_Time <= 12.0)y *= (g_Time - 6.0) / 6.0;
	float t = 1.0f;
	output.Pos = float4(x, y, z, t);
	//apply scaling, only works without
	//output.Pos = mul(output.Pos, g_World);
	// Transform position from object space to homogenious clip space
	output.Pos = mul(output.Pos, g_WorldViewProjection);
	float u = ((float)(xID) / (g_TerrainRes - 1));
	float v = ((float)(zID) / (g_TerrainRes - 1));
	output.Tex = float2(u, v);
	return output;
}

float4 ExperimentalPS(PosTex input) :SV_Target0{
	float3 n;
	//get normal from normalmap
	n.xz = (g_NormalMap.Sample(samAnisotropic, input.Tex).rg * 2).rg - 1;
	n.y = sqrt(1 - n.x*n.x - n.z*n.z);
	//
	n = normalize(mul(float4(n, 0), g_WorldNormals).xyz);
	//get texture color
	float3 matDiffuse = g_Diffuse.Sample(samLinearClamp, input.Tex).xyz;//g_HexagonTex g_Diffuse //(64/res*tex)%1
		//calculate light
		float i = saturate(dot(float4(n, 0), g_LightDir));

	//experimental
	//hexagonal grid texture
	float3 hexagon = g_HexagonTex.Sample(samLinearClamp, input.Tex).xyz;
		//get grey value to calculate strength of hexagonal grid
		float hexagonGrey = 1 - (hexagon.x + hexagon.y + hexagon.z) / 3.0;
	//set uv (0,0) to center
	input.Tex = input.Tex * 2 - 1;
	//calculate distance to center
	float dist = sqrt(input.Tex.x*input.Tex.x + input.Tex.y*input.Tex.y);//pixel distance from center
	float timeDist = 0.25*fmod(g_Time, 6.0);//current outer distance of the circle from center
	//alpha for terrain transparency
	//fading strength (smaller=faster fade)
	float fade = 1.1;
	//alpha for terrain transparency
	float a = 1 - ((fmod(g_Time - dist * 4, 6.0) / 6.0)) / fade;
	//circles
	float circleSize = 0.005;
	float innerHexBorder = 0.035;
	float innerFadeBorder = 0.065;
	float3 circleColor = float3(0.3, 1, 1);//cyan(0,1,1) yellow(1,1,0) purple(1,0,1)
		//circleColor = float3(1 - matDiffuse);//inverted terrain color
		//matDiffuse = 1 - matDiffuse;
		//prevent terrain render before first circle
		if (0.25*g_Time < dist) return float4(0, 0, 0, 0);
	//draw circle
	if (timeDist - circleSize < dist&&dist <= timeDist)
		return float4(circleColor, 1);
	//draw hexagon circle
	if (timeDist - innerHexBorder < dist&&dist <= timeDist)
		if (0.4 < hexagonGrey) //only draw hexagon if grey value is high enough
			//blend hexagon and terrain
			return (hexagonGrey*float4(circleColor, 1) + (1 - hexagonGrey)*float4(i*matDiffuse, 1));
	//fade haxagon circle
	if (timeDist - innerFadeBorder <= dist&&dist <= timeDist - innerHexBorder){
		if (0.5 < hexagonGrey){
			//calculate hexagon "alpha" to fade hexagon strength from 1 to 0.5 in this region
			hexagonGrey *= ((dist - (timeDist - innerFadeBorder)) / (innerFadeBorder - innerHexBorder)) / 2 + 0.5;
			return (hexagonGrey*float4(circleColor, 1) + (1 - hexagonGrey)*float4(i*matDiffuse, 1));
		}
	}
	//draw terrain
	//draw terrain on hexagon border
	if (0.55 < hexagonGrey){
		hexagonGrey *= 0.5;
		return (hexagonGrey*float4(circleColor, 1) + (1 - hexagonGrey)*float4(i*matDiffuse, 1));
	}
	//draw fading terrain in hexagon
	//return float4(i*matDiffuse, a);//fade terrain transparent fade=1.5
	return float4(a*i*matDiffuse, 1);//fade terrain black, fade=1.1
	//return float4(a*i*matDiffuse+(1-a)*circleColor, 1);//fade terrain circleColor, fade=2
}

float4 Experimental2PS(PosTex input) :SV_Target0{
	float3 n;
	//get normal from normalmap
	n.xz = (g_NormalMap.Sample(samAnisotropic, input.Tex).rg * 2).rg - 1;
	n.y = sqrt(1 - n.x*n.x - n.z*n.z);
	//
	n = normalize(mul(float4(n, 0), g_WorldNormals).xyz);
	//get texture color
	float3 matDiffuse = g_Diffuse.Sample(samLinearClamp, input.Tex).xyz;//g_HexagonTex g_Diffuse //(64/res*tex)%1
		//calculate light
		float i = saturate(dot(float4(n, 0), g_LightDir));

	//experimental
	//hexagonal grid texture
	float3 hexagon = g_HexagonTex.Sample(samLinearClamp, input.Tex).xyz;
		//get grey value to calculate strength of hexagonal grid
		float hexagonGrey = 1 - (hexagon.x + hexagon.y + hexagon.z) / 3.0;
	//set uv (0,0) to center
	input.Tex = input.Tex * 2 - 1;
	//calculate distance to center
	float dist = sqrt(input.Tex.x*input.Tex.x + input.Tex.y*input.Tex.y);//pixel distance from center
	float timeDist = 0.25*fmod(g_Time, 6.0);//current outer distance of the circle from center
	//alpha for terrain transparency
	//fading strength (smaller=faster fade)
	float fade = 1.1;
	//alpha for terrain transparency
	float a = 1 - ((fmod(g_Time - dist * 4, 6.0) / 6.0)) / fade;
	//circles
	float circleSize = 0.005;
	float innerHexBorder = 0.035;
	float innerFadeBorder = 0.065;
	float3 circleColor = float3(0.3, 1, 1);//cyan(0,1,1) yellow(1,1,0) purple(1,0,1)
		//generate grid
		if (g_Time <= 6.0){
		//prevent terrain render before first circle
		if (0.25*g_Time < dist) return float4(0, 0, 0, 0);
		//draw circle
		if (timeDist - circleSize < dist&&dist <= timeDist)
			return float4(circleColor, 1);
		//draw hexagon circle
		if (timeDist - innerHexBorder < dist&&dist <= timeDist)
			if (0.4 < hexagonGrey) //only draw hexagon if grey value is high enough
				//blend hexagon and terrain
				return (hexagonGrey*float4(circleColor, 1));
		//fade haxagon circle
		if (timeDist - innerFadeBorder <= dist&&dist <= timeDist - innerHexBorder){
			if (0.5 < hexagonGrey){
				//calculate hexagon "alpha" to fade hexagon strength from 1 to 0.5 in this region
				hexagonGrey *= ((dist - (timeDist - innerFadeBorder)) / (innerFadeBorder - innerHexBorder)) / 2 + 0.5;
				return (hexagonGrey*float4(circleColor, 1));
			}
		}
		if (0.55 < hexagonGrey){
			hexagonGrey *= 0.5;
			return (hexagonGrey*float4(circleColor, 1));
		}
		return float4(0, 0, 0, 0);
		}
	if (6.0 < g_Time&&g_Time <= 12.0){
		if (0.55 < hexagonGrey){
			hexagonGrey *= 0.5;
			return (hexagonGrey*float4(circleColor, 1));
		}
		return float4(0, 0, 0, 0);
	}
	//generate terrain color
	if (12.0 < g_Time&&g_Time <= 18.0){
		//draw circle
		if (timeDist - circleSize < dist&&dist <= timeDist)
			return float4(circleColor, 1);
		//draw hexagon circle
		if (timeDist - innerHexBorder < dist&&dist <= timeDist)
			if (0.4 < hexagonGrey) //only draw hexagon if grey value is high enough
				//blend hexagon and terrain
				return (hexagonGrey*float4(circleColor, 1) + (1 - hexagonGrey)*float4(i*matDiffuse, 1));
		//fade haxagon circle
		if (timeDist - innerFadeBorder <= dist&&dist <= timeDist - innerHexBorder){
			if (0.5 < hexagonGrey){
				//calculate hexagon "alpha" to fade hexagon strength from 1 to 0.5 in this region
				hexagonGrey *= ((dist - (timeDist - innerFadeBorder)) / (innerFadeBorder - innerHexBorder)) / 2 + 0.5;
				return (hexagonGrey*float4(circleColor, 1) + (1 - hexagonGrey)*float4(i*matDiffuse, 1));
			}
		}
		//prevent terrain render before circle
		if (timeDist < dist){
			if (0.55 < hexagonGrey){
				hexagonGrey *= 0.5;
				return (hexagonGrey*float4(circleColor, 1));
			}
			return float4(0, 0, 0, 0);
		}
		if (0.55 < hexagonGrey){
			hexagonGrey *= 0.5;
			return (hexagonGrey*float4(circleColor, 1) + (1 - hexagonGrey)*float4(i*matDiffuse, 1));
		}
	}
	//degenerate grid
	if (18.0 < g_Time&&g_Time <= 24.0){
		//draw circle
		if (timeDist - circleSize < dist&&dist <= timeDist)
			return float4(circleColor, 1);
		//prevent grid render after circle
		if (timeDist < dist){
			if (0.55 < hexagonGrey){
				hexagonGrey *= 0.5;
				return (hexagonGrey*float4(circleColor, 1) + (1 - hexagonGrey)*float4(i*matDiffuse, 1));
			}
		}
		return float4(i*matDiffuse, 1);

	}
	//draw terrain
	//draw terrain on hexagon border
	//draw terrain in hexagon
	return float4(i*matDiffuse, 1);
}

/*
PosTex SwitchVS(uint VertexID:SV_VertexID){
if (g_ShaderID == 1){
return TerrainVS(VertexID);
}
else if (g_ShaderID == 2){
return Experimental2VS(VertexID);
}
else{
return TerrainVS(VertexID);
}
}


float4 SwitchPS(PosTex input) :SV_Target0{
if (g_ShaderID == 1){
return ExperimentalPS(input);
}
else if (g_ShaderID == 2){
return Experimental2PS(input);
}
else{
return TerrainPS(input);
}
}
*/
//minimap shader stuff
PosTexWorld MinimapVS(uint VertexID:SV_VertexID){
	PosTexWorld output = (PosTexWorld)0;
	int xID = VertexID%g_TerrainRes;
	int zID = VertexID / g_TerrainRes;
	//set vertex postition in [-1,1]
	float x = ((float(xID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float z = ((float(zID) / (g_TerrainRes - 1)) * 2 - 1);// *g_TerrainRes / 2;
	float y = g_HeightMap[VertexID];
	float t = 1.0f;
	output.Pos = float4(x, y, z, t);
	//apply scaling
	//output.Pos = mul(output.Pos, g_TerrainScaling);
	output.World = mul(output.Pos, g_MinimapWorld);
	// Transform position from object space to homogenious clip space
	output.Pos = mul(output.Pos, g_MinimapWorldViewProjection);
	float u = ((float)(xID) / (g_TerrainRes - 1));
	float v = ((float)(zID) / (g_TerrainRes - 1));
	output.Tex = float2(u, v);
	return output;
}

float4 MinimapPS(PosTexWorld input) :SV_Target0{
	float3 n;
	float4 borderColor = float4(0, 0.8, 1, 1);
		//get normal from normalmap
		n.xz = (g_NormalMap.Sample(samAnisotropic, input.Tex).rg * 2).rg - 1;
	n.y = sqrt(1 - n.x*n.x - n.z*n.z);
	//
	n = normalize(mul(float4(n, 0), g_MinimapNormals).xyz);
	//create a kind of fresnel effect
	float3 viewDir = normalize(input.World.xyz - g_CameraPosWorld.xyz);
		//angle between normal and view at vertex > 90 degree => look at bottom of minimap
		//=>invert viewDir to prevent a "negative" of the effect
		if (dot(n, viewDir)<0){
		viewDir.x = -viewDir.x;
		viewDir.y = -viewDir.y;
		viewDir.z = -viewDir.z;
		}
	float border = dot(n, viewDir);
	border = 1 - border;
	border = pow(border, 2);//<- change strength of effect: lower velue = stronger effect
	//get texture color
	float3 matHex = g_HexagonTex.Sample(samLinearClamp, input.Tex / 2).xyz;
		float hexGrey = 1 - sqrt(matHex.x*matHex.x + matHex.y*matHex.y + matHex.z*matHex.z);
	//hexGrey = 1 - hexGrey;
	//calculate light
	if (hexGrey > 0.2) return float4(0.3, 1, 1, 0.8);
	//if (border <2.5&&border >0.5)return float4(0, 0, 1, 1);
	return (1 - border)*float4(0, 0, 0, 0) + border*borderColor;
}

//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------
technique11 Render
{
	pass P0
	{

		SetVertexShader(CompileShader(vs_4_0, TerrainVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, TerrainPS()));


		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

	pass P1_Mesh
	{

		SetVertexShader(CompileShader(vs_4_0, MeshVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, MeshPS()));


		SetRasterizerState(rsCullBack);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

	pass P2_Minimap
	{
		SetVertexShader(CompileShader(vs_4_0, MinimapVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, MinimapPS()));


		SetRasterizerState(rsCullBack);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(AlphaBlendingOn, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}