#include "NormalGenerator.h"



NormalGenerator::NormalGenerator(float* heightfield, int resolution,int height, const wchar_t* filename)
{
	_heightField = heightfield;
	_res = resolution;
	_height = height;
	_normalField = new Vector3[_res*_res];
	bool print = true;
	ConsoleProgressBar bar;
	if(print)std::cout << "Generating normals\n";
	GEDUtils::SimpleImage* _normalImage = new GEDUtils::SimpleImage(resolution, resolution);
	//generate normals
	for (int y = 1; y < _res; ++y){
		for (int x = 1; x < _res; ++x){
			_generateNormal(x,y,_normalImage);
		}
		if (print)bar.update(static_cast<float>(y)/(_res-1));
	}
	//generate border normals
	for (int i = 0; i < _res; ++i){
		_generateBorderNormal(i, 0, _normalImage);
		_generateBorderNormal(0, i, _normalImage);
		//(0,0) is computed twice
	}
	if (print)bar.end();
	//save normal image
	if (filename != nullptr){
		if (print)std::cout << "Saving normals\n";
		if (!_normalImage->save(filename)){
			throw "Coldn�t save Image\n";
		}
	}
	delete _normalImage;
}

void NormalGenerator::_generateNormal(int x, int y, GEDUtils::SimpleImage* normalImage){//x=u,y=v
	//get delta
	float du = _heightField[IDX(x + 1, y, _res + 1)] - _heightField[IDX(x-1, y, _res + 1)];
	float dv = _heightField[IDX(x, y + 1, _res + 1)] - _heightField[IDX(x, y-1, _res + 1)];
	du /= 2;
	dv /= 2;

	Vector3* normal=new Vector3;
	normal->x = -du*_height;
	normal->y = -dv*_height;
	normal->z = 1.0f;
	_normalize(normal);
	//convert normal vector [-1,1] to RGB [0,1]
	_convertNormal(normal);
	normalImage->setPixel(x, y, normal->x, normal->y, normal->z);
	_normalField[IDX(x, y, _res)] = *normal;
}
void NormalGenerator::_generateBorderNormal(int x, int y, GEDUtils::SimpleImage* normalImage){//x=u,y=v
	//get delta
	float du = _heightField[IDX(x + 1, y, _res + 1)] - _heightField[IDX(x , y, _res + 1)];
	float dv = _heightField[IDX(x, y + 1, _res + 1)] - _heightField[IDX(x, y , _res + 1)];
	Vector3* normal = new Vector3;
	normal->x = -du*_height;
	normal->y = -dv*_height;
	normal->z = 1.0f;
	_normalize(normal);
	_normalField[IDX(x, y, _res)] = *normal;
	//convert normal vector [-1,1] to RGB [0,1]
	_convertNormal(normal);
	normalImage->setPixel(x, y, normal->x, normal->y, normal->z);
}
/*
NormalGenerator::Vector3 NormalGenerator::_mul(Vector3 v1, Vector3 v2){
	Vector3 result;
	result.x = v1.y*v2.z - v1.z*v2.y;
	result.y = v1.z*v2.x - v1.x*v2.z;
	result.z = v1.x*v2.y - v1.y*v2.x;
	return result;
}*/
void NormalGenerator::_normalize(Vector3* v){
	float x = v->x;
	float y = v->y;
	float z = v->z;
	float length = sqrt(x*x + y*y + z*z);
	v->x /= length;
	v->y /= length;
	v->z /= length;
}
void NormalGenerator::_convertNormal(Vector3* v){
	v->x = (v->x + 1.0f) / 2.0f;
	v->y = (v->y + 1.0f) / 2.0f;
	v->z = (v->z + 1.0f) / 2.0f;
}
/*
void NormalGenerator::_invert(Vector3 v){
	v.x = -v.x;
	v.y = -v.y;
	v.z = -v.z;
}*/
NormalGenerator::Vector3* NormalGenerator::getNormalField(){
	return _normalField;
}
NormalGenerator::~NormalGenerator()
{
}
