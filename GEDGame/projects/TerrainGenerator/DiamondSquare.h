#pragma once

#include <iostream>
#include <random>
class DiamondSquare
{
public:
	DiamondSquare(int res,float);
	~DiamondSquare();
	void printArray(float* array, int _wi, int _hi);
	float* _getHeightField();
	float* _getResizedHeightField();
	float* downsizeHeightField(float* heightfield,int resolution);

private:

	int _res;
	float _roughness;
	std::default_random_engine _rand;
	float* _heightField;
	float* _resizedHeightField;
	float* _downsizedHeightField;


	void _squareStep(int,int,int);
	void _diamondStep(int, int, int);
	float _random(float min, float max);
	float _getHeight(int x, int y);
	void _setHeight(int x, int y, float value);
	float* _getdownsizedHeightField();
};

