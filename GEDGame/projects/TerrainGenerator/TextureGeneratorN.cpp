
#include "TextureGeneratorN.h"


TextureGeneratorN::TextureGeneratorN(const wchar_t* flat_low, const  wchar_t* steep_low, const  wchar_t* flat_high, const  wchar_t* steep_high, NormalGenerator::Vector3* normal_map, int resolution, float* heigh_map, const wchar_t* save_destination){
	//Loading Textures
	GEDUtils::SimpleImage _flat_low(flat_low);
	GEDUtils::SimpleImage _steep_low(steep_low);
	GEDUtils::SimpleImage _flat_high(flat_high);
	GEDUtils::SimpleImage _steep_high(steep_high);

	//saving information of normals
	_resolution = resolution;
	_upperBorder = 0.55;
	_lowerBorder = 0.45;

	//generate Textures
	GEDUtils::SimpleImage Texture(_resolution, _resolution);

	float rl = 0, gl = 0, bl = 0;
	float rh = 0, gh = 0, bh = 0;
	float r = 0, g = 0, b = 0;
	color co0, co1, co2, co3;
	float alpha,alphaHeight;

	

	std::cout << "Generating new Texture \n";
	ConsoleProgressBar bar;
	bar.update(0.0f);
	for (int y = 0; y < _resolution; ++y)
	{
		for (int x = 0; x < _resolution; ++x)
		{
			//calculate alphas
			alpha = _calcAlphaSlope(normal_map[IDX(x, y, _resolution)].z);
			float height = heigh_map[IDX(x, y, _resolution)];
			alphaHeight = _calcAlphaHeight(height);
			//alpha = _calcAlphas(heigh_map[IDX(x, y, _resolution)], normal_map[IDX(x, y, _resolution)].z);
			//get tiledColor for current pixel
			co0 = _getColorTiled(x, y, _flat_low);
			co1 = _getColorTiled(x, y, _steep_low);
			co2 = _getColorTiled(x, y, _flat_high);
			co3 = _getColorTiled(x, y, _steep_high);
			//C3 = a3*c3+(1-a3)*(a2*c2 + (1-a2)*(a1*c1 + (1-a1)*c0))
			if (height > _upperBorder){//calculate blended RBG values for heigher ground
				r = co2.r*alpha + co3.r*(1 - alpha);
				g = co2.g*alpha + co3.g*(1 - alpha);
				b = co2.b*alpha + co3.b*(1 - alpha);
			}
			else if (height <_lowerBorder){//calculate blended RBG values for lower ground
				r = co0.r*alpha + co1.r*(1 - alpha);
				g = co0.g*alpha + co1.g*(1 - alpha);
				b = co0.b*alpha + co1.b*(1 - alpha);
			}
			else{//calculate blended RBG values for height border ground
				r = (co2.r*alpha + co3.r*(1 - alpha))*alphaHeight + (co0.r*alpha + co1.r*(1 - alpha))*(1-alphaHeight);
				g = (co2.g*alpha + co3.g*(1 - alpha))*alphaHeight + (co0.g*alpha + co1.g*(1 - alpha))*(1 - alphaHeight);
				b = (co2.b*alpha + co3.b*(1 - alpha))*alphaHeight + (co0.b*alpha + co1.b*(1 - alpha))*(1 - alphaHeight);
			}
			//r = alpha.z*co3.r + (1 - alpha.z)*(alpha.y*co2.r + (1 - alpha.y)*(alpha.x*co1.r + (1 - alpha.x)*co0.r));
			//g = alpha.z*co3.g + (1 - alpha.z)*(alpha.y*co2.g + (1 - alpha.y)*(alpha.x*co1.g + (1 - alpha.x)*co0.g));
			//b = alpha.z*co3.b + (1 - alpha.z)*(alpha.y*co2.b + (1 - alpha.y)*(alpha.x*co1.b + (1 - alpha.x)*co0.b));
			//save new Pixel to Image
			Texture.setPixel(x, y, r, g, b);
		}
		bar.update((float)y / (float)resolution);
	}
	bar.update(1.0f);
	bar.end();
	//save Texture
	std::cout << "Saving Texture\n";
	if (!Texture.save(save_destination)){
		throw "Coldnt save Image\n";
	}
}

TextureGeneratorN::color TextureGeneratorN::_getColorTiled(int x, int y, GEDUtils::SimpleImage _Texture)
{	
	color Color;
	_Texture.getPixel(x % _Texture.getWidth(), y  % _Texture.getHeight(), Color.r, Color.g, Color.b);
	return Color;
}

NormalGenerator::Vector3 TextureGeneratorN::_calcAlphas(float height, float slope)
{
	float _upperBorder = 0.55;
	float _lowerBorder = 0.45;
	NormalGenerator::Vector3 alpha;
	
	if (height > _upperBorder){
		alpha.x = 0;
		alpha.y = height;
		alpha.z = height*slope;
	}
	else if (height >= _lowerBorder && height <= _upperBorder){
		alpha.x = (1 - height)*slope;
		alpha.y = height*0.5;
		alpha.z = height*slope;
	}
	else {
		alpha.x = (1 - height)*slope;
		alpha.y = height;
		alpha.z = 0;
	}
	
	return alpha;
}
float TextureGeneratorN::_calcAlphaHeight(float height){
	float alpha;
	if (height > _upperBorder){
		alpha = 1;
	}
	else if (height >= _lowerBorder && height <= _upperBorder){
		//alpha: convert [_lowerBorder,_upperBorder] to [0,1]
		alpha = (height - _lowerBorder) / (_upperBorder - _lowerBorder);
	}
	else {
		alpha = 0;
	}
	return alpha;
}
float TextureGeneratorN::_calcAlphaSlope(float slope){
	float _flatterBorder = 0.85;
	float _steeperBorder = 0.60;
	float alpha;
	if (slope > _flatterBorder){
		alpha = 1;
	}
	else if (slope >= _steeperBorder && slope <= _flatterBorder){
		//alpha: convert [_steeperBorder,_flatterBorder] to [0,1]
		alpha = (slope - _steeperBorder) / (_flatterBorder - _steeperBorder);
	}
	else {
		alpha = 0;
	}
	return alpha;
}

TextureGeneratorN::~TextureGeneratorN(){

}