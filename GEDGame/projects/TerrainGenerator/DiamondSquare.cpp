#include "DiamondSquare.h"
#include "Array2DSmoothing.h" //please use the Namespace, Cthuhlu, thx. Contains also the IDX(x,y,w)
#include "ConsoleProgressBar.h"
#include <string>

DiamondSquare::DiamondSquare(int res_, float roughness_)
{
	_res = res_;
	_roughness = roughness_;
	_heightField = new float[(_res+1)*(_res+1)];
	_rand.seed(103412934);//time(0));//
	bool print = true;
	//initialize corners
	if(print) std::cout << "initialize corners\n";
	_setHeight(0,0,_random(0.0,1.0));
	_setHeight(0, _res, _random(0.0, 1.0));
	_setHeight(_res, 0, _random(0.0, 1.0));
	_setHeight(_res, _res, _random(0.0, 1.0));
	//fill array progress bar init
	if (print) std::cout << "generating noise " << _res << "*" << _res << "\n";
	int steps=0;
	ConsoleProgressBar progressBar;
	//fill array
	for (int s = _res / 2; s >= 1; s /= 2){
		for (int y = s; y <= _res - s; y += 2 * s){
			for (int x = s; x <= _res - s; x += 2 * s){
				_diamondStep(x, y, s);
				steps++;
			}
			if (print)//print progress bar
				progressBar.update((static_cast<float> (steps) / (_res*_res)));
			
		}
		for (int y = 0; y <= _res; y += s){
			if (y % (2 * s) != 0) for (int x = 0; x <= _res; x += 2 * s){
				_squareStep(x, y, s);
				steps++;
			}
			else for (int x = s; x <= _res; x += 2 * s){
				_squareStep(x, y, s);
				steps++;
			}
			if (print)progressBar.update((static_cast<float> (steps) / (_res*_res)));
			
		}
	}
	if(print)progressBar.end();
	//remove res+1 column/row
	if (print) std::cout << "resize\n";
	float *temp = new float[_res*_res];
	for (int y = 0; y < _res; ++y){
		for (int x = 0; x < _res; ++x){
			temp[IDX(x, y, _res)] = _heightField[IDX(x, y, _res+1)];
		}
	}
	_resizedHeightField = temp;

	//downsize heightfield
	/* use float* DiamondSquare::downsizeHeightField(float* heightfield,int res)
	if (print) std::cout << "downsize\n";
	if (_res > 4){
		_downsizedHeightField = new float[(_res / 4)*(_res / 4)];

		for (int y = 0; y < _res / 4; ++y){
			for (int x = 0; x < _res / 4; ++x){
				//
				float height = 0;
				for (int j = y * 4; j < y * 4 + 4; ++j){
					for (int i = x * 4; i < x * 4 + 4; ++i){
						height += _heightField[IDX(i, j, _res + 1)];
					}
				}
				_downsizedHeightField[IDX(x, y, _res / 4)] = height / 16;
			}
			if (print)progressBar.update(y / (_res/4));
		}
		if (print)progressBar.end();
	}
	else{
		_downsizedHeightField = new float[1];
		_downsizedHeightField[0] = _heightField[0];
	}*/
	
}
void DiamondSquare::_squareStep(int _x, int _y, int _s){
	float value = 0;
	int values = 0;
	if (_y - _s >= 0){
		value += _heightField[IDX(_x, _y - _s, _res+1)];
		values++;
	}
	if (_y + _s <= _res){
		value += _heightField[IDX(_x, _y + _s, _res+1)];
		values++;
	}
	if (_x - _s >= 0){
		value += _heightField[IDX(_x - _s, _y, _res+1)];
		values++;
	}
	if (_x + _s <= _res){ 
		value += _heightField[IDX(_x + _s, _y, _res+1)]; 
		values++;
	}
	value /= values;
	value = _random(value - _roughness*(_s / static_cast<float> (_res)), value + _roughness*(_s / static_cast<float> (_res)));
	_heightField[IDX(_x, _y, _res+1)] = value;
}
void DiamondSquare::_diamondStep(int _x, int _y, int _s){
	float value=0;
	value += _heightField[IDX(_x-_s, _y - _s, _res+1)];
	value += _heightField[IDX(_x+_s, _y + _s, _res+1)];
	value += _heightField[IDX(_x-_s, _y+_s, _res+1)];
	value += _heightField[IDX(_x+_s, _y-_s, _res+1)];
	value /= 4;
	value = _random(value - _roughness*(_s / static_cast<float> (_res)), value + _roughness*(_s / static_cast<float> (_res)));
	_heightField[IDX(_x, _y, _res+1)] = value;
}

float DiamondSquare::_random(float _min, float _max){
	std::normal_distribution<float> dist(_min + (_max - _min) / 2.0,(_max - _min) / 2.0 * 0.8);
	float value = 0;
	do{
		value = dist(_rand);
	} while (value<0||value < _min || value>_max||value>1);
	return value;
}
float* DiamondSquare::downsizeHeightField(float* heightfield,int res){
	bool print = true;
	ConsoleProgressBar progressBar;
	//downsize heightfield
	if (print) std::cout << "downsize\n";
	if (res > 4){
		_downsizedHeightField = new float[(res / 4)*(res / 4)];

		for (int y = 0; y < res / 4; ++y){
			for (int x = 0; x < res / 4; ++x){
				//
				float height = 0;
				for (int j = y * 4; j < y * 4 + 4; ++j){
					for (int i = x * 4; i < x * 4 + 4; ++i){
						height += heightfield[IDX(i, j, res)];
					}
				}
				_downsizedHeightField[IDX(x, y, res / 4)] = height / 16;
			}
			if (print)progressBar.update(static_cast<float> (y) / ((res / 4)-1));
		}
		if (print)progressBar.end();
	}
	else{
		_downsizedHeightField = new float[1];
		_downsizedHeightField[0] = _heightField[0];
	}
	return _downsizedHeightField;
}
float DiamondSquare::_getHeight(int _x, int _y){
	return _heightField[IDX(_x,_y,_res+1)];
}
void DiamondSquare::_setHeight(int _x, int _y, float _value){
	_heightField[IDX(_x, _y, _res+1)] = _value;
}
float* DiamondSquare::_getHeightField(){
	return _heightField;
}
float* DiamondSquare::_getResizedHeightField(){
	return _resizedHeightField;
}
float* DiamondSquare::_getdownsizedHeightField(){
	return _downsizedHeightField;
}
DiamondSquare::~DiamondSquare()
{
}
