#include "ConsoleProgressBar.h"


ConsoleProgressBar::ConsoleProgressBar()
{
	_progressDone = 0;
	_progressLength = 20;
}

ConsoleProgressBar::ConsoleProgressBar(int length)
{
	_progressDone = 0;
	_progressLength = length;
}

void ConsoleProgressBar::update(float progress){
	if (progress > 1.0 || progress < 0.0)progress=1.0;
	int c = progress*_progressLength;
	if (c > _progressDone){//only print progress bar if update is neccessary (performance!)
		_progressBar = "[";
		_progressDone = c;
		for (int i = 0; i < _progressDone; ++i)_progressBar += "=";
		for (int i = _progressDone; i < _progressLength; ++i)_progressBar += " ";
		_progressBar += "]";
		std::cout << _progressBar << "\r";
		std::cout.flush();
	}
}

void ConsoleProgressBar::end(){
	std::cout << _progressBar << "\n";
	std::cout.flush();
}


ConsoleProgressBar::~ConsoleProgressBar()
{
}
