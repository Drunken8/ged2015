
#pragma once

#include <iostream>
#include <string>
#include <SimpleImage.h>
#include "Array2DSmoothing.h"
#include "NormalGenerator.h"
#include "ConsoleProgressBar.h"

class TextureGeneratorN
{
public:

	/**
	@param flat_low path to grass.png e.g.
	@param steep_low path to dirt.png e.g.
	@param flat_high path to rock.png e.g.
	@param steep_high path to pebbles.png e.g.
	@param normal_map the array the texture is for
	@param resolution the resolution of the normal, highmap and texture
	@param high_map the array the texture is for
	@param save_destination the path to save to
	**/
	TextureGeneratorN(const wchar_t* flat_low, const wchar_t* steep_low, const  wchar_t* flat_high, const  wchar_t* steep_high, NormalGenerator::Vector3* normal_map, int resolution, float* high_map,const wchar_t* save_destination);
	~TextureGeneratorN();

	struct color{ float r, g, b; };

private:
	int _resolution;
	float _upperBorder;
	float _lowerBorder;
	GEDUtils::SimpleImage _flat_low();
	GEDUtils::SimpleImage _steep_low();
	GEDUtils::SimpleImage _flat_high();
	GEDUtils::SimpleImage _steep_high();
	
	color _getColorTiled(int x, int y, GEDUtils::SimpleImage Texture);
	NormalGenerator::Vector3 _calcAlphas(float height, float slope);
	float _calcAlphaHeight(float height);
	float _calcAlphaSlope(float slope);
};