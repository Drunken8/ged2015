// TerrainGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Array2DSmoothing.h"
#include "DiamondSquare.h"
#include "NormalGenerator.h"
#include "TextureGeneratorN.h"
#include <string>
#include <sstream>
#include <vector>
#include <SimpleImage.h>
#include <TextureGenerator.h>

int resolution;
std::string o_height;
std::string o_color;
std::string o_normal;
float* height_field;

void ArgumentError(int number){
	std::cout << "False argument! #" << number << "\n";
	exit(0);
}

int _tmain(int argc, _TCHAR* argv[])
{
	

	//-r <resolution> -o_height <output heightfield filename> -o_color <output color filename> -o_normal <output normal filename>

	//READ INPUT
	if (argc != 9){
		std::cout << "Wrong Number of Arguments! \n it was: " << argc << " | expectet: 9\n";
		exit(0);
	}
	else {
		if (_tcscmp(argv[1], _TEXT("-r")) != 0){
			ArgumentError(1);
		}
		else {
			resolution = _tstoi(argv[2]);
			if (resolution <= 0){
				std::cout << "Resolution is 0 or negative: " << resolution << ". Only use positive Values!\n";
			}
		}
		if (_tcscmp(argv[3], _TEXT("-o_height")) != 0){
			ArgumentError(2);
		}
		else {
			std::stringstream sstream;
			sstream << argv[4];
			sstream >> o_height;
		}
		if (_tcscmp(argv[5], _TEXT("-o_color")) != 0){
			ArgumentError(3);
		}
		else {
			std::stringstream sstream;
			sstream << argv[6];
			sstream >> o_color;
		}
		if (_tcscmp(argv[7], _TEXT("-o_normal")) != 0){
			ArgumentError(4);
		}
		else {
			std::stringstream sstream;
			sstream << argv[8];
			sstream >> o_normal;
		}
	}
	//Array2DSmoothing::RNGArray(height_field, resolution+1, resolution+1); //for Random field

	//DO DIAMONDSQUARE
	DiamondSquare DSA(resolution, 0.8);
	height_field = DSA._getResizedHeightField();

	//SMOOTH
	std::cout << "Smooting the Map.... (can take a while)\n";
	Array2DSmoothing::smoothArray(height_field, resolution, resolution, 2, 0.5f); //BEWARE OF BIG SMOOTHS! its (n*2+1)^2*hi*wi!

	//SAVE diwnsized height
	float* downsizedHeight = DSA.downsizeHeightField(height_field, resolution);
	std::cout << "Saving downsized height\n";
	GEDUtils::SimpleImage height_Image(resolution/4, resolution/4);
	for (int i = 0; i < resolution/4; ++i){
		for (int j = 0; j < resolution/4; ++j){
			height_Image.setPixel(i, j, downsizedHeight[IDX(i, j, resolution/4)]); //Copy every value as greyvalue to Image
		}
	}
	if (!height_Image.save(argv[4])){
		throw "Coldnt save Image\n";
	}

	//GENERATE REST
	height_field = DSA._getHeightField();
	NormalGenerator normalGen(height_field, resolution,resolution, argv[8]);
	height_field = DSA._getResizedHeightField();
	std::cout << "Texture \n";
	TextureGeneratorN texGen(L"../../../../external/textures/gras15.jpg",		//gras15.jpg
		L"../../../../external/textures/ground02.jpg",							//ground02.jpg
		L"../../../../external/textures/pebble03.jpg",								//rock4
		L"../../../../external/textures/rock4.jpg",							//pebble03.jgp
		normalGen.getNormalField(), resolution, height_field, argv[6]);
	/*
	TextureGeneratorN texGen(L"../../../../external/textures/debug_green.jpg",		//gras15.jpg
		L"../../../../external/textures/debug_blue.jpg",							//ground02.jpg
		L"../../../../external/textures/debug_yellow.jpg",								//rock4
		L"../../../../external/textures/debug_red.jpg",							//pebble03.jgp
		normalGen.getNormalField(), resolution, height_field, argv[6]);
	*/
	/*
	std::cout << "Switching between Vector and Array \n";
	std::vector<float> vec(height_field, height_field + (resolution + 1)*(resolution + 1));
	std::cout << "Making Texture Generator \n";
	GEDUtils::TextureGenerator texGen(L"../../../../external/textures/gras15.jpg",
		L"../../../../external/textures/ground02.jpg",
		L"../../../../external/textures/pebble03.jpg",
		L"../../../../external/textures/rock1.jpg");
	std::cout << "generating Normal and Texture\n";
	texGen.generateAndStoreImages(vec, resolution, argv[6], argv[8]);
	*/
	delete[] DSA._getHeightField();
	delete[] DSA._getResizedHeightField();
	delete[] downsizedHeight;
	delete[] normalGen.getNormalField();
	return 0;
}

//for testing DSA
/*
int _tmain(int argc, _TCHAR* argv[])
{
	resolution = 2048/2;
	DiamondSquare DSA(resolution,0.45);
	height_field = DSA._getResizedHeightField();
	std::cout << "smooting.... \n";
	Array2DSmoothing::smoothArray(height_field, resolution, resolution, 5, 0.5f); //BEWARE OF BIG SMOOTHS! its (n*2+1)^2*hi*wi!
	//Array2DSmoothing::printArray(DSA._getHeightField(),8,8);
	GEDUtils::SimpleImage height_Image(resolution, resolution);
	for (int i = 0; i < resolution; ++i){
		for (int j = 0; j < resolution; ++j){
			height_Image.setPixel(i, j, height_field[IDX(i, j, resolution)]); //Copy every value as greyvalue to Image
		}
	}
	if (!height_Image.save("testDSA.jpg")){
		throw "Coldn�t save Image";
	}
	height_field = DSA._getHeightField();
	std::cout << "vector \n";
	std::vector<float> vec(height_field, height_field + (resolution + 1)*(resolution + 1));
	std::cout << "texGen \n";
	GEDUtils::TextureGenerator texGen(L"../../../../external/textures/gras15.jpg",
		L"../../../../external/textures/ground02.jpg",
		L"../../../../external/textures/pebble03.jpg",
		L"../../../../external/textures/rock1.jpg");
	std::cout << "generating \n";
	texGen.generateAndStoreImages(vec, resolution, L"testDSAcolor.jpg", L"testDSAnormal.jpg");
	return 0;
}*/



//for testing Array2DSmoothing:
/*
int wi = 7; //breite
int hi = 9; //hoehe

int _tmain(int argc, _TCHAR* argv[])
{
	std::srand(time(0));

	float *test = new float[wi*hi];

	for (int i = 0; i < wi*hi; ++i)
	{
		test[i] = rand() / (float)RAND_MAX;
	}
	
	std::cout << "Original : \n";
	Array2DSmoothing::printArray(test, wi, hi);
	Array2DSmoothing::smoothArray(test, wi, hi, 1, 1);
	std::cout << "Smoothed: \n";
	Array2DSmoothing::printArray(test, wi, hi);

	return 0;
}
*/

