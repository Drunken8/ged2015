#pragma once
#include "Array2DSmoothing.h"
#include "ConsoleProgressBar.h"
#include <iostream>
#include <string>
#include <math.h>
#include <SimpleImage.h>

class NormalGenerator
{
public:
	//NormalGenerator();
	/**
	@param heightfield heightfield of resolution res+1
	@param resolution resolution of the output normal map
	@param height the height of the terrain to weight the normals
	**/
	NormalGenerator(float* heightfield, int resolution, int height, const wchar_t* filename);
	~NormalGenerator();
	
	struct Vector3{ float x, y, z; };//u,v,f
	Vector3* getNormalField();

private:
	float* _heightField;
	Vector3* _normalField;
	int _res;
	int _height;

	void _generateNormal(int x, int y, GEDUtils::SimpleImage* normalImage);
	void _generateBorderNormal(int x, int y, GEDUtils::SimpleImage* normalImage);
	Vector3 _mul(Vector3 v1, Vector3 v2);
	void _normalize(Vector3* v);
	void _convertNormal(Vector3* v);
	void _invert(Vector3 v);
	
};

