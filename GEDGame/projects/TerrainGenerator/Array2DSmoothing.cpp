
#include "Array2DSmoothing.h"

void Array2DSmoothing::RNGArray(float* _array, int _wi, int _hi){

	std::default_random_engine generator;
	generator.seed(time(0));
	std::normal_distribution<double> distribution(0.5f, 0.5f);
	float rand_num;

	for(int i = 0; i < _wi*_hi; ++i){
		rand_num = static_cast<float>(distribution(generator));
		while ((rand_num < 0.0f || rand_num > 1.0f))
		{
			rand_num = static_cast<float>(distribution(generator));
		}
		_array[i] = rand_num;
	}
}


void Array2DSmoothing::printArray(float* _array, int _wi, int _hi){
	for (int i = 0; i < _hi; ++i){
		for (int j = 0; j < _wi; ++j){
			std::cout << _array[IDX(j, i, _wi)] << " ";
		}
		std::cout << "\n";
	}
}

void Array2DSmoothing::smoothArray(float* _array, int _wi, int _hi, int _smoothingRange, float _smoothingStrength){

	ConsoleProgressBar bar;
	bar.update(0.0f);
	float *temp = new float[_wi*_hi];
	for (int i = 0; i < _wi*_hi; ++i){ //copys Array for access to old values
		temp[i] = _array[i];
	}

	for (int i = 0; i < _hi; ++i){ //every Element
		for (int j = 0; j < _wi; ++j){
			int counter = 0;
			float value = 0;
			for (int x = (-1)*_smoothingRange; x <= _smoothingRange; ++x){
				for (int y = (-1)*_smoothingRange; y <= _smoothingRange; ++y){ //field of size (_smoothingRange*2+1)^2 with current point as middle
					if (i + y >= 0 && i + y < _hi && j + x >= 0 && j + x < _wi){
						if (!(x == 0 && y == 0)){
							++counter;
							value += temp[IDX(x + j, y + i, _wi)] * _smoothingStrength;
						} else {
							value += temp[IDX(x + j, y + i, _wi)]; //middle wont be changed by Strength
						}
					}
				}
			}
			//std::cout << value << " | " << counter << "\n"; //f�E debug
			_array[IDX(j, i, _wi)] = value / (1 + counter * _smoothingStrength); 
			bar.update((float)i /(float) _wi);
		}
	}
	bar.update(1.0f);
	bar.end();
	delete[] temp;
}