#pragma once
#include <string>
#include <iostream>
class ConsoleProgressBar
{
public:
	ConsoleProgressBar();
	ConsoleProgressBar(int length);
	/**
	@param progress the progress in [0.0,1.0]
	**/
	void update(float progress);
	void end();
	~ConsoleProgressBar();
private:
	int _progressLength;
	int _progressDone;
	std::string _progressBar;
};

