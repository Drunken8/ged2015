#pragma once

#include "stdafx.h" //Headerdatei
#include <cstdlib> //Zufallszahl
#include <iostream> //Consolenoutput
#include <ctime> //Abfrage der Zeit zum seeden
#include <random> // advanced RNG
#include "ConsoleProgressBar.h"

// Access a 2D array of width w at position x / y
#define IDX(x, y, w) ((x) + (y) * (w)) //definieren vor nutzung

namespace Array2DSmoothing
{
	void printArray(float*, int _wi, int _hi);
	void smoothArray(float*, int _wi, int _hi, int _smoothingRange, float _smoothingStreng);
	void RNGArray(float*, int _wi, int _hi);
};